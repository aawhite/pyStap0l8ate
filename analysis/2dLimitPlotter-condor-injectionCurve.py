from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from  matplotlib import cm as cm
import random
import cPickle as pickle
# import pickle
sys.path=["../source"]+sys.path
from extra import *
import dictionary
from lambdaConversion import *
from math import sqrt

# mpl.use("pgf")
pgf_with_rc_fonts = {
    # "font.family": "sans-serif",
    "font.size" : "15",
    # "font.sans-serif": ["helvetica"],
    "pgf.texsystem":"pdflatex",
    "pgf.preamble":[
                    r"\usepackage{amsmath}",\
                    r"\usepackage[english]{babel}",\
                    r"\usepackage{arev}",\
                   ]
}
mpl.rcParams.update(pgf_with_rc_fonts)
mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']
plt.rcParams["patch.force_edgecolor"] = True
plt.clf(); plt.cla()
fig = plt.figure(1)

####################################################################################################
# Make plot of [limit] vs [signal recovery score] from pickled limitDict, for different fit settings
# File format from condor
####################################################################################################

def ticksInside(removeXLabel=False):
    """ Make atlas style ticks """
    ax=plt.gca()
    ax.tick_params(labeltop=False, labelright=False)
    plt.xlabel(ax.get_xlabel(), horizontalalignment='right', x=1.0)
    plt.ylabel(ax.get_ylabel(), horizontalalignment='right', y=1.0)
    ax.tick_params(axis='y',direction="in",left=1,right=1,which='both')
    ax.tick_params(axis='x',direction="in",labelbottom=not removeXLabel,bottom=1, top=1,which='both')

def atlasInternal(position="nw",status="Work-in-progress",subnote="",lumi=140):
    ax=plt.gca()
    # decide the positioning
    if position=="se":
        textx=0.95; texty=0.05; verticalalignment="bottom"; horizontalalignment="right"
    if position=="nw":
        textx=0.05; texty=0.95; verticalalignment="top"; horizontalalignment="left"
    if position=="ne":
        textx=0.95; texty=0.95; verticalalignment="top"; horizontalalignment="right"
    if position=="n":
        textx=0.5; texty=0.95; verticalalignment="top"; horizontalalignment="center"
    # add label to plot
    lines = [r"\noindent \textbf{{\emph{{ATLAS}}}} {0}".format(status),
             r"$\textstyle\sqrt{s}=13 \text{ TeV } \int{\text{Ldt}}="+str(lumi)+r"\text{ fb}^{\text{-1}}$",
             subnote,
            ]
    labelString = "\n".join(lines)
    # labelString="l=36.1 fb-1"
    plt.text(textx,texty, labelString,transform=ax.transAxes,va=verticalalignment,ha=horizontalalignment, family="sans-serif")
    # set axis labels
    ticksInside()

def calcCurves(signalName,limits):
    # set up plot
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))
    ll = lambdaLimit() # class to calculate limits
    for yVal in limits.keys():
        for xVal in limits[yVal].keys():
            print red(limits[yVal][xVal].keys())
            thisLimit = limits[yVal][xVal]["noSig"]
            print xVal,yVal
            ####################
            # calculate limit
            ####################
            nSiglimit=thisLimit["expLimit"]
            extrapMin = thisLimit["extrapMin"]
            extrapMax = thisLimit["extrapMax"]
            channel   = thisLimit["channel"]
            lumi      = thisLimit["lumi"]
            model     = thisLimit["chirality"]
            interference = thisLimit["interference"]
            limit = ll.getLambdaLimit(nSiglimit,extrapMin,extrapMax,channel,model=model,interference=interference,lumi=lumi)
            print green("Limit=",limit)
            ####################
            # distance from 1:1 signal reco
            ####################
            recoRms = 0
            signalMasses=limits[yVal][xVal].keys()
            for signalMass in signalMasses:
                thisLimit = limits[yVal][xVal][signalMass]
                injected = thisLimit["nSigYieldSr"]
                recovered = thisLimit["nObsYieldSr"]-thisLimit["nFitYieldSr"]
                recoRms+=(injected-recovered)**2
            recoRms=sqrt(recoRms)
            print green("recoRms=",recoRms)
            ####################
            # add points to plot
            ####################
            plt.plot(recoRms,limit,"r.")
            s = "{0:.2f},{1:.2f}".format(xVal,yVal)
            shift = 0.00
            plt.text(recoRms+shift,limit+shift,s,
                     horizontalalignment='left',
                     verticalalignment='center',
                     size=8,
                     )
    lumi = limits[yVal][xVal]["noSig"]["lumi"] # just get lumi for one
    yrange = plt.gca().get_ylim()
    xrange = plt.gca().get_xlim()
    plt.ylim(top=yrange[1]*1.3)
    plt.xlim(right=xrange[1]*1.)
    atlasInternal(position="nw",subnote=signalName.replace("_"," "),lumi=lumi)
    plt.title("Labels: {0},{1}".format(xAxis,yAxis))
    plt.ylabel("Expected $\Lambda$ Limit [TeV]")
    plt.xlabel("Recovery Score")
    path = "plots/scatter-{0}".format(signalName)
    plt.savefig(path+".png")
    plt.savefig(path+".pdf")


os.popen("rm plots/*")
import glob
paths = glob.glob("lustre/pickle-newSignalModelOpt-injections-10x10/*pickle")
limits={}
for path in paths:
    print red(path)
    f=open(path,"r")
    xAxis = "fitMin"
    yAxis = "fitMax"

    # this loop is over the multiple limitDict's stored in the pickle file. 
    # Each one corresponds to a different signal injection
    while 1:
        # load next limit
        try: limit = pickle.load(f)
        except: break

        # load model/range values (check that these are present in the file)
        try:
            extrapMin    = limit["extrapMin"]
            fitMax       = limit["fitMax"]
            fitMin       = limit["fitMin"]
            interference = limit["interference"]
            chirality    = limit["chirality"]
            channel      = limit["channel"]
            signalMass   = limit["signalMass"]
        except:
            raise BaseException("This script requires some values to be stored in the limit dictionary. Please check those")
        # round x,y values to make them better as dictionary keys
        y = round(limit[yAxis],3)
        x = round(limit[xAxis],3)


        # grab signal name
        signal = "{0}_{1}_{2}".format(limit["chirality"],limit["interference"],channel)

        # add this to the dictionary
        if signal not in limits.keys(): limits[signal]={}
        if y not in limits[signal].keys(): limits[signal][y]={}
        if x not in limits[signal][y].keys(): limits[signal][y][x]={}
        limits[signal][y][x][signalMass] = limit

        print yellow(x,y,signal,signalMass,limits[signal][y][x].keys())

###############################
# Make plot of just one signal:
###############################
# print limits.keys()
# signalName = "RL_dest_ee"
# calcCurves(signalName,limits[signalName])
# quit()

###############################
# Make plots of all signals
###############################
for signalName in limits.keys():
    calcCurves(signalName,limits[signalName])
    # quit()


