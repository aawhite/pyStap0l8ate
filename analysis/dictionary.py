
################################################################################
# Dictionary of file locations, PDF functions, etc
################################################################################

# Structure: data[release][channel][pdf]
# where release = 20, 21, prev
# channel = ee, mm

################################################################################
# release 20, current
################################################################################
data20 = {"ee":{},"mm":{}}
# mm
data20["mm"]["backgroundModel"]           = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-pow(x/13.0,1.0/3.0),diphotonLeadingCoef)*pow(x/13.0,p0+p1*log(x/13.0)+p2*log(x/13.0)^2+p3*log(x/13.0)^3)',x,diphotonLeadingCoef[3.9175,1,10],p0[-11.267,-30,10],p1[-4.2736,-10,5],p2[-0.92751,-5,1],p3[-0.083411,-1,0.1])"
data20["mm"]["backgroundTemplate"]        = "../data/mergedHistos_mm.root:mergedSpectrum_total:GeV:rebin10"
# ee
data20["ee"]["backgroundModel"]           = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-pow(x/13.0,0.5),diphotonLeadingCoef)*pow(x/13.0,p0+p1*log(x/13.0)+p2*log(x/13.0)^2+p3*log(x/13.0)^3)',x,diphotonLeadingCoef[2.8088,1,10],p0[-11.492,-30,10],p1[-4.2473,-10,5],p2[-0.92928,-5,1],p3[-0.085168,-1,0.1])"
data20["ee"]["backgroundTemplate"]        = "../data/mergedHistos_ee.root:mergedSpectrum_total:GeV:rebin10"

################################################################################
# release 21, current
################################################################################
data21 = {"ee":{},"mm":{}}
sharedInputs = "../../SharedInputsProj/rel21_templates/"
# mm
# 2018 model
# data21["mm"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-pow(x/13.0,1.0/3.0),diphotonLeadingCoef)*pow(x/13.0,p0+p1*log(x/13.0)+p2*log(x/13.0)^2+p3*log(x/13.0)^3)',x,diphotonLeadingCoef[2.1005,1,15],p0[-1.1710e+01,-30.,10.],p1[-4.2252e+00,-10.,5.],p2[-9.3048e-01,-5.,1.],p3[-8.6747e-02,-1.,0.1])"
# copy Peter's signal model, parameters from new CP recs
data21["mm"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-pow(x/13.0,1.0/3.0),diphotonLeadingCoef)*pow(x/13.0,p0+p1*log(x/13.0)+p2*log(x/13.0)^2+p3*log(x/13.0)^3)',x,diphotonLeadingCoef[1.88782,0.5,15.],p0[-11.7032,-50.,0.],p1[-4.21916,-20.,0.],p2[-0.931291,-5.,0.],p3[-0.0869486,-1.,0.])"
data21["mm"]["backgroundTemplate"]                     = sharedInputs+"/merged_mm.root:mm_dibosonNoOutliers_dilepWt_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF"]                  = sharedInputs+"/merged_mm_kFactorSyst_.root:mm_diboson_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_ALPHAS__1down"]     = sharedInputs+"/merged_mm_kFactorSyst_ALPHAS__1down.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_ALPHAS__1up"]       = sharedInputs+"/merged_mm_kFactorSyst_ALPHAS__1up.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_CHOICE_HERAPDF20"]  = sharedInputs+"/merged_mm_kFactorSyst_CHOICE_HERAPDF20.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_CHOICE_NNPDF30"]    = sharedInputs+"/merged_mm_kFactorSyst_CHOICE_NNPDF30.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF_EV1"]           = sharedInputs+"/merged_mm_kFactorSyst_PDF_EV1.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF_EV2"]           = sharedInputs+"/merged_mm_kFactorSyst_PDF_EV2.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF_EV3"]           = sharedInputs+"/merged_mm_kFactorSyst_PDF_EV3.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF_EV4"]           = sharedInputs+"/merged_mm_kFactorSyst_PDF_EV4.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF_EV5"]           = sharedInputs+"/merged_mm_kFactorSyst_PDF_EV5.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF_EV6"]           = sharedInputs+"/merged_mm_kFactorSyst_PDF_EV6.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF_EV7"]           = sharedInputs+"/merged_mm_kFactorSyst_PDF_EV7.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF_EW__1down"]     = sharedInputs+"/merged_mm_kFactorSyst_PDF_EW__1down.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF_EW__1up"]       = sharedInputs+"/merged_mm_kFactorSyst_PDF_EW__1up.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF__1down"]        = sharedInputs+"/merged_mm_kFactorSyst_PDF__1down.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PDF__1up"]          = sharedInputs+"/merged_mm_kFactorSyst_PDF__1up.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PI__1down"]         = sharedInputs+"/merged_mm_kFactorSyst_PI__1down.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_PI__1up"]           = sharedInputs+"/merged_mm_kFactorSyst_PI__1up.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_REDCHOICE_NNPDF30"] = sharedInputs+"/merged_mm_kFactorSyst_REDCHOICE_NNPDF30.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_SCALE_Z__1down"]    = sharedInputs+"/merged_mm_kFactorSyst_SCALE_Z__1down.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"
data21["mm"]["backgroundTemplate_KF_SCALE_Z__1up"]      = sharedInputs+"/merged_mm_kFactorSyst_SCALE_Z__1up.root:mm_dibosonNoOutliers_smoothDY_smoothTTbar:GeV:rebin10"

# ee
# force params negative
# data21["ee"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,diphotonLeadingCoef)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,diphotonLeadingCoef[6.8805],p0[-1.0870e+01,-30.,0.],p1[-4.1732e+00,-10.,0],p2[-9.5372e-01,-5.,0],p3[-8.9194e-02,-1.,0])"
# fix LCF
# data21["ee"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,diphotonLeadingCoef)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,diphotonLeadingCoef[6.8805],p0[-1.0870e+01,-30.,10.],p1[-4.1732e+00,-10.,5.],p2[-9.5372e-01,-5.,1.],p3[-8.9194e-02,-1.,0.1])"
# float LCF
# data21["ee"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,diphotonLeadingCoef)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,diphotonLeadingCoef[6.8805,0.0001,30],p0[-1.0870e+01,-30.,10.],p1[-4.1732e+00,-10.,5.],p2[-9.5372e-01,-5.,1.],p3[-8.9194e-02,-1.,0.1])"
# copy Peter's signal model, parameters from new CP recs
data21["ee"]["backgroundModel"]                        = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,diphotonLeadingCoef)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,diphotonLeadingCoef[5.36855,0.5,15.],p0[-11.0792,-50.,0.],p1[-4.16267,-20.,0.],p2[-0.952431,-5.,0.],p3[-0.0901539,-1.,0.])"
data21["ee"]["backgroundTemplate"]                     = sharedInputs+"/merged_ee.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF"]                  = sharedInputs+"/merged_ee_kFactorSyst_.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_ALPHAS__1down"]     = sharedInputs+"/merged_ee_kFactorSyst_ALPHAS__1down.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_ALPHAS__1up"]       = sharedInputs+"/merged_ee_kFactorSyst_ALPHAS__1up.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_CHOICE_HERAPDF20"]  = sharedInputs+"/merged_ee_kFactorSyst_CHOICE_HERAPDF20.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_CHOICE_NNPDF30"]    = sharedInputs+"/merged_ee_kFactorSyst_CHOICE_NNPDF30.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF_EV1"]           = sharedInputs+"/merged_ee_kFactorSyst_PDF_EV1.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF_EV2"]           = sharedInputs+"/merged_ee_kFactorSyst_PDF_EV2.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF_EV3"]           = sharedInputs+"/merged_ee_kFactorSyst_PDF_EV3.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF_EV4"]           = sharedInputs+"/merged_ee_kFactorSyst_PDF_EV4.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF_EV5"]           = sharedInputs+"/merged_ee_kFactorSyst_PDF_EV5.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF_EV6"]           = sharedInputs+"/merged_ee_kFactorSyst_PDF_EV6.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF_EV7"]           = sharedInputs+"/merged_ee_kFactorSyst_PDF_EV7.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF_EW__1down"]     = sharedInputs+"/merged_ee_kFactorSyst_PDF_EW__1down.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF_EW__1up"]       = sharedInputs+"/merged_ee_kFactorSyst_PDF_EW__1up.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF__1down"]        = sharedInputs+"/merged_ee_kFactorSyst_PDF__1down.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PDF__1up"]          = sharedInputs+"/merged_ee_kFactorSyst_PDF__1up.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PI__1down"]         = sharedInputs+"/merged_ee_kFactorSyst_PI__1down.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_PI__1up"]           = sharedInputs+"/merged_ee_kFactorSyst_PI__1up.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_REDCHOICE_NNPDF30"] = sharedInputs+"/merged_ee_kFactorSyst_REDCHOICE_NNPDF30.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_SCALE_Z__1down"]    = sharedInputs+"/merged_ee_kFactorSyst_SCALE_Z__1down.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"
data21["ee"]["backgroundTemplate_KF_SCALE_Z__1up"]      = sharedInputs+"/merged_ee_kFactorSyst_SCALE_Z__1up.root:ee_dibosonNoOutliers_201516fakes_smoothDY_smoothTTbar:GeV:rebin10"

################################################################################
# Previous analysis
################################################################################
prevSharedInputs = "/home/prime/dilepton/SharedInputsProj/prevRound_templates/"
dataPrev = {"ee":{},"mm":{}}
dataPrev["ee"]["backgroundModel"]                      = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,diphotonLeadingCoef)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,diphotonLeadingCoef[6.8805,0.0001,30],p0[-1.0870e+01,-30.,10.],p1[-4.1732e+00,-10.,5.],p2[-9.5372e-01,-5.,1.],p3[-8.9194e-02,-1.,0.1])"
dataPrev["ee"]["backgroundTemplate"]                   = prevSharedInputs+"/DataBackground_EE_13TeV_36p5fb_Search.root:DY_Mass_Template:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar1"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar1:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar2"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar2:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar3"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar3:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar4"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar4:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar5"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar5:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar6"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar6:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFVar7"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFVar7:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFChoice"]      = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFChoice:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFScale"]       = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFScale:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PDFAlphaS"]      = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PDFAlphaS:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_THPI"]           = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_THPI:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_THEW"]           = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_THEW:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_PRW"]            = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_PRW:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EFFReco"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EFFReco:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EFFIso"]         = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EFFIso:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EFFTrig"]        = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EFFTrig:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EFFTotal"]       = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EFFTotal:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EXPScale"]       = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EXPScale:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_BE"]             = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_BE:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_THTT"]           = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_THTT:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_THDB"]           = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_THDB:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_QCD"]            = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_QCD:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EFFID"]          = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EFFID:TeV:rebin1"
dataPrev["ee"]["backgroundTemplate_KF_EXPRes"]         = prevSharedInputs+"/Sys_EE_13TeV.root:backgroundTemplate_KF_EXPRes:TeV:rebin1"

data = {"21":data21,"20":data20,"prev":dataPrev}

################################################################################
# Dictionay of lists of PDF names to consider
################################################################################
# Structure: data[release] = list
# where release = 20, 21, prev

# rel20, rel21:
currentPdfNames = [
            "backgroundTemplate_KF",
            "backgroundTemplate_KF_ALPHAS__1down",
            "backgroundTemplate_KF_ALPHAS__1up",
            "backgroundTemplate_KF_CHOICE_HERAPDF20",
            "backgroundTemplate_KF_CHOICE_NNPDF30",
            "backgroundTemplate_KF_PDF_EV7",
            "backgroundTemplate_KF_PDF_EV6",
            "backgroundTemplate_KF_PDF_EV5",
            "backgroundTemplate_KF_PDF_EV4",
            "backgroundTemplate_KF_PDF_EV3",
            "backgroundTemplate_KF_PDF_EV2",
            "backgroundTemplate_KF_PDF_EV1",
            "backgroundTemplate_KF_PDF_EW__1down",
            "backgroundTemplate_KF_PDF_EW__1up",
            "backgroundTemplate_KF_PDF__1down",
            "backgroundTemplate_KF_PDF__1up",
            "backgroundTemplate_KF_PI__1down",
            "backgroundTemplate_KF_PI__1up",
            "backgroundTemplate_KF_REDCHOICE_NNPDF30",
            "backgroundTemplate_KF_SCALE_Z__1down",
            "backgroundTemplate_KF_SCALE_Z__1up",
           ]
# previous analysis
prevPdfNames = [
            "backgroundTemplate_KF_PDFVar1",
            "backgroundTemplate_KF_PDFVar2",
            "backgroundTemplate_KF_PDFVar3",
            "backgroundTemplate_KF_PDFVar4",
            "backgroundTemplate_KF_PDFVar5",
            "backgroundTemplate_KF_PDFVar6",
            "backgroundTemplate_KF_PDFVar7",
            "backgroundTemplate_KF_PDFChoice",
            "backgroundTemplate_KF_PDFScale",
            "backgroundTemplate_KF_PDFAlphaS",
            "backgroundTemplate_KF_THPI",
            "backgroundTemplate_KF_THEW",
            "backgroundTemplate_KF_PRW",
            "backgroundTemplate_KF_EFFReco",
            "backgroundTemplate_KF_EFFIso",
            "backgroundTemplate_KF_EFFTrig",
            "backgroundTemplate_KF_EFFTotal",
            "backgroundTemplate_KF_EXPScale",
            "backgroundTemplate_KF_BE",
            "backgroundTemplate_KF_THTT",
            "backgroundTemplate_KF_THDB",
            "backgroundTemplate_KF_QCD",
            "backgroundTemplate_KF_EFFID",
            "backgroundTemplate_KF_EXPRes",
           ]

# select pdfnames to use based on "release
pdfNames = {"20":currentPdfNames,"21":currentPdfNames,"prev":prevPdfNames}


