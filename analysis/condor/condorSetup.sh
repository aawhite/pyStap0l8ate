
        #! /bin/bash
        export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
        source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh ""
        # asetup AnalysisBase,21.2.1,here
        echo "Running: /atlas/data18c/aaronsw/dilepton/pyStap0l8ate/analysis/condor/condorSingleLimit.py ${@}"
        lsetup "root 6.12.04-x86_64-slc6-gcc62-opt"
        /atlas/data18c/aaronsw/dilepton/pyStap0l8ate/analysis/condor/condorSingleLimit.py $@
    