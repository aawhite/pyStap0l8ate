import os
import numpy as np
from math import log

# run multiple rooswift jobs on condor
# 
# Use: change the argString, analysisPath, and startPoints
#      then run with python2 condorRun.py
#      then use the condor submit command to submit to condor
#
# This will run a bunch of swift fits over a bunch of inputs on condor
# Currently set up to run RooSwift for multiple extrap functions, multiple extrap starts

def makeExecuitable():
    # in case the execuitable gets lost, here's what it should be
    # this function will save it to analysisPath/condorSetup.sh
    global analysisPath
    exe = '''
        #! /bin/bash
        export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
        source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh ""
        # asetup AnalysisBase,21.2.1,here
        echo "Running: {0}/{1} ${{@}}"
        lsetup "root 6.12.04-x86_64-slc6-gcc62-opt"
        {0}/{1} $@
    '''.format(analysisPath,programName)
    f=open(os.path.join(analysisPath,"condorSetup.sh"),"w")
    f.write(exe)
    f.close()
    os.popen("chmod 755 condorSetup.sh")

def createQueueEntry(fullArgString):
    global jobCount,outputPath, job, loopVariables
    outputName = fullArgString.replace(" ","_")
    # make nice, unique output name
    argList = fullArgString.split()
    outputName = [ argList[i][1:]+"W"+argList[i+1] for i in range(len(argList)) if i<len(argList) and argList[i] in loopVariables.keys() ]
    outputName = "_".join(outputName)

    outputFullPath = os.path.join(outputPath,outputName)
    fullArgString=fullArgString.format(outputFullPath)
    print "#"*50
    print fullArgString

    job+= "# number {0}\n".format(jobCount)
    job+= "Arguments = {0}\n".format(fullArgString)
    job+= "Queue\n\n"

    jobCount+=1

def recursiveLooper(depth,fullArgString):
    global loopVariables
    # print "Recursive called with depth:", depth
    # loop over all combinations of variables in loopVariables, in order of sorted(loopVariables.keys())
    # loopVariables is dictionary of variables to loop over
    # depth is the index in the dictionary
    # fullArgString is the string of arguements for rooSwift
    # func is the function to call when the fullArgString is complete 
    #
    # base case: if depth==len(loopVariables.keys())-1
    if depth==len(loopVariables.keys()):
        # print "Base case, depth = ",depth
        createQueueEntry(fullArgString)
        return
    else:
        argName = sorted(loopVariables.keys())[depth]
        for val in loopVariables[argName]:
            #call self
            nextFullArgString = fullArgString + " {0}={1}".format(argName,val)
            recursiveLooper(depth+1,nextFullArgString)
            if single: break


def makeCondorJob():
    # Make quick condor job for submitting multiple fits
    # save as analysisPath/job.condor
    global argString, outputPath, single, analysisPath, job, programName, logOutputName

    # save all root outputs to outputRootFiles/
    # save all condor logs to outputCondorSl7/
    # outputDirRoot=os.path.join(analysisPath,"outputRootFiles")
    outputDirCondor=os.path.join(analysisPath,logOutputName)
    # os.popen("rm -r {0}; mkdir {1}".format(outputDirRoot, outputDirRoot))
    os.popen("rm -r {0}; mkdir {1}".format(outputDirCondor, outputDirCondor))
    # outputPath=outputDirRoot

    # make header
    job+= "Universe = vanilla\n"
    # job+= "Executable = {0}/{1}\n".format(analysisPath,programName)
    job+= "Executable = {0}/condorSetup.sh\n".format(analysisPath)
    job+= "Output = {0}/$(Process).out\n".format(outputDirCondor)
    job+= "Error = {0}/$(Process).error\n".format(outputDirCondor)
    job+= "\n\n"

    # make queue
    count=0
    # loop over all variables in loopVariables
    recursiveLooper(0,argString)

    # save job as file
    f=open(os.path.join(analysisPath,"job.condor"),"w")
    f.write(job)
    f.close()


# ##########################################
# inputs to function as global variables
# ##########################################

loopVariables = {} 
# programName = "python2"
programName = "condorSingleLimit.py"

# output string is where the output root files get saved
outputPath="" # keep empty, global variable
# run string is the arguements for RooSwift

# path to your analysis
analysisPath="/atlas/data18c/aaronsw/dilepton/pyStap0l8ate/analysis/condor"

n=20
tag="lustre{0}x{0}".format(n)
outputDir = "/lustre/umt3/user/aaronsw/dileptonPickles"
path = os.path.join(outputDir,"pickle-"+tag+"/limit2d-{{0}}.pickle")
os.popen("rm "+path.format().format("*"))
os.popen("mkdir {0}/pickle-{1}".format(outputDir,tag))
logOutputName = outputDir+"/condorLog-"+tag
os.popen("rm {0}/*; mkdir {0}".format(logOutputName))

extrapMax     = 6
lumi          = 140
fitMin        = 0.19
release       = "21"
limitsOn      = True
quick         = False

# limitsOn      = False

# change single to True to run a quick test
# single=True; quick=True
single=False; quick=False

argString=''
# argString += " singleLimit.py"
argString += " quick={0}".format(quick)
argString += " limitsOn={0}".format(limitsOn)
argString += " release={0}".format(release)
argString += " fitMin={0}".format(fitMin )
argString += " lumi={0}".format(lumi)
argString += " extrapMax={0}".format(extrapMax )
argString += " path={0}".format(path)
argString += " plotOn={0}".format(1)
argString += " channels=[ee,mm]"
argString += " interferences=[dest,const]"
argString += " chiralities=[LL,RL,LR,RR]"
argString += " signalMasses=[noSig]"


loopVariables["extrapMin"] = np.logspace(log(3.0,10),log(1,10),n)
loopVariables["fitMax"]    = np.logspace(log(1,10),log(3,10),n)




# save the condor execuitable (only needs to be run once)
makeExecuitable()

# ##########################################
# This is the main thing to make the condor execuitable!
# will save it as analysisPath/job.condor
# ##########################################
jobCount=0
job=""
makeCondorJob() # note, this also deletes previous output...

print "%"*50
print '''run with\n\tcondor_submit job.condor ; repeat "condor_release aaronsw"'''
print "or:\n\tcondor_submit_test job.condor"
# you could also use: os.popen("condor_submit job.condor")
