#! /usr/bin/env python2
from __future__ import division
import os, sys, math, copy
import numpy as np
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../../source"]+sys.path
sys.path=["../../analysis"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import pystrap as ps
import dictionary
from math import log
from extra import *
import poissonLimitFunctionDoubleGauss as plf
import modelConstructor

################################################################################
# Script for making single limit calculation
################################################################################

for param in sys.argv:
    if "=" not in param: continue
    name = param.split("=")[0]
    val = param.split("=")[1]
    print param
    print yellow(param, "{0}={1}".format(name,val))
    if val[0]=="[" and val[-1]=="]":
        val=val.replace("'","")
        valList = val[1:-1].split(",")
        print red("loading",valList)
        exec("{0}={1}".format(name,valList))
    else:
        try:
            exec("{0}={1}".format(name,val))
        except:
            exec("{0}='{1}'".format(name,val))


if fitMax-fitMin<0.1:
    print "done"*50
    print "quitting, fitMin fitMax too close",quit()
if extrapMin<fitMax:
    print "done"*50
    print "quitting, extrapMin,fitMax too close",quit()
# print red(fitMin,fitMax,extrapMin); continue



for channel in channels:
    for chirality in chiralities:
        for interference in interferences:
            for signalMass in signalMasses:

                default = {\
                           "signalInjectScale":0,
                           "fitMin":fitMin,"fitMax":fitMax,"extrapMin":extrapMin,"extrapMax":extrapMax,
                           "interpLeadingCoef":False,
                           }

                latexChanName = channel.replace("mm",r"\mu\mu")
                print red(release,channel)
                release=str(release)
                print dictionary.data[release].keys()
                default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
                # corrected path for condor directory:
                default["backgroundTemplate"] = "../"+dictionary.data[release][channel]["backgroundTemplate"]
                default["backgroundTemplate"]+= ":rescale{0}".format(lumi/140)
                # signal model setup
                plotOn=bool(plotOn)
                signalPdf = modelConstructor.fitSignalShape(interference,chirality,plotOn=plotOn,pathPrefix="../")
                default["signalModel"]        = signalPdf


                ########################################
                # 1)
                # Nominal fit
                ########################################
                nominal = ps.extrapolation(default)
                nominal.fit()
                nominalYields = nominal.yields()

                if limitsOn:
                    ########################################
                    # 2)
                    # calculate fit uncertainty via toys
                    ########################################
                    if quick: n=1
                    else: n=30
                    for i in range(n):
                        print red("Toy",i,"/",n)
                        nominal.fitToy(saveToy=False)
                    # fit SS is std of toy fit diffs
                    fitSs = np.array(nominal.toyFitDiff()).std()

                    ########################################
                    # 3)
                    # fit mc variations, to get those
                    ########################################
                    mcExtraps = []
                    mcYields = {}
                    for i, mcName in enumerate(dictionary.pdfNames[release]):
                        default["backgroundTemplate"] = "../"+dictionary.data[release][channel][mcName]
                        default["backgroundTemplate"]+= ":rescale{0}".format(lumi/140)
                        mcExtraps.append(ps.extrapolation(default))
                        mcExtraps[-1].fit()
                        mcYields[mcName] = mcExtraps[-1].yields()
                        if quick and i==0: break
                    # mc SS is envelope of spur sig's of MC variations
                    mcSs = max([abs(mcYields[y]["nSpur"]) for y in mcYields.keys()])
                    mcSs = max(mcSs,nominalYields["nSpur"])

                limitDict={}
                # set up signal
                if signalMass=="noSig":
                    default["signalInjectScale"]   = 0
                    signaName = "noSig"
                    try: del default["signalTemplate"]
                    except: pass
                else:
                    signalMass = int(signalMass)
                    signaName                      = "{0}_{1}_{2}".format(chirality,interference,signalMass)
                    signalHistName                 = "diff_CI_{0}_TeV".format(signaName)
                    default["signalTemplate"]      = "/atlas/data18c/aaronsw/dilepton/pyStap0l8ate/data/ci/templates_r21_ee/CI_Template_{0}.root:{1}:GeV:10".format(chirality,signalHistName)
                    default["signalTemplate"]     += ":rescale{0}".format(lumi/80.5)
                    default["signalInjectScale"]   = 1

                # perform fit
                injectionFit = ps.extrapolation(default)
                injectionFit.fit()

                # get yields
                injectionYields = injectionFit.yields()
                nBkg = injectionYields["nBkg"]
                nFit = injectionYields["nFit"]
                nSig = injectionYields["nSig"]
                nObs = injectionYields["nObs"]
                # calculate limit, save into dictionary
                print red("Limits to be calculated",limitsOn)
                if limitsOn:
                    limitResult=plf.poissonLimit(nFit, nSig, nObs, abs(fitSs),abs(mcSs))
                    print red(limitResult)
                else: limitResult = {}
                # add other useful information
                limitResult["extrapMin"] = default["extrapMin"]
                limitResult["extrapMax"] = default["extrapMax"]
                limitResult["nSigYieldSr"] = injectionYields["nSig"]
                limitResult["nFitYieldSr"] = injectionYields["nFit"]
                limitResult["nObsYieldSr"] = injectionYields["nObs"]
                limitResult["nBkgYieldSr"] = injectionYields["nBkg"]
                limitResult["nSigRecoSr"] = injectionYields["nObs"]-injectionYields["nFit"]
                if limitResult["nSigYieldSr"]!=0:
                    limitResult["recoDivInj"] = limitResult["nSigRecoSr"]/limitResult["nSigYieldSr"]
                else:
                    limitResult["recoDivInj"] = None
                limitResult["simpleSpur"] = abs(nBkg-nFit)
                limitResult["simpleSpurRatio"] = abs(nBkg-nFit)/(nBkg)
                limitResult["lumi"] = lumi
                limitResult["fitMax"]=fitMax
                limitResult["fitMin"]=fitMin
                limitResult["extrapMin"]=extrapMin
                limitResult["extrapMax"]=extrapMax
                limitResult["signal"]=signaName
                limitResult["signalMass"]=signalMass
                limitResult["interference"]=interference
                limitResult["chirality"]=chirality
                limitResult["channel"]=channel


                uniqueName="{0}-{1}-{2}-{3}-{4}-{5}".format(extrapMin,fitMax,interference,chirality,signalMass,channel)
                thisPath=path.format(uniqueName)
                # save pickle
                print "="*20
                print "Saving to",thisPath
                print limitResult.keys()
                print "="*20
                f = open(thisPath,"a")
                pickle.dump(limitResult,f)



print "done"*50
