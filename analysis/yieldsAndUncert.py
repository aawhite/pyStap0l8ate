from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random, pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
import pystrap as ps
import dictionary
from math import log
import plotter
from extra import *


####################################################################################################
# Make yield table, add up uncertainties
####################################################################################################

# configure this script
# os.popen("rm plots/*")
quick=True
quick=False
release="21"
# release="prev"
fitMin = 0.25
fitMax = 1
table = "Channel & Fit Max & nObs & nExp & mc SS & mc SS/nObs & fit SS &fit SS/nObs\\\\\n"
lumi = 36.1

for fitMax in np.arange(1,4,1):
    default = {\
               "signalInjectScale":0,
               "fitMin":fitMin,"extrapMin":fitMax,"extrapMax":6,"fitMax":fitMax,
               "interpLeadingCoef":False,
               }

    # for channelName in ["ee","mm"]:
    for channelName in ["ee"]:
    # for channelName in ["ee"]:
    # for channelName in ["mm"]:
        # load templates from dictionary
        # default["signalTemplate"]     = "../data/CI_Template_lin.root:{0}:GeV:10".format(signalHistName)
        default["backgroundModel"]    = dictionary.data[release][channelName]["backgroundModel"]
        default["backgroundTemplate"] = dictionary.data[release][channelName]["backgroundTemplate"]
        # scale template command
        default["backgroundTemplate"] += ":rescale{0}".format(lumi/140)

        ########################################
        # 1)
        # nominal fit, to get yields, spur sig, etc
        ########################################
        nominal = ps.extrapolation(default)
        nominal.fit()
        nominalYields = nominal.yields()
        path = "plots/nominalFit-fitMax{0}-{1}.png".format(int(fitMax),channelName)
        # plotter.plotMplRatio(nominal,path=path,logx=True,ratioMin=0.5,ratioMax=1.5)

        ########################################
        # 2)
        # toys, to get toy spur sig, etc
        ########################################
        if quick:
            for i in range(2): nominal.fitToy()
        else: 
            for i in range(200): nominal.fitToy()
        path = "plots/fitWithToys-fitMax{0}-{1}.png".format(int(fitMax),channelName)
        # plotter.plotToys(nominal,path=path,logx=True,ratioMin=0.5,ratioMax=1.5)
        # plotter.plotToyParamHist(nominal,path=path)


        ########################################
        # 3)
        # fit pdf variations, to get those
        ########################################
        mcExtraps = []
        mcYields = {}
        for i, mcName in enumerate(dictionary.pdfNames[release]):
            default["backgroundTemplate"] = dictionary.data[release][channelName][mcName]
            mcExtraps.append(ps.extrapolation(default))
            mcExtraps[-1].fit()
            mcYields[mcName] = mcExtraps[-1].yields()
            # make plot
            path = "plots/mc-fitMax{0}-{1}-{2}.png".format(int(fitMax),mcName,channelName)
            # plotter.plotMplRatio(mcExtraps[-1],path=path,logx=True,ratioMin=0.5,ratioMax=1.5)
            if quick: break

        ########################################
        # 4)
        # show nominalYields, results
        ########################################
        for k in sorted(nominalYields.keys()):
            print green(k), red(nominalYields[k])
        for k in sorted(mcYields.keys()):
            for j in sorted(mcYields[k].keys()):
                print green(k+"_"+j), red(mcYields[k][j])
        # get upper and lower toy nSpur
        upperFit = np.array([abs(y["fitDiff"]) for y in nominal._toyYields if y["fitDiff"]>0]).std()
        lowerFit = np.array([abs(y["fitDiff"]) for y in nominal._toyYields if y["fitDiff"]<0]).std()
        # find maximum mc SS
        mcSs = max([abs(mcYields[k]["nSpur"]) for k in mcYields.keys()])
        mcSs = max(mcSs,abs(nominalYields["nSpur"]))

        # nominalYields
        nExp = nominalYields["nFit"]
        nObs = nominalYields["nObs"]
        tp = 4
        table += "{0} & {1} & {2} & {3} & {4} & {5} & $^{{+{6}}}_{{{7}}}$ & $^{{+{8}}}_{{{9}}}$\\\\\n".format(
                     sround(channelName,tp),sround(fitMax,tp),sround(nObs,tp),sround(nExp,tp),
                     sround(mcSs,tp),sround(mcSs/nObs,tp),
                     sround(upperFit,tp),sround(lowerFit,tp),sround(upperFit/nObs,tp),sround(lowerFit/nObs,tp)
                 )

        if quick: break
    if quick: break



print green("Table, lumi={0}".format(lumi))
print table

print ""
print "#"*80
print green("Done (any crash after this is ay-okay)")
print "#"*80

