from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import matplotlib as mpl
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import pystrap as ps
import dictionary
from math import log
import plotter
from extra import *
import poissonLimitFunctionDoubleGauss as plf


####################################################################################################
# Scan over fitMax, produce limit plot
####################################################################################################
# default settings stored in dictionary
default = {\
           "signalInjectScale":0,
           "fitMin":0.20,"extrapMin":6,"extrapMax":6,"fitMax":6,
           "interpLeadingCoef":False,
           }

####################################################################################################

quick=True
quick=False
release="21"
fitMin = 0.25
lumi=36.1
# for channel in ["ee","mm"]:
for channel in ["ee"]:
    latexChanName = channel.replace("mm",r"\mu\mu")
    if quick:
        signalMasses = [28]
        fitMaxs = [0.89]
        extrapMins = [0.96]
    else:
        # signalMasses = [24,26,28,30]
        signalMasses = [28]
        n = 10
        extrapMins = np.logspace(log(1,10),log(3.5,10),n)
        fitMaxs = np.logspace(log(1,10),log(3,10),n)

    for signalMass in signalMasses:
        signalHistName="diff_LL_const_{0}_TeV".format(signalMass)
        # default["signalTemplate"]     = "../data/CI_Template_lin.root:{0}:GeV:10".format(signalHistName)
        default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
        default["backgroundTemplate"] = dictionary.data[release][channel]["backgroundTemplate"]
        default["backgroundTemplate"] += ":rescale{0}".format(lumi/140)

        limitDict={}
        for k, fitMax in enumerate(fitMaxs):
            for j, extrapMin in enumerate(extrapMins):
                if fitMax-fitMin<0.5: continue
                if extrapMin<fitMax: continue
                # print red(fitMin,fitMax,extrapMin); continue
                nominal = ps.extrapolation(default)
                nominal.update(fitMin=fitMin,fitMax=fitMax,extrapMin=extrapMin)
                nominal.fit()
                nominalYields = nominal.yields()
                if quick: n=5
                else: n=50
                for i in range(n):
                    print red(i,"/",n,",",k,"/",len(fitMaxs))
                    nominal.fitToy(saveToy=False)

                ########################################
                # 3)
                # fit mc variations, to get those
                ########################################
                mcExtraps = []
                mcYields = {}
                for i, mcName in enumerate(dictionary.pdfNames[release]):
                    default["backgroundTemplate"] = dictionary.data[release][channel][mcName]
                    default["backgroundTemplate"]+= ":rescale{0}".format(lumi/140)
                    mcExtraps.append(ps.extrapolation(default))
                    mcExtraps[-1].update(fitMin=fitMin,fitMax=fitMax,extrapMin=extrapMin)
                    mcExtraps[-1].fit()
                    mcYields[mcName] = mcExtraps[-1].yields()
                    # if quick: break

                ########################################
                # 4)
                # calculate limits
                ########################################
                # uncertainties
                # fit SS is std of toy fit diffs
                fitSs = np.array(nominal.toyFitDiff()).std()
                # mc SS is envelope of spur sig's of MC variations
                mcSs = max([abs(mcYields[y]["nSpur"]) for y in mcYields.keys()])
                mcSs = max(mcSs,nominalYields["nSpur"])
                nFit = nominalYields["nFit"]
                nSig = nominalYields["nSig"]
                nObs = nominalYields["nObs"]
                limitResult=plf.poissonLimit(nFit, nSig, nObs, abs(fitSs),abs(mcSs))
                if fitMax not in limitDict.keys(): limitDict[fitMax]={}
                limitDict[fitMax][extrapMin]=limitResult
                print "All spurious signals", [mcYields[y]["nSpur"] for y in mcYields.keys()]
                print "Nominal spurious signal",nominalYields["nSpur"]
                print red(nFit, nSig, nObs, abs(fitSs),abs(mcSs))
                print green(limitResult["expLimit"])

        # save pickle
        path="pickle/limit2d-fitMax-extrapMin-{0}-{1}-new.pickle".format(channel,signalMass)
        f = open(path,"w")
        pickle.dump(limitDict,f)
    if quick: break


print green("done "*20)
# needed to clean up pyroot
# del nominal

