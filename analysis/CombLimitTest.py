from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random
import cPickle as pickle
sys.path=["../source"]+sys.path
sys.path=["../analysis"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import pystrap as ps
import poissonLimitFunctionDoubleGauss as plf
import poissonCombLimitFunctionDoubleGauss as plfC
import combtest2 as test
import dictionary
from math import log
from extra import *

nFit_1  = 6.198
nObs_1  = 5.891
fitSs_1 = 0.406
mcSs_1  = 0.306
nSig    = 0

nFit_2  = 5.762
nObs_2  = 6.009
fitSs_2 = 0.347
mcSs_2  = 0.247



limitResult=plf.poissonLimit(nFit_1, nSig, nObs_1, abs(fitSs_1),abs(mcSs_1))

limitResult2=plf.poissonLimit(nFit_2, nSig, nObs_2, abs(fitSs_2),abs(mcSs_2))

limitResultC=test.poissonCombLimit(nFit_1, nSig, nObs_1, nFit_2, nSig, nObs_2, abs(fitSs_1), abs(mcSs_1), abs(fitSs_2), abs(mcSs_2))

print green("##"*50)
print green("part 1")
print red(limitResult)
print green("--"*50)
print green("##"*50)
print green("part 1")
print red(limitResult2)
print green("--"*50)
print green("##"*50)
print green("part 1")
print red(limitResultC)

