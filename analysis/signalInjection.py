from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import matplotlib as mpl
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import pystrap as ps
import dictionary
from math import log
import plotter
from extra import *
import poissonLimitFunctionDoubleGauss as plf
import modelConstructor



####################################################################################################
# Make signal injection tests with various signal levels
####################################################################################################

# optimization with expected limit only
setups       = [ \
                    {"channel":"ee","chirality":"RR","interference":"const","prevLimExp":25.7,"extrapMin":1.93,"fitMin":0.25,"fitMax":1.93},
                    {"channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7,"extrapMin":1.93,"fitMin":0.25,"fitMax":1.93},
                    {"channel":"ee","chirality":"LR","interference":"const","prevLimExp":23.9,"extrapMin":1.81,"fitMin":0.25,"fitMax":1.81},
                    {"channel":"ee","chirality":"RL","interference":"const","prevLimExp":23.8,"extrapMin":1.81,"fitMin":0.25,"fitMax":1.81},
                    {"channel":"ee","chirality":"RR","interference":"dest", "prevLimExp":19.9,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
                    {"channel":"ee","chirality":"LL","interference":"dest", "prevLimExp":19.9,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
                    {"channel":"ee","chirality":"LR","interference":"dest", "prevLimExp":21.2,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
                    {"channel":"ee","chirality":"RL","interference":"dest", "prevLimExp":21.2,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
               ]


# # optimization balancing expectedLimit and reco/Inj
# setups       = [ \
#                     {"channel":"ee","model":"LL","interference":"const","prevLimExp":25.7,"extrapMin":1.80,"fitMin":0.16,"fitMax":1.50},
#                     {"channel":"ee","model":"LR","interference":"const","prevLimExp":23.9,"extrapMin":1.93,"fitMin":0.16,"fitMax":1.78},
#                     {"channel":"ee","model":"RL","interference":"const","prevLimExp":23.8,"extrapMin":1.93,"fitMin":0.16,"fitMax":1.78},
#                     {"channel":"ee","model":"LL","interference":"dest", "prevLimExp":19.9,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
#                     {"channel":"ee","model":"LR","interference":"dest", "prevLimExp":21.2,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
#                     {"channel":"ee","model":"RL","interference":"dest", "prevLimExp":21.2,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
#                ]

# optimization over break, "final" optimization
fitMin = 0.19
setups       = [ \
                    {"tag":"optNy","channel":"ee","chirality":"LL","interference":"const","fitMin":fitMin,"fitMax":2.31,"extrapMin":2.31,"prevLimExp":26},
                    {"tag":"optNy","channel":"ee","chirality":"LR","interference":"const","fitMin":fitMin,"fitMax":2.20,"extrapMin":2.20,"prevLimExp":24},
                    {"tag":"optNy","channel":"ee","chirality":"RL","interference":"const","fitMin":fitMin,"fitMax":2.31,"extrapMin":2.31,"prevLimExp":24},
                    {"tag":"optNy","channel":"ee","chirality":"RR","interference":"const","fitMin":fitMin,"fitMax":2.20,"extrapMin":2.20,"prevLimExp":24},
                    {"tag":"optNy","channel":"ee","chirality":"LL","interference":"dest" ,"fitMin":fitMin,"fitMax":2.40,"extrapMin":2.40,"prevLimExp":20},
                    {"tag":"optNy","channel":"ee","chirality":"LR","interference":"dest" ,"fitMin":fitMin,"fitMax":2.45,"extrapMin":2.45,"prevLimExp":21},
                    {"tag":"optNy","channel":"ee","chirality":"RL","interference":"dest" ,"fitMin":fitMin,"fitMax":2.40,"extrapMin":2.40,"prevLimExp":21},
                    {"tag":"optNy","channel":"ee","chirality":"RR","interference":"dest" ,"fitMin":fitMin,"fitMax":2.40,"extrapMin":2.40,"prevLimExp":21},
                    {"tag":"optNy","channel":"mm","chirality":"LL","interference":"const","fitMin":fitMin,"fitMax":1.75,"extrapMin":1.75,"prevLimExp":24},
                    {"tag":"optNy","channel":"mm","chirality":"LR","interference":"const","fitMin":fitMin,"fitMax":1.75,"extrapMin":1.75,"prevLimExp":23},
                    {"tag":"optNy","channel":"mm","chirality":"RL","interference":"const","fitMin":fitMin,"fitMax":1.80,"extrapMin":1.80,"prevLimExp":22},
                    {"tag":"optNy","channel":"mm","chirality":"RR","interference":"const","fitMin":fitMin,"fitMax":2.01,"extrapMin":2.01,"prevLimExp":22},
                    {"tag":"optNy","channel":"mm","chirality":"LL","interference":"dest" ,"fitMin":fitMin,"fitMax":2.08,"extrapMin":2.31,"prevLimExp":18},
                    {"tag":"optNy","channel":"mm","chirality":"LR","interference":"dest" ,"fitMin":fitMin,"fitMax":2.10,"extrapMin":2.10,"prevLimExp":20},
                    {"tag":"optNy","channel":"mm","chirality":"RL","interference":"dest" ,"fitMin":fitMin,"fitMax":2.10,"extrapMin":2.10,"prevLimExp":20},
                    {"tag":"optNy","channel":"mm","chirality":"RR","interference":"dest" ,"fitMin":fitMin,"fitMax":2.31,"extrapMin":2.31,"prevLimExp":18},
               ]

# # test comparison between old/new settings
# setups       = [ \
#                     # {"tag":"expLimOpt","channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7,"extrapMin":1.93,"fitMin":fitMin,"fitMax":1.93},
#                     {"tag":"new","channel":"ee","chirality":"LL","interference":"const","fitMin":fitMin,"fitMax":2.31,"extrapMin":2.31,"prevLimExp":26},
#                     # {"tag":"recoInjOpt","channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7,"extrapMin":1.80,"fitMin":fitMin,"fitMax":1.50},
#                ]


####################################################################################################

quick=True
quick=False
limitsOn=False
# limitsOn=True
release="21"
lumi=140
# injectScales = np.arange(0,10,0.5)
signalMasses = list(np.arange(22,36,2))+["NoSig"]
# signalMasses=[28]
limitDict={}
os.popen("rm plots/*")

for setupI, setup in enumerate(setups):
    # default settings stored in dictionary
    default = {\
               "extrapMax":6,
               "interpLeadingCoef":False,
               "signalInjectScale":0,
               }
    model = setup["chirality"]
    channel=setup["channel"]
    tag=setup["tag"]
    default["backgroundModel"]     = dictionary.data[release][channel]["backgroundModel"]
    default["backgroundTemplate"]  = dictionary.data[release][channel]["backgroundTemplate"]
    default["backgroundTemplate"] += ":rescale{0}".format(lumi/140)
    default["fitMax"]              = setup["fitMax"]
    default["fitMin"]              = setup["fitMin"]
    default["extrapMin"]           = setup["extrapMin"]

    # signal chirality setup
    interference                   = setup["interference"]
    chirality                      = setup["chirality"]
    # signalPdf                      = fitSignalShape(interference,chirality)
    signalPdf = modelConstructor.fitSignalShape(interference,chirality,plotOn=1)

    default["signalModel"]         = signalPdf

    ########################################
    # 1)
    # Nominal fit
    ########################################
    nominal = ps.extrapolation(default)
    nominal.fit()
    nominalYields = nominal.yields()
    name = setup["channel"]+"_"+setup["chirality"]+"_"+setup["interference"]
    path = "plots/nominal-{0}.png".format(name)
    label=["Setup for: "+setup["channel"]+" "+setup["chirality"]+" "+setup["interference"]]
    plotter.plotMplRatioNoSig(nominal,path=path,logx=True,labels=label,ratioMin=0.0,ratioMax=1.5)
    # break
    # if setupI==1: break
    # continue

    if limitsOn:
        ########################################
        # 2)
        # calculate fit uncertainty via toys
        ########################################
        if quick: n=1
        else: n=50
        for i in range(n):
            print red("Toy",i,"/",n)
            nominal.fitToy(saveToy=False)
        # fit SS is std of toy fit diffs
        fitSs = np.array(nominal.toyFitDiff()).std()


        ########################################
        # 3)
        # fit mc variations, to get those
        ########################################
        mcExtraps = []
        mcYields = {}
        for i, mcName in enumerate(dictionary.pdfNames[release]):
            default["backgroundTemplate"] = dictionary.data[release][channel][mcName]
            default["backgroundTemplate"]+= ":rescale{0}".format(lumi/140)
            mcExtraps.append(ps.extrapolation(default))
            mcExtraps[-1].fit()
            mcYields[mcName] = mcExtraps[-1].yields()
            if quick and i==0: break
        # mc SS is envelope of spur sig's of MC variations
        mcSs = max([abs(mcYields[y]["nSpur"]) for y in mcYields.keys()])
        mcSs = max(mcSs,nominalYields["nSpur"])


    ########################################
    # 4) Loop over injection scales
    ########################################
    for injectI, signalMass in enumerate(signalMasses):
        print red("Signal test",injectI,"/",len(signalMasses))
        # do signal injection fit
        if signalMass!="NoSig":
            signalMass = int(signalMass)
            # signalName                      = "{0}_{1}_{2}".format(setup["model"],setup["interference"],signalMass)
            signalName                      = "{0}_{1}_{2}_{3}".format(setup["chirality"],setup["interference"],channel,tag)
            signalHistName                 = "diff_CI_{0}_{1}_{2}_TeV".format(setup["chirality"],setup["interference"],signalMass)
            default["signalTemplate"]      = "../data/ci/templates_r21_ee/CI_Template_{0}.root:{1}:GeV:10".format(model,signalHistName)
            default["signalTemplate"]     += ":rescale{0}".format(lumi/80.5)
            default["signalInjectScale"]   = 1
        else:
            default["signalInjectScale"]   = 0
            del default["signalTemplate"]
        injectionFit = ps.extrapolation(default)
        injectionFit.fit()
        # get yields
        injectionYields = injectionFit.yields()
        nFit = injectionYields["nFit"]
        nSig = injectionYields["nSig"]
        nObs = injectionYields["nObs"]
        limitResult={}
        if limitsOn:
            # calculate limit, save into dictionary
            limitResult=plf.poissonLimit(nFit, nSig, nObs, abs(fitSs),abs(mcSs))
        else: limitResult={}
        # add other useful information
        limitResult["extrapMin"] = default["extrapMin"]
        limitResult["extrapMax"] = default["extrapMax"]
        limitResult["nSigYieldSr"] = injectionYields["nSig"]
        limitResult["nFitYieldSr"] = injectionYields["nFit"]
        limitResult["nObsYieldSr"] = injectionYields["nObs"]
        limitResult["lumi"] = lumi
        limitResult["tag"] = tag
        if signalName not in limitDict.keys(): limitDict[signalName]={}
        limitDict[signalName][signalMass]=limitResult

        path = "plots/sigInj-{0}-{1}.png".format(signalName,signalMass)
        try:
            if signalMass!="NoSig":
                plotter.plotMplRatio(injectionFit,path=path,logx=True,labels=["Signal {0} {1} TeV".format(signalName.replace("_"," "),signalMass)],ratioMin=0.5,ratioMax=1.5)
            else:
                plotter.plotMplRatioNoSig(injectionFit,path=path,logx=True,labels=["Signal {0} {1} TeV".format(signalName.replace("_"," "),signalMass)],ratioMin=0.5,ratioMax=1.5)
        except: pass

        # if quick and injectI==0: break
        # break
    # break
    if quick: break

# save pickle
path="pickle/sigInject-finalOpt.pickle"
os.popen("rm "+path)
f = open(path,"w")
pickle.dump(limitDict,f)
print "Dumping to", path


print green("done "*20)
# needed to clean up pyroot
# del nominal

