from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path=["../analysis"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import pystrap as ps
import dictionary
from math import log
from extra import *
import poissonLimitFunctionDoubleGauss as plf
import plotter
import modelConstructor

################################################################################
# Script for making single limit calculation
################################################################################

for param in sys.argv:
    if "=" not in param: continue
    name = param.split("=")[0]
    val = param.split("=")[1]
    print yellow(param, "{0}={1}".format(name,val))
    try:
        exec("{0}={1}".format(name,val))
    except:
        exec("{0}='{1}'".format(name,val))



if fitMax-fitMin<0.1:
    print "done"*50
    print "quitting, fitMin fitMax too close",quit()
if extrapMin<fitMax:
    print "done"*50
    print "quitting, extrapMin,fitMax too close",quit()
# print red(fitMin,fitMax,extrapMin); continue

default = {\
           "signalInjectScale":0,
           "fitMin":fitMin,"fitMax":fitMax,"extrapMin":extrapMin,"extrapMax":extrapMax,
           "interpLeadingCoef":False,
           }


latexChanName = channel.replace("mm",r"\mu\mu")
default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
default["backgroundTemplate"] = dictionary.data[release][channel]["backgroundTemplate"]
default["backgroundTemplate"]+= ":rescale{0}".format(lumi/140)
# signal model setup
plotOn=bool(plotOn)
signalPdf, signalPdfNorm = modelConstructor.morphedSignal(interference,chirality,channel=channel,lumi=lumi)
default["signalModel"]         = signalPdf
default["signalNorm"]          = signalPdfNorm

########################################
# 1)
# Nominal fit
########################################
nominal = ps.extrapolation(default)

# print red("*"*50)
# print nominal.get("model").Print()
# print red("*"*50); quit()

nominal.fit()
nominalYields = nominal.yields()


if plotOn:
    plotPath = "plots/nominal-{0}-{1}.png".format(chirality,interference)
    plotter.plotMplRatioNoSig2(nominal,path=plotPath,logx=True,ratioMin=0.0,ratioMax=1.5,lumi=lumi)

if limitsOn:
    ########################################
    # 2)
    # calculate fit uncertainty via toys
    ########################################
    if quick: n=1
    else: n=20
    for i in range(n):
        print red("Toy",i,"/",n)
        nominal.fitToy(saveToy=False)
    # fit SS is std of toy fit diffs
    fitSs = np.array(nominal.toyFitDiff()).std()
    plotPath = "plots/toyParamHist-{0}-{1}.png".format(chirality,interference)
    plotter.plotToyParamHist(nominal,path=plotPath)


    ########################################
    # 3)
    # fit mc variations, to get those
    ########################################
    mcExtraps = []
    mcYields = {}
    for i, mcName in enumerate(dictionary.pdfNames[release]):
        default["backgroundTemplate"] = dictionary.data[release][channel][mcName]
        default["backgroundTemplate"]+= ":rescale{0}".format(lumi/140)
        mcExtraps.append(ps.extrapolation(default))
        mcExtraps[-1].fit()
        mcYields[mcName] = mcExtraps[-1].yields()
        if quick and i==0: break
    # mc SS is envelope of spur sig's of MC variations
    mcSs = max([abs(mcYields[y]["nSpur"]) for y in mcYields.keys()])
    mcSs = max(mcSs,nominalYields["nSpur"])

    plotPath = "plots/mcParamHist-{0}-{1}.png".format(chirality,interference)
    plotter.plotMcParamHist(nominal,mcExtraps,path=plotPath)


limitDict={}
# set up signal
if signalMass=="NoSig":
    default["signalInjectScale"]   = 0
    signaName = "NoSig"
    try: del default["signalTemplate"]
    except: pass
else:
    signalMass = int(signalMass)
    signaName                      = "{0}_{1}_{2}".format(chirality,interference,signalMass)
    signalHistName                 = "diff_CI_{0}_TeV".format(signaName)
    default["signalTemplate"]      = "../data/ci/templates_r21_ee/CI_Template_{0}.root:{1}:GeV:10".format(chirality,signalHistName)
    default["signalTemplate"]     += ":rescale{0}".format(lumi/80.5)
    default["signalInjectScale"]   = 1


# perform fit
injectionFit = ps.extrapolation(default)
injectionFit.fit()
if plotOn:
    plotPath = "plots/injection-{0}-{1}-{2}.png".format(chirality,interference,signalMass)
    plotter.plotMplRatio(injectionFit,path=plotPath,logx=True,ratioMin=0.0,ratioMax=1.5,lumi=lumi)


# get yields
injectionYields = injectionFit.yields()
nBkg = injectionYields["nBkg"]
nFit = injectionYields["nFit"]
nSig = injectionYields["nSig"]
nObs = injectionYields["nObs"]
# calculate limit, save into dictionary
print red("Limits to be calculated",limitsOn)
if limitsOn:
    limitResult=plf.poissonLimit(nFit, nSig, nObs, abs(fitSs),abs(mcSs))
    print red(limitResult)
else: limitResult = {}
# add other useful information
limitResult["extrapMin"] = default["extrapMin"]
limitResult["extrapMax"] = default["extrapMax"]
limitResult["nSigYieldSr"] = injectionYields["nSig"]
limitResult["nFitYieldSr"] = injectionYields["nFit"]
limitResult["nObsYieldSr"] = injectionYields["nObs"]
limitResult["nBkgYieldSr"] = injectionYields["nBkg"]
limitResult["nSigRecoSr"] = injectionYields["nObs"]-injectionYields["nFit"]
if limitResult["nSigYieldSr"]!=0:
    limitResult["recoDivInj"] = limitResult["nSigRecoSr"]/limitResult["nSigYieldSr"]
else:
    limitResult["recoDivInj"] = None
limitResult["simpleSpur"] = abs(nBkg-nFit)
if nBkg!=0:
    limitResult["simpleSpurRatio"] = abs(nBkg-nFit)/(nBkg)
limitResult["lumi"] = lumi
limitResult["fitMax"]=fitMax
limitResult["fitMin"]=fitMin
limitResult["extrapMin"]=extrapMin
limitResult["extrapMax"]=extrapMax
limitResult["signal"]=signaName


# save pickle
print "="*20
print "Saving to",path
print limitResult.keys()
print "="*20
f = open(path,"a")
pickle.dump(limitResult,f)
f.close()



print "done"*50
