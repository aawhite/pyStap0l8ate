#!/usr/bin/env python2

from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import matplotlib as mpl
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import pystrap as ps
import dictionary
from math import log
import plotter
from extra import *
import poissonLimitFunctionDoubleGauss as plf
from collections import defaultdict
import subprocess, time


################################################################################
# Make histograms of uncertainties for nominal fit setups
################################################################################

os.popen("rm plots/*")
channel = "ee"
release = "21"
lumi = 140
quick = True
# quick = False
limitsOn=False
limitsOn=True

setupsDest  = [ \
                    {"channel":"ee","chirality":"RR","interference":"dest", "prevLimExp":19.9,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
                    {"channel":"ee","chirality":"LL","interference":"dest", "prevLimExp":19.9,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
                    {"channel":"ee","chirality":"LR","interference":"dest", "prevLimExp":21.2,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
                    {"channel":"ee","chirality":"RL","interference":"dest", "prevLimExp":21.2,"extrapMin":2.69,"fitMin":0.25,"fitMax":1.12},
               ]

setupsConst  = [ \
                    {"channel":"ee","chirality":"RR","interference":"const","prevLimExp":25.7,"extrapMin":1.93,"fitMin":0.25,"fitMax":1.93},
                    {"channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7,"extrapMin":1.93,"fitMin":0.25,"fitMax":1.93},
                    {"channel":"ee","chirality":"LR","interference":"const","prevLimExp":23.9,"extrapMin":1.81,"fitMin":0.25,"fitMax":1.81},
                    {"channel":"ee","chirality":"RL","interference":"const","prevLimExp":23.8,"extrapMin":1.81,"fitMin":0.25,"fitMax":1.81},
               ]


# scatter plot optimization
fitMin=0.2
fitMax=2.53
setupsDest  = [ \
                    # {"channel":"ee","chirality":"RR","interference":"dest", "prevLimExp":21.0,"extrapMin":fitMax,"fitMin":fitMin,"fitMax":fitMax},
                    {"channel":"ee","chirality":"LL","interference":"dest", "prevLimExp":19.9,"extrapMin":fitMax,"fitMin":fitMin,"fitMax":fitMax},
                    # {"channel":"ee","chirality":"LR","interference":"dest", "prevLimExp":21.2,"extrapMin":fitMax,"fitMin":fitMin,"fitMax":fitMax},
                    # {"channel":"ee","chirality":"RL","interference":"dest", "prevLimExp":21.2,"extrapMin":fitMax,"fitMin":fitMin,"fitMax":fitMax},
               ]
setupsConst  = [ \
                    # {"channel":"ee","chirality":"RR","interference":"const","prevLimExp":25.7,"extrapMin":fitMax,"fitMin":fitMin,"fitMax":fitMax},
                    # {"channel":"ee","chirality":"LL","interference":"const","prevLimExp":25.7,"extrapMin":fitMax,"fitMin":fitMin,"fitMax":fitMax},
                    # {"channel":"ee","chirality":"LR","interference":"const","prevLimExp":23.9,"extrapMin":fitMax,"fitMin":fitMin,"fitMax":fitMax},
                    # {"channel":"ee","chirality":"RL","interference":"const","prevLimExp":23.8,"extrapMin":fitMax,"fitMin":fitMin,"fitMax":fitMax},
               ]

extrapMax     = 6
# path="pickle/uncertaintiesRedo-{0}-fitMax-extrapMin.pickle".format(channel)
path="pickle/limitsForBarPlot-newSigOpt-{0}-fitMax-extrapMin.pickle".format(channel)
os.popen("rm {0}".format(path))
log = "noErasePlots/log.txt"
err = "noErasePlots/err.txt"
logI=0

for setups in [setupsDest,setupsConst]:
    parallelList = []
    for setup in setups:
        fitMin        = setup["fitMin"]
        fitMax        = setup["fitMax"]
        extrapMin     = setup["extrapMin"]
        interference  = setup["interference"]
        chirality     = setup["chirality"]
        # signalMass    = int(setup["prevLimExp"])
        # if signalMass%2: signalMass-=1
        signalMass    = "NoSig"
        print green("="*20,interference,chirality,signalMass,"="*20)

        command = "python2 singleLimit-uncertPlots.py"
        command += r" quick={0}".format(quick)
        command += r" limitsOn={0}".format(limitsOn)
        command += r" release={0}".format(release)
        command += r" fitMin={0}".format(fitMin )
        command += r" lumi={0}".format(lumi)
        command += r" channel={0}".format(channel )
        command += r" fitMax={0}".format(fitMax )
        command += r" extrapMin={0}".format(extrapMin )
        command += r" extrapMax={0}".format(extrapMax )
        command += r" path={0}".format(path)
        command += r" interference={0}".format(interference)
        command += r" chirality={0}".format(chirality)
        command += r" signalMass={0}".format(signalMass)
        command += r" plotOn={0}".format(1)
        print green(command)
        parallelList.append(subprocess.Popen(command.split(),stdout=open(log.replace(".txt",str(logI)+".txt"),"w"),stderr=open(err.replace(".txt",str(logI)+".txt"),"w")))
        logI+=1
        # print result
        # break

    for i,p in enumerate(parallelList):
        print red("Waiting {0}/{1}".format(i,len(parallelList)))
        p.wait()

print green("Done")


