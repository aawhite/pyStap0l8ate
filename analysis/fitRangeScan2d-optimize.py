from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import matplotlib as mpl
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import pystrap as ps
import dictionary
from math import log
import plotter
from extra import *
import poissonLimitFunctionDoubleGauss as plf
from collections import defaultdict
import subprocess, time

start = time.time()

# massList = [28,"NoSig"]
# massList = [24]
# massList = [28]
massList = ["NoSig"]
chiralityList = ["LL","RL","LR","RR"]
# chiralityList = ["LL"]
interferenceList = ["const","dest"]
# interferenceList = ["const"]
channels = ["ee","mm"]
n = 10
# n = 16
fitMaxs = np.logspace(log(1,10),log(3,10),n)
# extrapMins = np.logspace(log(1,10),log(3.5,10),n)
extrapMins = np.logspace(log(3.5,10),log(1,10),n)
print "extrapMins", extrapMins
print "fitMaxs", fitMaxs
release = "21"
lumi = 140
quick = True
quick = False
# fitMins = [0.16,0.18,0.20,0.25,0.30,0.35]
# fitMins = [0.16,0.18,0.20,0.25]
# fitMins = [0.25,0.16,0.20]
fitMins = [0.25]
limitsOn=False
limitsOn=True
extrapMax=6
nErrors=0

os.popen("rm logs/*")
for channel in channels:
    for fitMinI, fitMin in enumerate(fitMins):
        ppath="pickle/limit2d-rel21New-postBreak-{0}-*.pickle".format(channel)
        os.popen("rm {0}".format(ppath))
        # loop over fitMax
        for fitMaxI, fitMax in enumerate(fitMaxs):
            print green(fitMaxI,"/",len(fitMaxs))
            for extrapMinI, extrapMin in enumerate(extrapMins):
                print yellow("#"*50)
                print green(fitMaxI,"/",len(fitMaxs)),red(extrapMinI,"/",len(extrapMins))
                print green("fitMax:",fitMax,"extrapMin:",extrapMin)
                print yellow("#"*50)
                for interferenceI, interference in enumerate(interferenceList):
                    parrallelList = []
                    for chiralityI, chirality in enumerate(chiralityList):
                        for signalMassI, signalMass in enumerate(massList):
                            name    = "{0}-{1}-{2}".format(interference,chirality,signalMass)
                            path    = ppath.replace("*",name)
                            command = "python2 singleLimit.py"
                            command += r" quick={0}".format(quick)
                            command += r" limitsOn={0}".format(limitsOn)
                            command += r" release='{0}'".format(release)
                            command += r" fitMin={0}".format(fitMin )
                            command += r" lumi={0}".format(lumi)
                            command += r" channel='{0}'".format(channel )
                            command += r" fitMax={0}".format(fitMax )
                            command += r" extrapMin={0}".format(extrapMin )
                            command += r" extrapMax={0}".format(extrapMax )
                            command += r" path='{0}'".format(path)
                            command += r" interference='{0}'".format(interference)
                            command += r" chirality='{0}'".format(chirality)
                            command += r" signalMass='{0}'".format(signalMass)
                            command += r" plotOn='{0}'".format(0)
                            log      = "logs/log-{0}-{1}-{2}.log".format(interference,chirality,signalMass,extrapMin,fitMin,fitMax)
                            err      = "logs/err-{0}-{1}-{2}.log".format(interference,chirality,signalMass,extrapMin,fitMin,fitMax)
                            print green(command.replace("'",r"\'"))
                            parrallelList.append(subprocess.Popen(command.split(),stdout=open(log,"w"),stderr=open(err,"w")))
                    print red("waiting")
                    for i,p in enumerate(parrallelList): 
                        print i,"/",len(parrallelList)
                        p.wait()
                    print red("waiting done")


print red("Number of errors",nErrors)
print red("Runtime (s)",time.time()-start)
print red("Runtime (m)",(time.time()-start)/60)
print red("Runtime (h)",(time.time()-start)/60/60)
print green("Done")


