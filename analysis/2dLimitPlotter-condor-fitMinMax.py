from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from  matplotlib import cm as cm
import random
import cPickle as pickle
# import pickle
sys.path=["../source"]+sys.path
from extra import *
import dictionary
from lambdaConversion import *

# mpl.use("pgf")
pgf_with_rc_fonts = {
    # "font.family": "sans-serif",
    "font.size" : "15",
    # "font.sans-serif": ["helvetica"],
    "pgf.texsystem":"pdflatex",
    "pgf.preamble":[
                    r"\usepackage{amsmath}",\
                    r"\usepackage[english]{babel}",\
                    r"\usepackage{arev}",\
                   ]
}
mpl.rcParams.update(pgf_with_rc_fonts)
mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']
plt.rcParams["patch.force_edgecolor"] = True
plt.clf(); plt.cla()
fig = plt.figure(1)

####################################################################################################
# Make 2d limit plot from pickled limitDict
# File format from condor
# Expect fitMin/fitMax scan
####################################################################################################

def ticksInside(removeXLabel=False):
    """ Make atlas style ticks """
    ax=plt.gca()
    ax.tick_params(labeltop=False, labelright=False)
    plt.xlabel(ax.get_xlabel(), horizontalalignment='right', x=1.0)
    plt.ylabel(ax.get_ylabel(), horizontalalignment='right', y=1.0)
    ax.tick_params(axis='y',direction="in",left=1,right=1,which='both')
    ax.tick_params(axis='x',direction="in",labelbottom=not removeXLabel,bottom=1, top=1,which='both')

def atlasInternal(position="nw",status="Work-in-progress",subnote="",lumi=140):
    ax=plt.gca()
    # decide the positioning
    if position=="se":
        textx=0.95; texty=0.05; verticalalignment="bottom"; horizontalalignment="right"
    if position=="nw":
        textx=0.05; texty=0.95; verticalalignment="top"; horizontalalignment="left"
    if position=="ne":
        textx=0.95; texty=0.95; verticalalignment="top"; horizontalalignment="right"
    if position=="n":
        textx=0.5; texty=0.95; verticalalignment="top"; horizontalalignment="center"
    # add label to plot
    lines = [r"\noindent \textbf{{\emph{{ATLAS}}}} {0}".format(status),
             r"$\textstyle\sqrt{s}=13 \text{ TeV } \int{\text{Ldt}}="+str(lumi)+r"\text{ fb}^{\text{-1}}$",
             subnote,
            ]
    labelString = "\n".join(lines)
    # labelString="l=36.1 fb-1"
    plt.text(textx,texty, labelString,transform=ax.transAxes,va=verticalalignment,ha=horizontalalignment, family="sans-serif")
    # plt.tight_layout(.5)
    # set axis labels
    ticksInside()

def makePlot(title,limitDict,channelName,signalMass,xAxis,yAxis,channel="ee",model="LL",interference="const",display="expLim"):
    # make plots based on limit dict
    # make plots
    print "*"*80
    print "*"*80
    print "Making plot", channelName, signalMass
    print "*"*80
    print "*"*80
    plt.clf(); plt.cla()
    plt.figure(figsize=(10,10))
    # plt.figure()
    # varName = "upperLimit"
    varName = "expLimit"
    yVals = sorted(limitDict.keys())
    # get set of all keys in table
    xVals = []
    for yVal in yVals:
        for xVal in limitDict[yVal].keys():
            if xVal not in xVals: xVals.append(xVal)
    xVals = sorted(xVals)
    # xVals.pop(-1)
    # build dataSet
    print red(xVals)
    print red(yVals)
    scale=""
    data = [[np.nan]*len(xVals) for i in range(len(yVals))]
    ll = lambdaLimit() # class to calculate limits
    for i,yVal in enumerate(yVals):
        for j, xVal in enumerate(xVals):
            if xVal in limitDict[yVal].keys():
                lumi = limitDict[yVal][xVal]["lumi"] # just get lumi for one
                if display=="expLim":
                    nSiglimit=limitDict[yVal][xVal]["expLimit"]
                    extrapMin = limitDict[yVal][xVal]["extrapMin"]
                    extrapMax = limitDict[yVal][xVal]["extrapMax"]
                    limit = ll.getLambdaLimit(nSiglimit,extrapMin,extrapMax,channel,model=model,interference=interference,lumi=lumi)
                    data[i][j] = limit; scale="95\\% CI upper limit on $\\Lambda$"
                elif display=="sigExpLim":
                    nSiglimit=limitDict[yVal][xVal]["expLimit"]
                    data[i][j] = nSiglimit; scale="95\\% CI upper limit on Signal Events"
                    print red(nSiglimit)
                elif display=="simpleSpurRatio":
                    data[i][j] = limitDict[yVal][xVal]["simpleSpurRatio"]
                    scale="Simple Spurious Signal (fit-bkg)/bkg"
                elif display=="fitBkgDif":
                    bkg = limitDict[yVal][xVal]["nBkgYieldSr"]
                    fit = limitDict[yVal][xVal]["nFitYieldSr"]
                    data[i][j] = 1-abs(fit-bkg)/bkg
                    scale=r"$1-|Fit-Bkg|/Bkg$"
                elif display=="recoDivInj":
                    data[i][j] = limitDict[yVal][xVal]["recoDivInj"]
                    scale="Recovered/Injected Signal"
                else:
                    raise BaseException("No display good",display)
    data = np.array(data)
    data[data==None] = np.nan
    print green(data)
    allNan=True
    for i in data:
        for j in i:
            if not np.isnan(j): allNan=False
    print green("Plot is: allNan={0}".format(allNan))
    if allNan: 
        print red("Unmakable plot, all nan values")
        return
    # data = np.flip(data,0)
    # data = np.transpose(data)
    # mask array
    # mask with maximum cell value
    maximum = 3
    # heatmap = plt.pcolor(data, cmap=cm.summer)
    if display=="recoDivInj" or display=="simpleSpurRatio" or display=="fitBkgDif":
        dataMax=1
        dataMin=0
        heatmap = plt.imshow(np.flip(data,0), cmap=cm.autumn,vmin=0,vmax=1,extent=[0,len(xVals),0,len(yVals)])
    else:
        dataMax=np.nanmax(data)
        dataMin=np.nanmin(data)
        heatmap = plt.imshow(np.flip(data,0),vmin=dataMin,vmax=dataMax,cmap=cm.autumn,extent=[0,len(xVals),0,len(yVals)])
    # add numbers to plot
    putNumbersOnPlot = True
    # putNumbersOnPlot = False
    scaleMin=1e30; scaleMax=0
    if putNumbersOnPlot:
        for y in range(data.shape[0]):
            for x in range(data.shape[1]):
                if np.isnan(data[y,x]): continue
                # if yVals[y]>=xVals[x]: continue
                # if data[y, x] > maximum: s = "X"
                # else: s = "{0:.2f}".format(data[y, x])
                scaleMin=min(scaleMin,data[y,x])
                scaleMax=max(scaleMax,data[y,x])
                s = "{0:.2f}".format(data[y, x])
                plt.text(x+0.5,y+0.5,s,
                         horizontalalignment='center',
                         verticalalignment='center',
                         size=4,
                         )
    nMarginTop=5 # add some whitespace at top of plot
    xlabels=[round(float(i),2) for i in xVals]
    ylabels=[round(float(i),2) for i in yVals]+["x"]*nMarginTop
    plt.xticks(np.arange(0.5,len(xlabels)+0.5,1),xlabels,rotation='vertical',fontsize=8)
    plt.yticks(np.arange(0.5,len(ylabels)+0.5,1),ylabels,fontsize=8)
    # plt.ylim(bottom=min(yVals)-1)
    # remove tick labels from top of plot
    plt.gca().set_yticks(plt.gca().get_yticks()[:-nMarginTop])
    plt.ylabel("{0} [TeV]".format(yAxis.replace("fitMax","Fit Range Maximum").replace("fitMin","Fit Range Minimum")))
    plt.xlabel("{0} [TeV]".format(xAxis.replace("extrapMin","Signal Range Minimum").replace("fitMax","Fit Range Maximum")))
    latexChanName = channelName.replace("mm",r"$\mu\mu$")
    latexChanName = latexChanName.replace("ee",r"$ee$")
    # title="{0} {1} {2} Channel".format(latexChanName, model,interference)
    title="{0} {1} {2}-channel".format(model,interference,latexChanName)
    atlasInternal(position="nw",subnote=title,lumi=lumi)
    # set up colorbar scale
    cbar=False
    if cbar:
        cbar = plt.colorbar(heatmap,ticks=[dataMin,dataMax])
        cbar.ax.set_yticks([dataMin,dataMax])
        cbar.ax.set_yticklabels([round(dataMin,1),round(dataMax,1)])
        cbar.ax.set_ylabel(scale,rotation=270,labelpad=20)
    path="plots/limit2d-{0}-{1}-{2}-{3}-{4}-{5}-{6}".format(display,xAxis,yAxis,channelName,signalMass,model,interference)
    path=path.replace(" ","_")
    print green("Saving plot")
    plt.savefig(path+".pdf",bbox_inches="tight")
    plt.savefig(path+".png",bbox_inches="tight")
    print green("Done save plot")

os.popen("rm plots/*")
import glob
# paths = glob.glob("umt3Download/*pickle")
# paths = glob.glob("umt3Download2/*pickle")
# paths = glob.glob("pickle-10x10/*pickle")
paths = glob.glob("pickle-full60x60/*pickle")
# paths = glob.glob("pickle-full30x30/*pickle")
# paths = glob.glob("pickle-full30x30/*pickle")
paths = glob.glob("lustre/pickle-lustre10x10/*pickle")
paths = glob.glob("lustre/pickle-lustre60x60/*pickle")
paths = glob.glob("pickle-lustre20x20/*pickle")
paths = glob.glob("lustre/pickle-newSignalModelOpt-fitMinScan3-20x20/*pickle")
paths = glob.glob("lustre/pickle-newSignalModelOpt-fitMinScan3-25x25/*pickle")
limits={}
for path in paths:
    print red(path)
    f=open(path,"r")
    name = os.path.basename(path)
    name=name.replace(".pickle","")
    name=name.replace("-","_")
    title=""
    xAxis = "fitMin"
    yAxis = "fitMax"

    # load and update
    # limit = pickle.load(f)
    try: limit = pickle.load(f)
    except: continue

    # load model/range values (check that these are present in the file)
    try:
        extrapMin    = limit["extrapMin"]
        fitMax       = limit["fitMax"]
        fitMin       = limit["fitMin"]
        interference = limit["interference"]
        chirality    = limit["chirality"]
        channel      = limit["channel"]
    except:
        raise BaseException("This script requires some values to be stored in the limit dictionary. Please check those")


    y = limit[yAxis]
    x = limit[xAxis]
    # if y<(x-0.5)*0.5: continue
    # if x>3.0: continue
    if x<0.1: continue
    if y>4.5: continue


    # grab signal name
    # signal                = limit["signal"]
    signal = "{0}_{1}_{2}_{3}".format(limit["chirality"],limit["interference"],limit["signalMass"],channel)

    if signal not in limits.keys(): limits[signal]={}
    if fitMax not in limits[signal].keys(): limits[signal][fitMax]={}
    limits[signal][y][x] = limit
    print yellow(limit.keys())


for signalName in limits.keys():
    print "*"*20
    print signalName, limits[signalName].keys()
    print "*"*20
    # print signalName; continue
    # if "LL_const" not in signalName: continue
    limitDict=limits[signalName]
    # load pickle file
    # limitDict=pickle.load(f)
    print "#"*20
    print signalName
    print limitDict.keys()
    print "#"*20
    model = signalName.split("_")[0]
    interference = signalName.split("_")[1]
    signalMass = signalName.split("_")[2]
    channel = signalName.split("_")[3]
    # for display in ["recoDivInj","fitBkgDif","simpleSpurRatio","expLim"]:
    for display in ["simpleSpurRatio"]:
    # for display in ["expLim"]:
        makePlot(title,limitDict,
                 channel,signalMass,
                 xAxis,yAxis,
                 model=model,interference=interference,
                 display=display,
                 channel=channel
                 )
    # quit()
