from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random, pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
import pystrap as ps
import dictionary
from math import log
import plotter
from extra import *

####################################################################################################
# Make fits with toys
####################################################################################################



# default settings stored in dictionary
default = {\
           "signalInjectScale":0,
           "fitMin":0.20,"extrapMin":6,"extrapMax":6,"fitMax":6,
           "interpLeadingCoef":False,
           }

####################################################################################################

os.popen("rm plots/*")
quick=True
quick=False
release="21"
signalMass = 20
fitMax = 1
extrapMin = 2
fitMin = 0.25
pout=open("extrap.pickle","w")
# for channelName in ["ee","mm"]:
for channelName in ["ee"]:
    latexChanName = channelName.replace("mm",r"\mu\mu")
    xAxisName = "r$M_{{{0}}}$ TeV".format(latexChanName)

    signalHistName="diff_LL_const_{0}_TeV".format(signalMass)
    default["signalTemplate"]     = "../data/CI_Template_lin.root:{0}:GeV:10".format(signalHistName)
    default["backgroundModel"]    = dictionary.data[release][channelName]["backgroundModel"]
    default["backgroundTemplate"] = dictionary.data[release][channelName]["backgroundTemplate"]
    nominal = ps.extrapolation(default)

    limitDict={}
    if fitMax-fitMin<0.5: continue
    nominal.update(fitMin=fitMin,fitMax=fitMax,extrapMin=extrapMin)
    n = 1e3
    if not quick:
        nominal.fit()
        for i in range(2): nominal.fitToy()
        # nominal.fit()
        # nominal.fitToy()
        # pickle.dump(nominal,pout)
        path = "plots/output.png"
        # plotter.plotMplRatio(nominal,path=path,logx=True,ratioMin=0.5,ratioMax=1.5)
        # plotter.plotToys(nominal,path=path,logx=True,ratioMin=0.5,ratioMax=1.5)
        # plotter.plotToyParamHist(nominal,path=path)
        plotter.plotOnlyToys(nominal,path=path,logx=True,ratioMin=0.5,ratioMax=1.5)
        break
    break

print green("Done")
# needed to clean up pyroot
# del e

