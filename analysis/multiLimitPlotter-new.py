from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from  matplotlib import cm as cm
import random
import cPickle as pickle
sys.path=["../source"]+sys.path
from extra import *
import dictionary
from lambdaConversion import *


# mpl.use("pgf")
pgf_with_rc_fonts = {
    # "font.family": "sans-serif",
    "font.size" : "15",
    # "font.sans-serif": ["helvetica"],
    "pgf.texsystem":"pdflatex",
    "pgf.preamble":[
                    r"\usepackage{amsmath}",\
                    r"\usepackage[english]{babel}",\
                    r"\usepackage{arev}",\
                   ]
}
mpl.rcParams.update(pgf_with_rc_fonts)
mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']
plt.rcParams["patch.force_edgecolor"] = True
plt.clf(); plt.cla()
fig = plt.figure(1)

####################################################################################################
# Make 2d limit plot from pickled limitDict
####################################################################################################

def ticksInside(removeXLabel=False):
    """ Make atlas style ticks """
    ax=plt.gca()
    ax.tick_params(labeltop=False, labelright=False)
    plt.xlabel(ax.get_xlabel(), horizontalalignment='right', x=1.0)
    plt.ylabel(ax.get_ylabel(), horizontalalignment='right', y=1.0)
    ax.tick_params(axis='y',direction="in",left=1,right=1,which='both')
    ax.tick_params(axis='x',direction="in",labelbottom=not removeXLabel,bottom=1, top=1,which='both')

def atlasInternal(position="nw",status="Work-in-progress",lumi=36.1,subnote=""):
    pass
    ax=plt.gca()
    # decide the positioning
    if position=="se":
        textx=0.95; texty=0.05; verticalalignment="bottom"; horizontalalignment="right"
    if position=="nw":
        textx=0.05; texty=0.95; verticalalignment="top"; horizontalalignment="left"
    if position=="ne":
        textx=0.95; texty=0.95; verticalalignment="top"; horizontalalignment="right"
    if position=="n":
        textx=0.5; texty=0.95; verticalalignment="top"; horizontalalignment="center"
    # add label to plot
    lines = [r"\noindent \textbf{{\emph{{ATLAS}}}} {0}".format(status),
             r"$\textstyle\sqrt{s}=13 \text{ TeV } \int{\text{Ldt}}="+str(lumi)+r"\text{ fb}^{\text{-1}}$",
            ]
    if subnote: lines.append(subnote)
    labelString = "\n".join(lines)
    # labelString="l=36.1 fb-1"
    plt.text(textx,texty, labelString,transform=ax.transAxes,va=verticalalignment,ha=horizontalalignment, family="sans-serif")
    # # plt.tight_layout(.5)
    # # set axis labels
    ticksInside()


# iPath = "pickle/uncertainties-ee-fitMax-extrapMin.pickle"
# iPath = "pickle/uncertaintiesRedo-ee-fitMax-extrapMin.pickle"
# iPath = "pickle/limitsForBarPlot-ee-fitMax-extrapMin.pickle"
iPath = "pickle/limitsForBarPlot-newSigOpt-ee-fitMax-extrapMin.pickle"
f=open(iPath,"r")
os.popen("rm plots/*")

print pickle.load(f)

bySignal={}
while True:
    try: d = pickle.load(f)
    except: break
    # print d.keys()
    signal = d["chirality"]+" "+d["interference"]
    bySignal[signal] = d
    print red(signal)


prevLim ={
            "LL_const":25.7,"LL_dest":19.9,"LR_const":23.9,"LR_dest":21.2,
            "RL_const":23.8,"RL_dest":21.2,"RR_const":23.6,"RR_dest":21.0,
         }

channel="ee"
lumi=36.1
lumi=140
plt.figure(figsize=(8,6))
xlabels = []
lambdaConvert = lambdaLimit() # class to calculate limits
signals = sorted(bySignal.keys(),key=lambda x: bySignal[x]["interference"]+bySignal[x]["chirality"])
for i,signal in enumerate(signals):
    print green(signal);
    continue
    print yellow(bySignal[signal].keys())
    l = bySignal[signal]
    # observed = l["upperLimit"]
    # expected = l["expLimit"]
    # uOneSig  = l["uOneSig"]
    # uTwoSig  = l["uTwoSig"]
    # lOneSig  = l["lOneSig"]
    # lTwoSig  = l["lTwoSig"]
    extrapMax= l["extrapMax"]
    extrapMin= l["extrapMin"]
    chirality = l["chirality"]
    interference = l["interference"]


    # scaleSig=lumi/80.5

    observed = lambdaConvert.getLambdaLimit(l["upperLimit"],extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    expected = lambdaConvert.getLambdaLimit(l["expLimit"],extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    uOneSig  = lambdaConvert.getLambdaLimit(l["uOneSig"],extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    uTwoSig  = lambdaConvert.getLambdaLimit(l["uTwoSig"],extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    lOneSig  = lambdaConvert.getLambdaLimit(l["lOneSig"],extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)
    lTwoSig  = lambdaConvert.getLambdaLimit(l["lTwoSig"],extrapMin,extrapMax,channel,model=chirality,interference=interference,lumi=lumi)

    prevExp  = prevLim[l["chirality"]+"_"+l["interference"]]
    xlabels.append(l["chirality"]+" "+l["interference"])

    width=1
    plt.bar(width*i,uTwoSig-lTwoSig,edgecolor="none",width=width,bottom=lTwoSig,color="yellow",label="$2\\sigma$ Expected")
    plt.bar(width*i,uOneSig-lOneSig,edgecolor="none",width=width,bottom=lOneSig,color="green",label="$1\\sigma$ Expected")
    prev=plt.errorbar(width*i,prevExp,fmt="r",linewidth=4,xerr=width/2,label=r"MC Expected Limit ($36.1fb^{-1}$)")
    obs=plt.errorbar(width*i,observed,fmt="ko",xerr=width/2,label="New Observid Limit (MC)")
    exp=plt.errorbar(width*i,expected,fmt="k",xerr=width/2,label="New Expected Limit ($140fb^{-1}$)")
    exp[-1][0].set_linestyle('--')

    if i==0: plt.legend(fontsize=12,loc=0, frameon=False)

atlasInternal(position="nw",lumi=140,subnote="$ee$ channel\n\\emph{Simulation}")
# plt.ylabel("Signal Strength $\\mu$")
plt.ylabel("95\% CL on $\\Lambda$ [TeV]")
# plt.title(title)
plt.xticks(np.arange(0.0,len(xlabels)+0.0,1),xlabels,rotation=45)
plt.ylim(0,plt.gca().get_ylim()[1]*1.5)
# plt.yscale("log")
# plt.ylim(0.10,1600)


name="limits"
path = "plots/{0}.pdf".format(name)
plt.savefig(path,bbox_inches="tight")
path = "plots/{0}.png".format(name)
plt.savefig(path,bbox_inches="tight")
    # break




################################################################################
# Make plots
################################################################################

print ""
print "#"*80
print green("Done (any crash after this is ay-okay)")
print "#"*80
