from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import matplotlib as mpl
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import pystrap as ps
import dictionary
from math import log
import plotter
from extra import *
import poissonLimitFunctionDoubleGauss as plf
from collections import defaultdict


channel = "ee"
release = "21"
lumi = 36.1
quick = True
# quick = False
limitsOn=False
limitsOn=True

fitMin        = 0.25
fitMax        = 1.93
extrapMin     = 1.93
extrapMax     = 6
interference  = "const"
chirality     = "LL"
signalMass    = "28"

fitMinStr="{0:.2f}".format(fitMin).replace(".","p")
path="pickle/limit2d-bigRun-{0}-fitMax-extrapMin-fitMin{1}.pickle".format(channel,fitMinStr)
os.popen("rm {0}".format(path))
os.popen("rm plots/*")


# command = "python2 singleLimitTestUncert.py"
command = "python2 singleLimit.py"
command += r" quick={0}".format(quick)
command += r" plotOn={0}".format(1)
command += r" limitsOn={0}".format(limitsOn)
command += r" release=\'{0}\'".format(release)
command += r" lumi={0}".format(lumi)
command += r" channel=\'{0}\'".format(channel )
command += r" path=\'{0}\'".format(path)
# ranges
command += r" fitMin={0}".format(fitMin )
command += r" fitMax={0}".format(fitMax )
command += r" extrapMin={0}".format(extrapMin )
command += r" extrapMax={0}".format(extrapMax )
# signal model
command += r" interference=\'{0}\'".format(interference)
command += r" chirality=\'{0}\'".format(chirality)
command += r" signalMass=\'{0}\'".format(signalMass)
print green(command)
# result=os.popen(command).read()
print result


print green("Done")


