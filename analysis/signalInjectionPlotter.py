from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random
import cPickle as pickle
# import pickle
sys.path=["../source"]+sys.path
from extra import *
from plotter import *
from lambdaConversion import *

from collections import defaultdict


################################################################################
# Make plot of limits for different signal injections
################################################################################

os.popen("rm plots/*")
# iPath = "pickle/sigInject-lowFit.pickle"
# iPath = "pickle/sigInject-newSigModel.pickle"
iPath = "pickle/sigInject-signalPrefit.pickle"
iPath = "pickle/sigInject-signalPrefitDest.pickle"
iPath = "pickle/sigInject-destNoSigModel.pickle"
iPath = "pickle/sigInject-allModels.pickle"
iPath = "pickle/sigInject-finalOpt.pickle"
# iPath = "pickle/sigInject-finalOptCompare.pickle"
quick = True
quick = False

ll = lambdaLimit() # class to calculate limits
limitDict = pickle.load(open(iPath,"r"))
sigTypes = sorted(limitDict.keys())
print "Signal types", sigTypes
filt = "const"
filt = "_"
filt = "X"
# filt = "dest"
# print open(iPath,"r").read()

names = []
xlist = []
ylist = defaultdict(list)
for sigType in sigTypes:
    # if "LL_const" not in sigType: continue
    # if filt not in sigType: continue
    print green("Processing signal type",sigType)
    # x axis is signal injection strength
    x = sorted(limitDict[sigType].keys())
    print "x=",x
    # x.pop(0)
    model = sigType.split("_")[0]
    interference = sigType.split("_")[1]
    channel = sigType.split("_")[2]
    tag = sigType.split("_")[3]
    # mass = sigType.split("_")[2]
    # mass = 10
    # yaxis is limit
    nSigRecover = []
    limitsSig = []
    yVals = defaultdict(list)
    for lambdaInject in x:
        # if lambdaInject<24: continue
        yVals["Injected Signal"].append(limitDict[sigType][lambdaInject]["nSigYieldSr"])
        yVals["Recovered Signal"].append(limitDict[sigType][lambdaInject]["nObsYieldSr"]-limitDict[sigType][lambdaInject]["nFitYieldSr"])
        yVals["Lambda"].append(lambdaInject)
        yVals["tag"].append(tag)

        # continue

        print red(limitDict[sigType][lambdaInject].keys())
        limitOn = "expLimit" in limitDict[sigType][lambdaInject].keys()
        if limitOn:
            expLimit=limitDict[sigType][lambdaInject]["expLimit"]
            obsLimit=limitDict[sigType][lambdaInject]["upperLimit"]
            np1 = limitDict[sigType][lambdaInject]["npWidth1"]
            np2 = limitDict[sigType][lambdaInject]["npWidth2"]
            nSigUncert = np.sqrt(np1*np1+np2*np2)
        extrapMin = limitDict[sigType][lambdaInject]["extrapMin"]
        extrapMax = limitDict[sigType][lambdaInject]["extrapMax"]
        lumi      = limitDict[sigType][lambdaInject]["lumi"]
        # lumi = 36.1

        # scaleSig  = lumi/36.1
        scaleSig  = lumi/80
        if limitOn:
            expLimitLambda = ll.getLambdaLimit(obsLimit,extrapMin,extrapMax,channel,model=model,interference=interference,scaleSig=scaleSig)
            obsLimitLambda = ll.getLambdaLimit(expLimit,extrapMin,extrapMax,channel,model=model,interference=interference,scaleSig=scaleSig)

        # record values
        yVals["nObs"].append(limitDict[sigType][lambdaInject]["nObsYieldSr"])
        if limitOn:
            yVals["nSigUncert"].append(nSigUncert)
            yVals["NP 2"].append(limitDict[sigType][lambdaInject]["np2"])
            yVals["NP 1"].append(limitDict[sigType][lambdaInject]["np1"])
            yVals["Expected Limit (Lambda)"].append(expLimitLambda)
            yVals["Observed Limit (Lambda)"].append(obsLimitLambda)
            yVals["Expected Limit (Signal Events)"].append(limitDict[sigType][lambdaInject]["expLimit"])
            yVals["Observed Limit (Signal Events)"].append(limitDict[sigType][lambdaInject]["upperLimit"])
            yVals["npWidth1"].append(limitDict[sigType][lambdaInject]["npWidth1"])
            yVals["npWidth2"].append(limitDict[sigType][lambdaInject]["npWidth2"])

        # if quick: break
    # make lists
    xlist.append(np.array(x))
    for var in yVals.keys():
        ylist[var].append(yVals[var])
    names.append(sigType.replace("_"," ")+" TeV")
    if quick: break
    # break

##############
### make plots
##############
#print red(ylist.keys())
#for var in ylist.keys():
#    path="plots/signalInjection-{0}{1}.png".format(var,filt)
#    ydata = ylist[var]
#    plotXY(xlist,ydata,names,xaxis=r"CI Scale $\Lambda$",yaxis=var,path=path)
#    # if quick: break

# make plot of nSigRecover vs nSigYieldSr
path="plots/signalInjection-recoVsInj-scatter.png"
xdata = ylist["Injected Signal"]
ydata = ylist["Recovered Signal"]
labels = ylist["Lambda"]
nSigUncert = ylist["nSigUncert"]
tags = ylist["tag"]
plotInjVsReco(xdata,ydata,labels,nSigUncert,names,xaxis="Signal Injected",yaxis="Signal Recovered",path=path,oneToOne=True,tags=tags)

# path="signalInjection-recoVsInj-hist.png"
# bName="Injected Signal"
# aName="Recovered Signal"
# aEntries = ylist[aName]
# bEntries = ylist[bName]
# aBins,bBins=xlist,xlist
# ratioLabel = "Recovered/Injected"
# listOfHistsRatio("Lambda",names,aBins,aEntries,aName,bBins,bEntries,bName,path=path,ratioLabel=ratioLabel)





