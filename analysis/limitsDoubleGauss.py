from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random, pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
import pystrap as ps
import dictionary
from math import log
import plotter
from extra import *
import poissonLimitFunctionDoubleGauss as plf
from lambdaConversion import *


####################################################################################################
# Limits plot with double gaussian constraint
####################################################################################################

# configure this script
os.popen("rm plots/*")
quick=True
# quick=False
release="21"
# release="prev"
fitMin = 0.25
fitMax = 1
extrapMax = 6
                    # # {"channel":"ee","model":"LL","interference":"const","prevLimExp":25.7,"extrapMin":1.47,"fitMax":1.17},
# limitsToMake = [ \
                    # {"channel":"ee","model":"LL","interference":"const","prevLimExp":25.7,"extrapMin":1.47,"fitMax":1.17},
                    # {"channel":"ee","model":"LL","interference":"const","prevLimExp":25.7,"extrapMin":1.93,"fitMax":1.93},
                    # {"channel":"ee","model":"LR","interference":"const","prevLimExp":23.9,"extrapMin":1.81,"fitMax":1.81},
                    # {"channel":"ee","model":"RL","interference":"const","prevLimExp":23.8,"extrapMin":1.81,"fitMax":1.81},
                    # {"channel":"ee","model":"LL","interference":"dest", "prevLimExp":19.9,"extrapMin":2.69,"fitMax":1.12},
                    # {"channel":"ee","model":"LR","interference":"dest", "prevLimExp":21.2,"extrapMin":2.69,"fitMax":1.12},
                    # {"channel":"ee","model":"RL","interference":"dest", "prevLimExp":21.2,"extrapMin":2.69,"fitMax":1.12},
               # ]

limitsToMake = [ \
                    {"channel":"ee","model":"LL","interference":"const","prevLimExp":25.7,"extrapMin":1.93,"fitMax":1.93},
                    {"channel":"ee","model":"LR","interference":"const","prevLimExp":23.9,"extrapMin":1.81,"fitMax":1.81},
                    {"channel":"ee","model":"RL","interference":"const","prevLimExp":23.8,"extrapMin":1.81,"fitMax":1.81},
                    {"channel":"ee","model":"LL","interference":"dest", "prevLimExp":19.9,"extrapMin":2.69,"fitMax":1.12},
                    {"channel":"ee","model":"LR","interference":"dest", "prevLimExp":21.2,"extrapMin":2.69,"fitMax":1.12},
                    {"channel":"ee","model":"RL","interference":"dest", "prevLimExp":21.2,"extrapMin":2.69,"fitMax":1.12},
               ]




default = {\
           "signalInjectScale":0,
           "fitMin":fitMin,"extrapMax":extrapMax,"interpLeadingCoef":False,
          }
# lumi = 140
lumi = 36.1
table = ""

lambdaConvert = lambdaLimit() # class to calculate limits
for planI,limitPlan in enumerate(limitsToMake):
    # update default
    default["fitMax"]    = limitPlan["fitMax"]
    default["extrapMin"] = limitPlan["extrapMin"]
    model                = limitPlan["model"]
    channel              = limitPlan["channel"]
    interference         = limitPlan["interference"]
    extrapMin = default["extrapMin"]
    extrapMax = default["extrapMax"]
    default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
    default["backgroundTemplate"] = dictionary.data[release][channel]["backgroundTemplate"]
    default["backgroundTemplate"] += ":rescale{0}".format(lumi/140)

    ########################################
    # 1)
    # nominal fit, to get yields, spur sig, etc
    ########################################
    nominal = ps.extrapolation(default)
    nominal.fit()
    path = "plots/nominal-{0}-{1}-{2}.png".format(channel,model,interference)
    # plotter.plotMplRatio(nominal,path=path,logx=True,ratioMin=0.5,ratioMax=1.5)
    nominalYields = nominal.yields()

    ########################################
    # 2)
    # toys, to get toy spur sig, etc
    ########################################
    # i=0
    # while 1:
    #     i+=1
    #     print red("making toy"), green(i)
    #     nominal._throwToy(1)
    #     nominal._toyIndex+=1
    if quick:
        n=50
    else:
        n=200
    for i in range(n):
        print red("Fitting toy {0}/{1}".format(i,n))
        nominal.fitToy()
    path = "plots/toys-{0}-{1}-{2}.png".format(channel,model,interference)
    # plotter.plotToys(nominal,path=path,logx=True,ratioMin=0.5,ratioMax=1.5)
    # plotter.plotAllToys(nominal,path=path)
    # plotter.plotToyParamHist(nominal,path=path)
    # nominal.printFitResults()
    # break

    ########################################
    # 3)
    # fit mc variations, to get those
    ########################################
    mcExtraps = []
    mcYields = {}
    for i, mcName in enumerate(dictionary.pdfNames[release]):
        default["backgroundTemplate"] = dictionary.data[release][channel][mcName]
        default["backgroundTemplate"]+= ":rescale{0}".format(lumi/140)
        mcExtraps.append(ps.extrapolation(default))
        mcExtraps[-1].fit()
        mcYields[mcName] = mcExtraps[-1].yields()
        # make plot
        path = "plots/mcVar-{0}-{1}-{2}-var{3}.png".format(channel,model,interference,i)
        plotter.plotMplRatio(mcExtraps[-1],path=path,logx=True,ratioMin=0.5,ratioMax=1.5)
        # if quick: break
    path = "plots/mc-{0}-{1}-{2}.png".format(channel,model,interference)
    # plotter.plotPdfs(nominal,mcExtraps,path=path,logx=True,ratioMin=0.5,ratioMax=1.5)
    # plotter.plotMcParamHist(nominal,mcExtraps,path=path)

    ########################################
    # 4)
    # calculate limits
    ########################################
    # uncertainties
    # fit SS is std of toy fit diffs
    fitSs = np.array(nominal.toyFitDiff()).std()
    # mc SS is envelope of spur sig's of MC variations
    mcSs = max([abs(mcYields[k]["nSpur"]) for k in mcYields.keys()])
    mcSs = max(mcSs,nominalYields["nSpur"])
    nFit = nominalYields["nFit"]
    nSig = nominalYields["nSig"]
    nObs = nominalYields["nObs"]
    limitResult=plf.poissonLimit(nFit, nSig, nObs, abs(fitSs),abs(mcSs))

    ########################################
    # 5)
    # convert to Lambda
    ########################################
    scaleSig=lumi/36.1
    l = limitResult
    # convert limits
    observed = lambdaConvert.getLambdaLimit(l["upperLimit"],extrapMin,extrapMax,model=model,interference=interference,scaleSig=scaleSig)
    expected = lambdaConvert.getLambdaLimit(l["expLimit"],extrapMin,extrapMax,model=model,interference=interference,scaleSig=scaleSig)
    uOneSig  = lambdaConvert.getLambdaLimit(l["uOneSig"],extrapMin,extrapMax,model=model,interference=interference,scaleSig=scaleSig)
    uTwoSig  = lambdaConvert.getLambdaLimit(l["uTwoSig"],extrapMin,extrapMax,model=model,interference=interference,scaleSig=scaleSig)
    lOneSig  = lambdaConvert.getLambdaLimit(l["lOneSig"],extrapMin,extrapMax,model=model,interference=interference,scaleSig=scaleSig)
    lTwoSig  = lambdaConvert.getLambdaLimit(l["lTwoSig"],extrapMin,extrapMax,model=model,interference=interference,scaleSig=scaleSig)
    # add info to dictionary
    limitsToMake[planI]["limits"] = {}
    limitsToMake[planI]["limits"]["observed"] = observed
    limitsToMake[planI]["limits"]["expected"] = expected
    limitsToMake[planI]["limits"]["uOneSig"]  = uOneSig
    limitsToMake[planI]["limits"]["uTwoSig"]  = uTwoSig
    limitsToMake[planI]["limits"]["lOneSig"]  = lOneSig
    limitsToMake[planI]["limits"]["lTwoSig"]  = lTwoSig

    tp = 3
    table += "channel:{0} &".format(sround(channel,tp))
    table += "model:{0} &".format(sround(model,tp))
    table += "interference:{0} &".format(sround(interference,tp))
    table += "nSig:{0} &".format(sround(nSig,tp))
    table += "nFit:{0} &".format(sround(nFit,tp))
    table += "nObs:{0} &".format(sround(nObs,tp))
    table += "fitSs:{0} &".format(sround(fitSs,tp))
    table += "mcSs:{0} &".format(sround(mcSs,tp))
    table += "expectedLimit:{0} &".format(sround(expected,tp))
    table += "\n"

    print red(nFit, nSig, nObs, abs(fitSs),abs(mcSs))
    print green(limitResult["expLimit"])
    print green("expected limit",expected)
    print yellow("extrapMin",extrapMin)
    print yellow("extrapMax",extrapMax)
    print yellow("model",model)
    print yellow("interference",interference)
    print yellow("scaleSig",scaleSig)

    if quick: break

########################################
# 6)
# Make plot
########################################
# if not quick: plotter.multiChanLimitPlot(limitsToMake,lumi)
plotter.multiChanLimitPlot(limitsToMake,lumi)

print red("TABLE")
print table
print red("/TABLE")

print ""
print "#"*80
print green("Done (any crash after this is ay-okay)")
print "#"*80

