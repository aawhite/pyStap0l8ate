import os, sys
import numpy as np
# sys.argv.append("-b")
sys.path=["../source"]+sys.path

####################################################################################################
# Test script for pyStrap
# The test here is to loop over several fitMax values, make plots and validate the extrapolation
####################################################################################################

import pystrap as ps


default = {\
           "backgroundModel":"EXPR::diphoton5('((1 - ((x/13.)^(0.5)))^(1.*diphotonLeadingCoef))*(x^(p1 + p2*log(x/13.) + p3*(log(x/13.)^2) + p4*(log(x/13.)^3)))*0.002495/(pow(0.091187-x,2)-pow(0.002495,2))',x,diphotonLeadingCoef[0],p1[-4.5000e+00,-10,1],p2[-3.0442e+00,-5, 5.],p3[-9.29259e-01,-1.,1.],p4[-8.51735e-02,-1.,1.])",
           "backgroundTemplate":"input/RooSwiftResults_swiftHist.root:swiftHist:TeV",
           "fitMin":0.15,"extrapMin":1,"extrapMax":6,
           "interpLeadingCoef":True,
           }

# clean plots directory
os.popen("mkdir plots/; rm plots/*")

# launch extrapolation 
extrap = ps.extrapolation(default,fitMax=1)

# loop over various fit ranges
for fitMax in np.arange(0.5,2,0.5):
    extrap.update(fitMaxVal=fitMax,extrapMinVal=fitMax)
    extrap.fit()
    specialPath="plots/fitMax-{0}.png".format(str(fitMax).replace(".","p"))
    extrap.plot(mode="simple",path=specialPath)
    # break

# needed to clean up pyroot
del extrap
print "Done"
