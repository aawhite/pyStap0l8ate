from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random, pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path=["../analysis"]+sys.path
import pystrap as ps
import dictionary
from math import log
import plotter
from extra import *

####################################################################################################
# Test script for pyStrap
####################################################################################################

import pystrap as ps

# the syntax for inputs is:
# path:[th1f Name]:[GeV or TeV]:[rebin number]

# default settings stored in dictionary
channel="ee"
lumi=36.1
release="21"
# release="prev"
fitMin = 0.25
fitMax = 1
extrapMax = 6

default = {\
           "signalInjectScale":0,
           "fitMin":fitMin,"extrapMax":extrapMax,"interpLeadingCoef":False,
          }

default["fitMax"]    = 1.5
default["extrapMin"] = 1.5
default["fitMax"] = 1.5

#bkg Settings
default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
default["backgroundTemplate"] = dictionary.data[release][channel]["backgroundTemplate"]
default["backgroundTemplate"] += ":rescale{0}".format(lumi/140)

# signal settings:
chirality = "LL"
interference = "const"
signalMass = 28
signalName                      = "{0}_{1}_{2}".format(chirality,interference,signalMass)
signalHistName                 = "diff_{0}_TeV".format(signalName)
default["signalTemplate"]      = "../data/ci/CI_Template_{0}.root:{1}:GeV:10".format(chirality,signalHistName)
default["signalTemplate"]     += ":rescale{0}".format(lumi/36.1)
#set to 0 to not inject anything
default["signalInjectScale"]   = 0

e = ps.extrapolation(default)
e.fit()


# make simple diagnostic plot
os.popen("mkdir plots/; rm plots/*")
path = "plots/nominal.png"
plotter.plotMplRatio(e,path=path,logx=True,ratioMin=0.0,ratioMax=1.5)


# needed to clean up pyroot
print ""
print "#"*80
print green("Done (any crash after this is ay-okay)")
print "#"*80
print "Done"
