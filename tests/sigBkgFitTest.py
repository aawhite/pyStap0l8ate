from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path=["../analysis"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import pystrap as ps
import dictionary
from math import log
from extra import *
import poissonLimitFunctionDoubleGauss as plf
import plotter

################################################################################
# Test implementation of fit using S+B implementation in pyStrap
################################################################################

quick=True
release="21"
lumi=36.1
channel="ee"
fitMin = 0.25
fitMax = 1.93
extrapMin = 1.93
extrapMax = 6
chirality="LL"
interference="const"
signalMass=28
os.popen("rm plots/*")

default = {\
           "signalInjectScale":0,
           "fitMin":fitMin,"fitMax":fitMax,"extrapMin":extrapMin,"extrapMax":extrapMax,
           "interpLeadingCoef":False,
           }
latexChanName = channel.replace("mm",r"\mu\mu")
# background setup
backgroundModel = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,LCF)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,LCF[6.8805,0.0001,30],p0[-1.0870e+01,-30.,10.],p1[-4.1732e+00,-10.,5.],p2[-9.5372e-01,-5.,1.],p3[-8.9194e-02,-1.,0.1])"
# backgroundModel = "EXPR::backgroundModel('pow(1-log(2.718*x/13),p)',x,p[0,-10,1000])"
# backgroundModel = "EXPR::backgroundModel('pow(1-log(2.718*x/13),p)+f*(b1+x*b2)',x,p[0,-10,1000],b1[1,1,10],b2[-1,-10,10],f[0,0,1])"
# backgroundModel = "SUM::backgroundModel(EXPR::bkg('pow(1-log(2.718*x/13),p)',x,p[0,-10,1000]),frac_bg[0.1,0,1]*EXPR::sig('b1+x*b2',x,b1[1,1,10],b2[-1,-10,10]))"
# backgroundModel                = "EXPR::backgroundModel('b1',x,b1[5,0,10])"
# backgroundModel = "EXPR::backgroundModel('x/13',x)"
default["backgroundModel"]     = backgroundModel
default["backgroundTemplate"]  = dictionary.data[release][channel]["backgroundTemplate"]
default["backgroundTemplate"] += ":rescale{0}".format(lumi/140)
# signal setup
signaName                      = "{0}_{1}_{2}".format(chirality,interference,signalMass)
signalHistName                 = "diff_{0}_TeV".format(signaName)
# signalModel                    = "EXPR::signalModel('b1+x*b2',x,b1[1,1,10],b2[-1,-10,10])"
# signalModel                    = "EXPR::signalModel('exp(-b1*x/13)',x,b1[1,1,10])"
signalModel                    = "EXPR::signalModel('pow(x/13,b1)',x,b1[1,1,10])"
# signalModel                    = "EXPR::signalModel('s1',x,s1[1,0,10])"
# signalModel                    = "EXPR::signalModel('b1*pow(1-pow(b2*x/6,2),1)',x,b1[1,0,10],b2[0,0,10])"
# signalModel                    = "EXPR::signalModel('b1*(1-pow(x/6,b2))',x,b1[1,0,10],b2[0,0,1])"
default["signalModel"]         = signalModel
default["signalTemplate"]      = "../data/ci/CI_Template_{0}.root:{1}:GeV:10".format(chirality,signalHistName)
default["signalTemplate"]     += ":rescale{0}".format(lumi/36.1)
default["signalInjectScale"]   = 1


########################################
# 1)
# Nominal fit
########################################
nominal = ps.extrapolation(default)
nominal.fit()
# nominalYields = nominal.yields()
path = "plots/testSb.png"
# plotter.plotMplRatio(nominal,path=path,logx=True,ratioMin=0.0,ratioMax=1.5)
# path = "plots/testSbNoSig.png"
# plotter.plotMplRatioNoSig(nominal,path=path,logx=True,ratioMin=0.0,ratioMax=1.5)
plotter.plotMplRatioLxp(nominal,path=path,logx=True,ratioMin=0.0,ratioMax=1.5)


print green("DONE"*20)
