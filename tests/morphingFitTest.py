from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path=["../analysis"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import dictionary
from extra import *
from ROOT import *
from ROOT.RooStats import *
import roowrap as rw
try:
    from matplotlib.ticker import ScalarFormatter
    import matplotlib.ticker as ticker
except: pass
import matplotlib.pyplot as plt
import matplotlib as mpl
import plotter
import modelConstructor
import pystrap as ps



################################################################################
# Test script to implement new morphing signal shape
################################################################################

os.popen("rm plots/*")
interference = "const"
chirality = "LL"
channel = "ee"
release = "21"
lumi = 36
fitMin = 0.2
fitMax = 1
extrapMin = fitMax
extrapMax = 6

default = {\
           "signalInjectScale":0,
           "fitMin":fitMin,"fitMax":fitMax,"extrapMin":extrapMin,"extrapMax":extrapMax,
           "interpLeadingCoef":False,
           }


latexChanName = channel.replace("mm",r"\mu\mu")
default["backgroundModel"]    = dictionary.data[release][channel]["backgroundModel"]
default["backgroundTemplate"] = dictionary.data[release][channel]["backgroundTemplate"]
default["backgroundTemplate"]+= ":rescale{0}".format(lumi/140)
# signal model setup
signalPdf, signalPdfNorm = modelConstructor.morphedSignal(interference,chirality,channel=channel,lumi=lumi)
# signalPdf = None
default["signalModel"]         = signalPdf
default["signalNorm"]          = signalPdfNorm

signalMass = 20
signaName                      = "{0}_{1}_{2}".format(chirality,interference,signalMass)
signalHistName                 = "diff_CI_{0}_TeV".format(signaName)
default["signalTemplate"]      = "../data/ci/templates_r21_ee/CI_Template_{0}.root:{1}:GeV:10".format(chirality,signalHistName)
default["signalTemplate"]     += ":rescale{0}".format(lumi/80.5)
default["signalInjectScale"]   = 1

nominal = ps.extrapolation(default)

nominal.get("lambda").setVal(signalMass)

# the fit is slow, you can comment this if you want
# nominal.fit()


plotPath = "plots/nominal-{0}-{1}.png".format(chirality,interference)
plotter.plotMplRatio(nominal,path=plotPath,logx=True,ratioMin=0.0,ratioMax=1.5,lumi=lumi)

print green("DONE")
