from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path=["../analysis"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import dictionary
from extra import *
from ROOT import *
from ROOT.RooStats import *
import roowrap as rw
try:
    from matplotlib.ticker import ScalarFormatter
    import matplotlib.ticker as ticker
except: pass
import matplotlib.pyplot as plt
import matplotlib as mpl
import plotter
import modelConstructor


###################################################
## load intepolation module:
#from ROOT import gROOT
#gROOT.LoadMacro("../source/hmorph.cxx")
#from ROOT import HMorph
###################################################


################################################################################
# Test of a way to interpolate between signal histograms
################################################################################

def morphPlot(pdf1,name1,pdf2,name2,morphPdfs,morphingVals,norms,tag="",mode=None,lumi=None):
    """ Diagnostic plot """
    print yellow("=== Plotting",tag,"===")
    plt.figure(figsize=(5,5))

    if mode==None: path = "plots/output.png"
    else: path= "plots/morph-{0}-{1}-{2}".format(*mode.values())

    # normalizations
    plotMinX = 1
    plotMaxX = 6
    rebin = 200


    for i in range(len(morphPdfs)):
        morphPdfs[i].cut(minRange=plotMinX,maxRange=plotMaxX)
    pdf1.cut(minRange=plotMinX,maxRange=plotMaxX)
    pdf2.cut(minRange=plotMinX,maxRange=plotMaxX)

    # for i in range(len(morphPdfs)):
    #     morphPdfs[i].normalize(1,minRange=0,maxRange=plotMaxX)
    # pdf1.normalize(1,minRange=0,maxRange=plotMaxX)
    # pdf2.normalize(1,minRange=0,maxRange=plotMaxX)

    for i in range(len(morphPdfs)):
        # morphPdfs[i].normalize(1,minRange=0,maxRange=plotMaxX)
        morphPdfs[i].scale(norms[i])
        # morphPdfs[i].normalize(norms[i],minRange=0,maxRange=plotMaxX)
        # morphPdfs[i].normalize(norms[i],minRange=0,maxRange=plotMaxX)

    for i in range(len(morphPdfs)):
        morphPdfs[i].rebin(rebin)
        # morphPdfs[i].y = [max(0,j) for j in morphPdfs[i].y]
    pdf1.rebin(rebin)
    pdf2.rebin(rebin)

    for i,pdfm in enumerate(morphPdfs):
        # plt.plot(pdfm.x,pdfm.y,marker=".",linestyle=" ",alpha=0.5,label=r"Morphed $\Lambda=${0:.1f}".format(morphingVals[i]))
        plt.plot(pdfm.x,pdfm.y,marker=".",linestyle=" ",alpha=0.5)
    plt.plot([],[],marker=".",linestyle=" ",alpha=0.5,label="Interpolation")
    plt.plot(pdf1.x,pdf1.y,"ro",label=name1.replace("_"," ").replace("diff","MC"))
    plt.plot(pdf2.x,pdf2.y,"bo",label=name2.replace("_"," ").replace("diff","MC"))


    plt.yscale('log')
    plt.xscale('log')
    yrange = plt.gca().get_ylim()
    plt.ylim(top=yrange[1]*1000)
    plt.ylim(bottom=yrange[0]/10)

    plotter.ticksInside(removeXLabel=False)
    subnote=""
    if mode!=None:
        subnote=" ".join(mode.values())
    subnote = subnote.replace("mm",r"$\mu\mu$")
    subnote = subnote.replace("ee",r"$ee$")
    plotter.atlasInternal(position="nw",lumi=lumi,subnote=subnote)
    # plt.ylabel("(Fit-Data)/Fit",fontsize=12)
    plt.xlabel(r"$m_{ll}$ TeV",fontsize=12)
    plt.xscale('log')
    ax = plt.gca()
    ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
    ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.1f"))
    ax.set_xticks([plotMinX,3,plotMaxX])
    #plt.xlim((plotMinX,plotMaxX))
    ##Get correct ticks
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    plt.legend(fontsize=8,frameon=False)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")



def add(x):
    global w
    getattr(w,"import")(x)

def sadd(x):
    global w
    print yellow("Adding:",x)
    getattr(w,"factory")(x)

def get(x):
    global w
    return w.obj(x)

def histToRdh(hist):
    """ Convert histogram to RooDataHist, return RDH """
    name=hist.GetName()
    title=hist.GetTitle()
    # rescale
    factor = 1/1000
    minimum = hist.GetBinLowEdge(1)*factor
    maximum = hist.GetBinLowEdge(hist.GetNbinsX()+1)*factor
    rescaled = TH1D(name,title,hist.GetNbinsX(),minimum,maximum)
    for i in range(0,hist.GetNbinsX()+1):
        c = hist.GetBinCenter(i)
        v = hist.GetBinContent(i)
        rescaled.Fill(c*factor, v)
    # convert
    x    = get("x")
    name+="_rdh"
    rdh  = RooDataHist(name,name,RooArgList(x),rescaled)
    rdh.Print()
    return rdh, rescaled

def scaleNorm(hist,norm):
    """ Convert histogram to RooDataHist, return RDH """
    from ROOT import TH1D
    for i in range(0,hist.GetNbinsX()+1):
        v = hist.GetBinContent(i)
        v*=norm
        hist.SetBinContent(i,v)
    return hist

def noNeg(hist):
    for i in range(1,hist.GetNbinsX()-1):
        c = hist.GetBinContent(i)
        if c<0:
            hist.SetBinContent(i,0)
    return hist


for param in sys.argv:
    if "=" not in param: continue
    name = param.split("=")[0]
    val = param.split("=")[1]
    print yellow(param, "{0}={1}".format(name,val))
    try:
        exec("{0}={1}".format(name,val))
    except:
        exec("{0}='{1}'".format(name,val))


# make workspace
lumi=140

w = RooWorkspace("w","w")
sadd("x[0,1,6]")

morphPdf, signalPdfNorm = modelConstructor.morphedSignal(interference,chirality,channel=channel,lumi=lumi)
add((morphPdf, signalPdfNorm ))
# add(signalPdfNorm)
# add(morphPdf)

morphPdf=get("morph")
x = get("x")


# load hist
signalPath = "../data/ci/templates_r21_{1}/CI_Template_{0}.root".format(chirality,channel)
f = TFile.Open(signalPath)
hists=[]
rdhs = []
signalMasses       = [10,40]
signalHistNames    = []
for signalMass in signalMasses:
    signalName     = "{0}_{1}_{2}".format(chirality,interference,signalMass)
    signalHistName = "diff_CI_{0}_TeV".format(signalName)
    signalHistNames.append(signalHistName)
for name in signalHistNames:
    hist = f.Get(name)
    hist = noNeg(hist)
    hist = scaleNorm(hist,lumi/80.5)
    print signalPath
    print yellow(name)
    print yellow(hist)
    hists.append(hist)
    # make roofit objects
    rdh,hist = histToRdh(hists[-1])
    rhp = RooHistPdf(name+"_rhp",name+"_rhp",RooArgSet(x),rdh,0)
    sadd("{0}[{1}]".format(name+"_yld",rdh.sumEntries()))
    yld = get(name+"_yld")
    ext = RooExtendPdf(name+"_pdf",name+"_pdf",rhp,yld,"fullRange")
    # build morph
    signalMass = int(name.split("_")[4])
    x.setBins(int(hist.GetNbinsX()))
    rdhs.append(rdh)
    add(rdh)
    add(ext)
    add(rhp)

print yellow("="*20)
# quit()

i1 = 0
i2 = -1
r1 = rdhs[i1]
r2 = rdhs[i2]
pdf1 = rw.rooWrap(r1,x)
pdf2 = rw.rooWrap(r2,x)
morphPdfs = []
norms = []
morphingVals = np.arange(10,40,3)
# morphingVals = [20,24,26]
for aval in morphingVals:
    get("lambda").setVal(aval)
    morphPdfs.append(rw.rooWrap(get("morph"),x))
    norms.append(get("morphNorm").getVal())
    print red(norms[-1])
    print green(morphPdfs[-1].y)
    print yellow(morphPdfs[-1].x)
mode={
    "interference":interference,
    "chirality":chirality,
    "channel":channel}
morphPlot(pdf1,signalHistNames[i1],pdf2,signalHistNames[i2],morphPdfs,morphingVals,norms,mode=mode,lumi=lumi)

print green("DONE")
