import os, sys
import numpy as np
# sys.argv.append("-b")
sys.path=["../source"]+sys.path

####################################################################################################
# Test script for pyStrap
####################################################################################################

import pystrap as ps

####################################################################################################
# DEFAULT SETTINGS STORED IN DICTIONARY
default = {\
            # background model with wide range for diphotonLeadingCoef
           "backgroundModel":"EXPR::backgroundModel('((1 - ((x/13.)^(0.5)))^(1.*diphotonLeadingCoef))*(x^(p1 + p2*log(x/13.) + p3*(log(x/13.)^2) + p4*(log(x/13.)^3)))*0.002495/(pow(0.091187-x,2)-pow(0.002495,2))',x,diphotonLeadingCoef[5,-50,50],p1[-4.5000e+00,-10,1],p2[-3.0442e+00,-5, 5.],p3[-9.29259e-01,-1.,1.],p4[-8.51735e-02,-1.,1.])",
           "fitMin":0.1,"extrapMin":1,"extrapMax":6,"fitMax":1,
           "interpLeadingCoef":False,
           }
channel="ee"
channel="mm"
if channel=="ee":
    default["backgroundTemplate"]="../data/nominal_ee.root:nominal_ee:GeV:10"
else:
    default["backgroundTemplate"]="../data/nominal_mm.root:nominal_mm:GeV:10"
e = ps.extrapolation(default)

####################################################################################################
# LOOP OVER MULTIPLE VALUES FOR FIT MINIMUM
fitMins = np.arange(0.15,3,0.05)
# os.popen("mkdir plots; rm plots/*.png")
for fitMin in fitMins:
    # update fit minimum
    e.update(fitMin=fitMin)
    e.fit()
    # plotting details
    niceFitMin = "{0:.3f}".format(fitMin).replace(".","p")
    path = "plots/output-fitMin{0}-chan{1}.png".format(niceFitMin,channel)
    labels = []
    labels.append("Start: {0:.3f} TeV".format(fitMin))
    try:
        e.plot(path=path,mode="mplratio",logx=True,labels=labels,
               xAxisName=r"$M_{{{0}}}$ TeV".format(channel.replace("m",r"\mu")),
               xMin=0.15,ratioMin=0,ratioMax=2)
    except:
        continue
    # break
####################################################################################################

# needed to clean up pyroot
del e
print "Done"
