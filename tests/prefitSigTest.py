from __future__ import division
import os, sys, math, copy
import numpy.ma as ma
import numpy as np
import random
import cPickle as pickle
# sys.argv.append("-b")
sys.path=["../source"]+sys.path
sys.path=["../analysis"]+sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import pystrap as ps
import dictionary
from math import log
from extra import *
import poissonLimitFunctionDoubleGauss as plf
import plotter

################################################################################
# Test implementation of fit using S+B implementation in pyStrap
# prefit sig shape
################################################################################

sigFits=[]

quick=True
release="21"
lumi=36.1
channel="ee"
fitMin = 0.25
fitMax = 6
extrapMin = 6
extrapMax = 6
chirality="LL"
interference="dest"
signalMass=28
os.popen("rm plots/*")

# perofrm signal prefit

default = {\
           "signalInjectScale":0,
           "fitMin":fitMin,"fitMax":fitMax,"extrapMin":extrapMin,"extrapMax":extrapMax,
           "interpLeadingCoef":False,
           }
latexChanName = channel.replace("mm",r"\mu\mu")
# background setup
backgroundModel = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,LCF)*pow(x/13,p0+p1*log(x/13)+p2*log(x/13)^2+p3*log(x/13)^3)',x,LCF[6.8805,0.0001,30],p0[-1.0870e+01,-30.,10.],p1[-4.1732e+00,-10.,5.],p2[-9.5372e-01,-5.,1.],p3[-8.9194e-02,-1.,0.1])"
default["backgroundModel"]     = backgroundModel
default["backgroundTemplate"]  = dictionary.data[release][channel]["backgroundTemplate"]
default["backgroundTemplate"] += ":rescale{0}".format(lumi/140)
# signal setup
signalPdf = fitSignalShape(interference,chirality)
signalName                     = "{0}_{1}_{2}".format(chirality,interference,signalMass)
signalHistName                 = "diff_{0}_TeV".format(signalName)
default["signalModel"]         = signalPdf
default["signalTemplate"]      = "../data/ci/CI_Template_{0}.root:{1}:GeV:10".format(chirality,signalHistName)
default["signalTemplate"]     += ":rescale{0}".format(lumi/36.1)
default["signalInjectScale"]   = 1


########################################
# 1)
# Nominal fit
########################################
nominal = ps.extrapolation(default)
nominal.fit()

# try:
#     path = "plots/signalPrefit.png"
#     plotter.plotMplRatio(nominal,path=path,logx=True,ratioMin=0.0,ratioMax=1.5)
# except:
#     path = "plots/testSbNoSig.png"
#     plotter.plotMplRatioNoSig(nominal,path=path,logx=True,ratioMin=0.0,ratioMax=1.5)

path = "plots/signalPrefit.png"
# plotter.plotMplRatio(nominal,path=path,logx=True,ratioMin=0.0,ratioMax=1.5)
plotter.plotMplRatioLxp(nominal,path=path,logx=True,ratioMin=0.0,ratioMax=1.5)



print green("DONE"*20)
