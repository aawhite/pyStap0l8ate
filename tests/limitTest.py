import os, sys
# sys.argv.append("-b")
sys.path=["../source"]+sys.path

####################################################################################################
# Run extrap for rel20,rel21 and mm,ee channels
# make plots for each of these
####################################################################################################

import pystrap as ps

# default settings stored in dictionary
default = {\
           "backgroundModel":"EXPR::backgroundModel('((1 - ((x/13.)^(0.5)))^(1.*diphotonLeadingCoef))*(x^(p1 + p2*log(x/13.) + p3*(log(x/13.)^2) + p4*(log(x/13.)^3)))*0.002495/(pow(0.091187-x,2)-pow(0.002495,2))',x,diphotonLeadingCoef[5,-50,50],p1[-4.5000e+00,-10,1],p2[-3.0442e+00,-5, 5.],p3[-9.29259e-01,-1.,1.],p4[-8.51735e-02,-1.,1.])",
           # "backgroundModel":"EXPR::backgroundModel('((1 - ((x/13.)^(0.5)))^(1.*diphotonLeadingCoef))*(x^(p1 + p2*log(x/13.) + p3*(log(x/13.)^2) + p4*(log(x/13.)^3)))*0.002495/(pow(0.091187-x,2)-pow(0.002495,2))',x,diphotonLeadingCoef[-999,999],p1[-4.5000e+00,-10,1],p2[-3.0442e+00,-5, 5.],p3[-9.29259e-01,-1.,1.],p4[-8.51735e-02,-1.,1.])",
           "signalTemplate":"../data/CI_Template_lin.root:diff_LL_const_24_TeV:GeV:10",
           "signalInjectScale":0,
           "fitMin":0.30,"extrapMin":1,"extrapMax":6,"fitMax":1,
           "interpLeadingCoef":False,
           }

os.popen("rm plots/*")
doMu=False
doRel21=False
xAxisName = [r"$M_{ee}$ TeV",r"$M_{\mu\mu}$ TeV"][doMu]
if doRel21:
    if doMu:
        default["backgroundTemplate"]="../data/nominal_mm.root:nominal_mm:GeV:10"
    else:
        default["backgroundTemplate"]="../data/nominal_ee.root:nominal_ee:GeV:10"
else:
    if doMu:
        default["backgroundTemplate"]="../data/mergedHistos_mm.root:mergedSpectrum_total:GeV:10"
    else:
        default["backgroundTemplate"]="../data/mergedHistos_ee.root:mergedSpectrum_total:GeV:10"


e = ps.extrapolation(default)
e.fit()
e.limits()

# make simple diagnostic plot
e.plot(path="plots/output.png",mode="limit",title="Limit for LL_const_24_TeV")

# needed to clean up pyroot
del e
# quit()

print "Done"
