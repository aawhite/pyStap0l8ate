import os


os.popen("rm plots/*")

for chirality in ["LL","LR","RL","RR"]:
    for channel in ["ee","mm"]:
        for interference in ["const","dest"]:
            command = "python2 integralMorph.py "
            command += r" channel=\'{0}\'".format(channel )
            command += r" interference=\'{0}\'".format(interference)
            command += r" chirality=\'{0}\'".format(chirality)
            print command
            os.popen(command)

