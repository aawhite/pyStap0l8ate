from __future__ import division

try:
    import plotter
    hasPlotting=True
except:
    hasPlotting=False

from extra import *

################################################################################
# This script helps set up the model
################################################################################

fitSignalShape_sigFits=[]
def fitSignalShape(interference,chirality,channel="ee",plotOn=False,pathPrefix=""):
    """ Utility for getting signal model
        * If interference == const: returns fitted PDF to signal type
        * if interference == dest:  returns fitted PDF in particular range
    """
    if interference=="const":
        signalMass=20
        return fitSignalShapeBackend(signalMass,interference,chirality,channel=channel,plotOn=plotOn,pathPrefix=pathPrefix)
    elif interference=="dest":
        return None
        signalMass=20
        return fitSignalShapeBackend(signalMass,interference,chirality,fitMin=1.7,channel=channel,plotOn=plotOn,pathPrefix=pathPrefix)
    else:
        raise BaseException("Invalid interference for fitSignalShape: {0}".format(interference))

def fitSignalShapeBackend(signalMass,interference,chirality,channel="ee",
                          fitMin=0.25,fitMax=6,plotOn=False,
                          pathPrefix="",
                         ):
    """ Utility for fitting signal shape
        Returns PDF of fitted signal, which can be used for a signal model
    """
    from ROOT import RooArgSet
    import pystrap as ps
    global fitSignalShape_sigFits
    default = {\
               "signalInjectScale":0,
               "fitMin":fitMin,"fitMax":fitMax,
               "extrapMin":6,"extrapMax":6,
               "interpLeadingCoef":False,
               }
    # background setup
    backgroundModel = "EXPR::backgroundModel('(0.0024952/(pow(0.0911876-x,2)+pow(0.0024952,2)))*pow(1-x/13,sLCF)*pow(x/13,s0+s1*log(x/13)+s2*log(x/13)^2+s3*log(x/13)^3)',x,sLCF[6.8805,0.0001,30],s0[-1.0870e+01,-30.,10.],s1[-4.1732e+00,-10.,5.],s2[-9.5372e-01,-5.,1.],s3[-8.9194e-02,-1.,0.1])"
    default["backgroundModel"]      = backgroundModel
    # signal setup
    signalName                      = "{0}_{1}_{2}".format(chirality,interference,signalMass)
    signalHistName                  = "diff_CI_{0}_TeV".format(signalName)
    default["backgroundTemplate"]   = pathPrefix+"../data/ci/templates_r21_{2}/CI_Template_{0}.root:{1}:GeV:10".format(chirality,signalHistName,channel)
    # Do fit
    fitSignalShape_sigFits.append(ps.extrapolation(default))
    fitSignalShape_sigFits[-1].fit()
    path      = "plots/signalFit{0}.png".format(signalName)
    labels    = ["Signal Shape: {0}".format(signalName.replace("_"," "))]
    # fix model parameters
    pdf       = fitSignalShape_sigFits[-1].get("backgroundModel")
    x         = fitSignalShape_sigFits[-1].get("x")
    rooParams = pdf.getParameters(RooArgSet(x))
    iterator  = rooParams.fwdIterator()
    while True:
        var=iterator.next()
        if not var: break
        var.setConstant()
    # diagnostic plot
    if plotOn and hasPlotting:
        path = "plots/signalFit-{0}-{1}-{2}-{3}.png".format(signalMass,interference,chirality,channel)
        plotter.plotMplRatioNoSig(fitSignalShape_sigFits[-1],path=path,logx=True,labels=labels,ratioMin=0.0,ratioMax=1.5)
    return pdf


################################################################################
# This implements the morphing signal model
################################################################################

def scaleNorm(hist,norm):
    """ Convert histogram to RooDataHist, return RDH """
    from ROOT import TH1D
    for i in range(0,hist.GetNbinsX()+1):
        v = hist.GetBinContent(i)
        v*=norm
        hist.SetBinContent(i,v)
    return hist

def scaleAxis(hist):
    """ Convert histogram to RooDataHist, return RDH """
    from ROOT import TH1D
    name=hist.GetName()
    title=hist.GetTitle()
    # rescale
    factor = 1/1000
    minimum = hist.GetBinLowEdge(1)*factor
    maximum = hist.GetBinLowEdge(hist.GetNbinsX()+1)*factor
    rescaled = TH1D(name,title,hist.GetNbinsX(),minimum,maximum)
    for i in range(0,hist.GetNbinsX()+1):
        c = hist.GetBinCenter(i)
        v = hist.GetBinContent(i)
        rescaled.Fill(c*factor, v)
    return rescaled

def noNeg(hist):
    for i in range(1,hist.GetNbinsX()-1):
        c = hist.GetBinContent(i)
        if c<0:
            hist.SetBinContent(i,0)
    return hist



modelConstructorFiles = {}
modelConstructorVars = []
modelConstructorPdfs = []
modelConstructorHists = {}
modelConstructorHasRun = False
def morphedSignal(interference,chirality,channel="ee",pathPrefix="",lumi=140):
    """ This function returns an instance of the "morph" class, a custom morphing
        PDF implementation that morphs between TH1's
    """
    global modelConstructorFiles, modelConstructorHasRun, modelConstructorPdfs, modelConstructorVars

    ##################################################
    # load intepolation module:
    from ROOT import RooRealVar, TFile, TH1D, RooGaussian
    from ROOT import gROOT
    gROOT.LoadMacro("../source/class.h")
    from ROOT import HMorph, HMorphNorm
    ##################################################

    signalPath                      = pathPrefix+"../data/ci/templates_r21_{1}/CI_Template_{0}.root".format(chirality,channel)
    # load masses
    signalMasses       = np.arange(10,41,2)
    # signalMasses       = [38,40]
    signalNames        = []
    signalHistNames    = []
    for signalMass in signalMasses:
        signalName     = "{0}_{1}_{2}".format(chirality,interference,signalMass)
        signalHistName = "diff_CI_{0}_TeV".format(signalName)
        signalNames.append(signalName)
        signalHistNames.append(signalHistName)

    # make workspace
    x = RooRealVar("x","x",0.11,0.10,6)
    l = RooRealVar("lambda","lambda",11,10,100)
    modelConstructorVars.append(x)
    modelConstructorVars.append(l)


    # load hist
    morphPdf = HMorph("morph","morph",x,l)
    morphPdfNorm = HMorphNorm("morphNorm","morphNorm",morphPdf,x,l)

    if signalPath not in modelConstructorFiles.keys():
        f = TFile.Open(signalPath)
        modelConstructorFiles[signalPath]=f

    for name in signalHistNames:
        if name not in modelConstructorHists.keys():
            hist = modelConstructorFiles[signalPath].Get(name)
            if not hist: raise BaseException("Did not find hist {0} in file".format(name))
            hist = noNeg(hist)
            hist = scaleAxis(hist)
            hist = scaleNorm(hist,lumi/80.5)
            modelConstructorHists[name] = hist
        else:
            hist = modelConstructorHists[name]
        # hist.Rebin(10)
        print signalPath
        print yellow(name)
        print yellow(hist)
        print yellow(hist.GetBinCenter(0))
        # scale hist
        # build morph
        signalMass = int(name.split("_")[4])
        morphPdf.add(hist,signalMass)

    # add a zero histogram as a test at very large lambda
    hist = hist.Clone("newZeroHist")
    [hist.SetBinContent(i,0) for i in range(hist.GetNbinsX()+1)]
    morphPdf.add(hist,100)


    # print green(morphPdfNorm.getVal())
    # print morphPdfNorm.Print(); 
    # print green("DONE")
    # quit()

    # morphPdf = RooGaussian("morph","morph",x,l,l)
    modelConstructorHasRun = True
    modelConstructorPdfs.append([morphPdf,morphPdfNorm])
    return modelConstructorPdfs[-1][0],modelConstructorPdfs[-1][1]
