from __future__ import division
import sys, time
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import numpy as np
from ROOT import *
from ROOT.RooStats import *
import numpy.ma as ma
from extra import *



####################################################################################################
# It also holds the wrapper "rooWrapper" class for taking RooFit objects and converting to np arrays
####################################################################################################

class rooWrap:
    def __init__(self,source,var=None,verbose=False,**kwargs):
        # print "__init__"
        """ Helpful function like class for extracting numpy arrays from ROOT objects like pdf, hist
            Inputs:
            * SOURCE can accept TH1, RooDataHists(needs VAR), RooAbsPdf(needs VAR)
                     or numpy.array(needs VAR)
            * VAR can accept RooRealVar, this will be used for the binning
              (Optional, can also use TH1 binnings)
              In the case of a numpy.array SOURCE, VAR is np.array of binLowEdge
              In the case of a rooWrap as VAR, binning coppied directly from this
            Outputs:
            * numpy array for bins, values based on inpus
        """
        # if self._verbose: print "*"*50
        # if self._verbose: print "DEBUG-PLOT: Starting", source
        # if not source:
            # raise BaseException("Tried to run rooWrap with undefined source")
        self._source = source
        self._var = var
        self._verbose = verbose

        if "reNorm" in kwargs.keys(): self._reNorm=kwargs["reNorm"]
        else: self._reNorm=False

        # define _type based on what kind of object source is
        self._setInputType()

        # get binning. defines _nBins, _binLowEdges, _binWidths
        self._getBinning()

        # evaluate source in bins
        self._evaluateSource()

        # public members
        self._update()
        self._source = None


    def __str__(self):
        # print "__str__"
        """ Print as string """
        self._update()
        s = ""
        s+= "Y="+str(self.y)+"\n"
        s+= "X="+str(self.x)
        return s

    def sum(self,minRange=None,maxRange=None,invert=False):
        # print "sum"
        """ return sum of self.y in range (if provided) 
            invert = True to get sum outside of this range
        """
        # TODO: this may be possible in native numpy. This can be changed later
        if minRange!=None and maxRange!=None and not invert:
            total = sum([self.y[i]    for i in range(len(self.x)) \
                                      if self.x[i]>minRange and self.x[i]<maxRange])
        elif minRange!=None and maxRange!=None and invert:
            total = sum([self.y[i]    for i in range(len(self.x)) \
                                      if self.x[i]<minRange or self.x[i]>maxRange])
        else:
            total = sum(self.y)
        return total

    def divByBinWidth(self):
        print "divByBinWidth"
        """ Divide the contents of each bin by the width of the bin 
            Useful for variable bin plotting?
        """

    def normalize(self, scale, minRange=None, maxRange=None, invert=False):
        # print "normalize"
        """ Normalize histogram to a scale 
            If minRange and maxRange are provided, normalize within these
            invert = True to get sum outside of this range
        """
        if minRange!=None and maxRange!=None:
            # scale and denom are counted between the ranges 
            denom = self.sum(minRange=minRange,maxRange=maxRange,invert=invert)
            # scale = sum([scaleRw.y[i] for i in range(len(scaleRw.x)) \
            #                           if scaleRw.x[i]>minRange and scaleRw.x[i]<maxRange])
            # denom = sum([self.y[i]    for i in range(len(self.x)) \
            #                           if self.x[i]>minRange and self.x[i]<maxRange])
        else:
            # full sum is used
            denom = self.sum()
        if denom==0:
            print 
            print yellow("#"*50)
            print yellow("Some pre-crash details")
            print yellow("Scale:", scale)
            print yellow("RooWrapper used for denominator:")
            print yellow(self)
            print yellow("Denominator:", denom)
            print yellow("minRange={0}, maxRange={1}, scale={2}, denom={3}".format(minRange,maxRange,scale,denom))
            print yellow("#"*50, "\n")
            raise BaseException("Denom=0 in normalizeTo(): can't divide by zero")
        self._binContents = self._binContents * scale / denom
        self._update()
        return scale / denom


    def normalizeTo(self, scaleRw, minRange=None, maxRange=None, invert=False):
        # print "normalizeTo"
        """ Normalize histogram to a scale based on another RW (scaleRw
            In otherwords, scale bin contents by scale/sum(binContents)
            If minRange and maxRange are provided, normalize within these
            invert = True to get sum outside of this range
        """
        if minRange!=None and maxRange!=None:
            # scale and denom are counted between the ranges 
            scale = scaleRw.sum(minRange=minRange,maxRange=maxRange,invert=invert)
        else:
            # full sum is used
            scale = scaleRw.sum()
        return self.normalize(scale,minRange=minRange,maxRange=maxRange,invert=invert)

    def scale(self,scale):
        # print "scale"
        """ Scal bin contents by input scale """
        self._binContents = self._binContents * scale
        self._update()


    def cut(self,minRange=None,maxRange=None):
        # print "cut"
        """ Cut _binCenters and _binContents, _binLowEdges, _binWidths such that the X range is between minRange and maxRange
            Either or both may be provided
        """
        self._binWidths   = self._cutArray(self._binWidths,  self._binCenters,minRange=minRange,maxRange=maxRange)
        self._binLowEdges = self._cutArray(self._binLowEdges,self._binCenters,minRange=minRange,maxRange=maxRange)
        self._binContents = self._cutArray(self._binContents,self._binCenters,minRange=minRange,maxRange=maxRange)
        # finally, trim binCenters
        self._binCenters  = self._cutArray(self._binCenters, self._binCenters,minRange=minRange,maxRange=maxRange)
        self._nBins       = len(self._binCenters)
        self._update()
        return self

    def rebin(self,n):
        # print "rebin"
        """ Rebin by factor n """
        newBinCenters  = []
        newBinContents = []
        newBinLowEdges = []
        newBinWidths   = []
        iNew = -1 # rebinned index
        # loop over existing data
        for iOld, binCenter in enumerate(self._binCenters):
            # condition for creating new bin
            if iOld%n==0:
                newBinContents.append(0)
                newBinLowEdges.append(self._binLowEdges[iOld])
                newBinCenters.append([])
                newBinWidths.append([])
                iNew+=1
            # merge into current bin
            newBinCenters[iNew]  .append(self._binCenters[iOld])
            newBinWidths[iNew]   .append(self._binWidths[iOld])
            newBinContents[iNew]+=self._binContents[iOld]
        # average bin centers
        for iBin, binList in enumerate(newBinCenters):
            newBinCenters[iBin]  = np.array(newBinCenters[iBin]).mean()
            newBinWidths[iBin]   = np.array(newBinWidths[iBin]).sum()
        # swap over new bins, bin centers
        self._binCenters         = np.array(newBinCenters[0:-1])
        self._binContents        = np.array(newBinContents[0:-1])
        self._binLowEdges        = np.array(newBinLowEdges[0:-1])
        self._binWidths          = np.array(newBinWidths[0:-1])
        # update
        self._update()




####################################################################################################
# Private functions
####################################################################################################

    def _rebinArray(self,arry,n):
        # print "_rebinArray"
        """ Rebin arry by factor n """

    def _update(self):
        # print "_update"
        """ update public parameters after a change """
        self.x = self._binCenters
        # print self._binWidths
        self.y = self._binContents

    def _cutArray(self,Y,X,minRange=None,maxRange=None):
        # print "_cutArray"
        """ Cut Y such that X range between minRange, maxRange 
            Return cut array Y
        """
        if minRange==None and maxRange==None: return Y
        elif maxRange==None: # cut only with minRange
            ymasked = ma.masked_where(x < minRange, Y, copy=True)
        elif minRange==None: # cut only with maxRange
            ymasked = ma.masked_where(X > minRange, Y, copy=True)
        else: # cut with both
            ymasked = ma.masked_where(X < minRange, Y, copy=True)
            xmasked = ma.masked_where(X < minRange, X, copy=True)
            ymasked = ma.masked_where(xmasked > maxRange, ymasked, copy=True)
        return np.array(ymasked.compressed())


    def _evaluate(self,x):
        # print "_evaluate"
        """ evaluate _source at position x based on _type 
            return value is divided per bin width
        """
        if self._type == "pdf":
            if not self._var:
                print "==== Debug: var=",self._var
                raise BaseException("rooWrap tried to eval pdf with _var not defined")
            # update var value
            self._var.setVal(x)
            # get pdf value
            if self._reNorm:
                ret = self._source.getVal(RooArgSet(self._var))
            else:
                ret = self._source.getVal()
        if self._type == "th1":
            binNumber = self._source.FindBin(x)
            binWidth  = self._source.GetBinWidth(binNumber)
            ret = self._source.GetBinContent(binNumber) / binWidth
        if self._type == "np":
            binNumber = self._source.FindBin(x)
            ret = self._source[binNumber]
        return ret

    def _evaluateSource(self):
        # print "_evaluateSource"
        """ Requires bin variables defined, source defined, _type defined
            Evaluates source in each bin
        """
        # copy source in the case of a np array as source
        if self._type == "np":
            self._binContents=self._source
            return
        # otherwise, evaluate
        binVals = []
        for iBin, binCenter in enumerate(self._binCenters):
            # instantaneous value at this point
            iVal   = self._evaluate(binCenter)
            # with of this bin
            width  = self._binWidths[iBin]
            # value for this bin
            binVal = iVal*width
            binVals.append(binVal)
        self._binContents = np.array(binVals)

    def _setInputType(self):
        # print "_setInputType"
        """ Set variable _type based on _source
            Set variable _binType based on _var
        """
        self._binType=None
        self._type   =None
        if self._verbose: print "DEBUG-PLOT: _setInputType"
        if isinstance(self._source, TH1F) or isinstance(self._source, TH1D):
            self._type = "th1"
            self._binType="th1"
        elif isinstance(self._source,RooDataHist):
            self._type = "rdh"
        elif isinstance(self._source,RooAbsPdf) or isinstance(self._source,RooAbsReal):
            self._type = "pdf"
        elif isinstance(self._source,(np.ndarray,np.generic)):
            self._type = "np"
        else:
            print "Input given:", self._source
            raise BaseException("rooWrap given invalid input type (see printout above)")

        # get type used for binning
        if self._var!=None and isinstance(self._var,RooRealVar):
            self._binType = "rooVar"
        elif self._var!=None and isinstance(self._var,(np.ndarray,np.generic)):
            self._binType = "np"
        elif self._var!=None and isinstance(self._var,rooWrap):
            self._binType = "rw"

    def _rdhToHist(self,rdh):
        # print "_rdhToHist"
        if self._verbose: print "DEBUG: _rdhToHist"
        """ Convert RooDataHist to histogram, return TH1 """
        name= rdh.GetName()+str(time.time())
        if not self._var:
            raise BaseException("rooWrap converting RDH to TH1, var not defined")
        x   = self._var
        hist = rdh.createHistogram(name,x)
        if self._verbose: print "Converted rdh to hist"
        return hist

    def _getBinning(self):
        # print "_getBinning"
        """ Get binning from RooRealVar self._var 
            Binning info consists of 1) bin lower edges, 2) bin widths
        """
        if self._verbose: print "DEBUG-PLOT: getBinning"
        if self._binType == "rooVar":
            # get bins from variable
            # require var to be defined
            if not self._var or not isinstance(self._var,RooRealVar):
                raise BaseException("Invalid RooRealVar to use for bins")
            # get binning
            binning           = self._var.getBinning()
            self._nBins       = binning.numBins()
            self._binLowEdges = [binning.binLow(i) for i in range(self._nBins)]
            self._binWidths   = [binning.binWidth(i) for i in range(self._nBins)]
        if self._binType == "np":
            raise BaseException("numpy.array binning not implemented yet in rooWrapper - you can change this!")
        if self._binType == "rw":
            self._nBins = self._var._nBins
            self._binLowEdges = self._var._binLowEdges
            self._binWidths   = self._var._binWidths
        if self._type == "rdh":
            # convert rdh to th1, and get bins from that
            if not self._var:
                raise BaseException("rooWrap given RDH, but invalid RooRealVar to use for bins")
            self._source = self._rdhToHist(self._source)
            self._type   = "th1" # this will carry on to the th1 get bins
            self._binType= "th1" # this will carry on to the th1 get bins
        if self._binType == "th1":
            # get bins from th1
            hist = self._source
            self._nBins       = hist.GetNbinsX()
            self._binLowEdges = [hist.GetBinLowEdge(i) for i in range(1,hist.GetNbinsX()+1)]
            self._binWidths   = [hist.GetBinWidth(i) for i in range(1,hist.GetNbinsX()+1)]
        self._binCenters = [self._binLowEdges[i]+self._binWidths[i]/2 for i in range(self._nBins)]
        self._binCenters = np.array(self._binCenters)
