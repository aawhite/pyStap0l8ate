from __future__ import division
import sys
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
sys.argv.append("-b")
from ROOT import TH1F, RooWorkspace, RooDataHist, RooArgList, TCanvas, RooFit, TText, TPad
from ROOT import TLine, RooArgSet, RooAddPdf,RooCategory,RooDataSet,RooCmdArg,RooSimultaneous
from ROOT import RooLinkedList, RooAbsReal, TFile
from ROOT.RooFit import *
from ROOT import kGreen, kRed, kFALSE, kBlue
from ROOT.RooStats import ModelConfig, AsymptoticCalculator,HypoTestInverter, HypoTestCalculatorGeneric
from extra import *


# poisson limit with RooStats
# model is Poisson * Gauss * Gauss

# following https://twiki.cern.ch/twiki/bin/view/RooStats/RooStatsExercisesMarch2015

def shapeLimit(extrap, npWidth1=1,npWidth2=1):
    """ calculate shape-based (multi-bin) limit
        np1 = nuissance parameter width  number:1
        np2 = nuissance parameter width  number:2
    """

    w = extrap.get("w") #RooWorkspace("w","w")
    rdhObj = extrap.get("template")
    fitObj = extrap.get("backgroundModel")
    x      = extrap.get("x")


    ##########################

    //Make muFreeHyp
    ModelConfig* muFreeHyp = new ModelConfig("muFreeHyp_" + arg.range + "_" + arg.hypo,arg.wksp);
    muFreeHyp->SetPdf(arg.pdf);
    muFreeHyp->SetParametersOfInterest("mu");
    muFreeHyp->SetNuisanceParameters("muZeroParameters");
    if(arg.wksp->cat("channel")->numTypes() > 1) muFreeHyp->SetObservables("x,channel");
    else muFreeHyp->SetObservables("x");
    ((RooArgSet*)arg.wksp->pdf(arg.pdf)->getParameters(*arg.wksp->var("x")))->setAttribAll("Constant",true);
    SetNamedSetConstant("muZeroParameters",arg.wksp,false);
    arg.wksp->var("mu")->setConstant(false);
    arg.wksp->var("mu")->setVal(0.);
    //if(arg.options.Contains("doSyst")) muFreeHyp->SetGlobalObservables("spurSigSigma_" + (TString)arg.wksp->cat("channel")->getLabel());
    if(arg.options.Contains("doSyst"))
    {
        /*
        TString constrPars = "";
        for(int i = 0; i<(int)arg.wksp->cat("channel")->numTypes(); i++)
        {
            arg.wksp->cat("channel")->setIndex(i);
            constrPars += "spurSigSigma_" + (TString)arg.wksp->cat("channel")->getLabel();
            if(i+1<(int)arg.wksp->cat("channel")->numTypes()) constrPars += ",";
        }

        muFreeHyp->SetConstraintParameters(constrPars);
        */
        muFreeHyp->SetConstraintParameters("nuisanceParameters");
    }

    muFreeHyp->SetSnapshot( RooArgSet(*arg.wksp->var("mu")) );
    cout << "Printing ModelConfig:" << endl;
    //muFreeHyp->Print();
    arg.wksp->import(*muFreeHyp);
    delete muFreeHyp;

    ########################


    # make poisson model
    #w.factory("sum:nExp(nSig[0,-300,300],prod:bkgProd(sum:bkgSum(1,np1[0,-1,1],np2[0,-1,1]),nBkg[{0}]))".format(nBkg))
    #w.var("nSig").setVal(nSig)
    #w.factory("Poisson:pois(nObs[0],nExp)");
    #w.factory("Gaussian:g1(np1,0,npWidth1[{0}])".format(npWidth1/nBkg));
    #w.factory("Gaussian:g2(np2,0,npWidth2[{0}])".format(npWidth2/nBkg));
    #w.factory("PROD:model(pois,g1,g2)");
    # w.var("npWidth1").setConstant(True);

    ###
    mc = ModelConfig("sbModel",w);
    mc.SetPdf(w.pdf("backgroundModel"));
    mc.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    mc.SetObservables(RooArgSet(w.var("nObs")));
    mc.SetNuisanceParameters(RooArgSet(w.var("np1")));
    mc.SetNuisanceParameters(RooArgSet(w.var("np2")));
    # these are needed for the hypothesis tests
    mc.SetSnapshot(RooArgSet(w.var("nSig")));
    # mc.SetGlobalObservables(w.var("nBkgExpect")); # removed etienne
    mc.Print();
    # import model in the workspace
    getattr(w,"import")(mc);
    # make data set with the namber of observed events
    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    w.var("nObs").setVal(nObs);
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);
    # ##################################################
    sbModel = mc;

    # create null hypothesis H0 (based on H)
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    bModel.SetSnapshot((RooArgSet(w.var("nSig"))));
    # fix nSig value to 0
    nSigNoSig = bModel.GetParametersOfInterest().first();
    nSigNoSig.setVal(0);
    # nSigNoSig.setConstant();
    bModel.SetSnapshot(RooArgSet(nSigNoSig));

    # do aysmtotic test
    ac = AsymptoticCalculator(data, bModel, sbModel);
    ac.SetOneSided(True);
    # AsymptoticCalculator::SetPrintLevel(-1);

    # create hypotest inverter
    hypoCalc = HypoTestInverter(ac);
    hypoCalc.SetFixedScan(1000,0.01,100, True);#added by etienne 2
    hypoCalc.SetConfidenceLevel(0.95);
    hypoCalc.UseCLs(True);
    hypoCalc.SetVerbose(False);
    # hypoCalc.SetOneSidedDiscovery(True);

    # htr = HypoTestCalculatorGeneric(hypoCalc).GetHypoTest();
    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest();
    htir = hypoCalc.GetInterval();

    # get resutls
    ret = {}
    ret["upperLimit"] = htir.UpperLimit();
    ret["lowerLimit"] = htir.LowerLimit();
    ret["expLimit"]   = htir.GetExpectedUpperLimit(0);
    ret["uOneSig"]    = htir.GetExpectedUpperLimit(1);
    ret["uTwoSig"]    = htir.GetExpectedUpperLimit(2);
    ret["lOneSig"]    = htir.GetExpectedUpperLimit(-1);
    ret["lTwoSig"]    = htir.GetExpectedUpperLimit(-2);
    ret["nExpModel"]  = w.obj("nExp").getVal()
    ret["nBkg"]       = nBkg
    ret["nObs"]       = nObs
    ret["npWidth1"]   = npWidth1
    ret["npWidth2"]   = npWidth2
    ret["nSig"]       = w.var("nSig").getVal()
    ret["np1"]        = w.var("np1").getVal()
    ret["np2"]        = w.var("np2").getVal()

    return ret


if __name__=="__main__":
    ### test
    nBkg=10
    nSig=3
    nObs=15
    np1=1
    np2=1
    limit=poissonLimit(nBkg, nSig, nObs, np1,np2)
    print "="*50
    print limit
    # print limit["nObs"]
    print limit["nExpModel"]
    print limit["nBkg"]
    print limit["nSig"]
    print limit["np1"]
    print limit["np2"]
    print "="*50
