# import pystrap as ps
import numpy as np
import os,sys
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
from ROOT import *
import cPickle as pickle


# some formatting functions

def firstUpper(s):
    return s[0].upper()+s[1:]

def yellow(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[33m{0}\033[39m".format(" ".join(string))
    return ret

def red(*string):
    """ return string as red """
    string = [str(s) for s in string]
    ret = "\033[31m{0}\033[39m".format(" ".join(string))
    return ret

def green(*string):
    """ return string as green """
    string = [str(s) for s in string]
    ret = "\033[32m{0}\033[39m".format(" ".join(string))
    return ret


plotColors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
plotColorsDotted = ['b:', 'g:', 'r:', 'c:', 'm:', 'y:', 'k:']

def sround(n,d):
    """ Round number n to d digits for a table """
    fullNumber = str(n)
    sign = ("-" in fullNumber)
    fullNumber = fullNumber.replace("-","")
    # break number into parts
    if "." in fullNumber:
        integers = fullNumber[:fullNumber.find(".")]
        decimal = fullNumber[fullNumber.find(".")+1:]
    else:
        integers = fullNumber
        decimal = None

    # remove numbers
    decimalDigits = d - len(integers)
    if decimalDigits>0 and decimal!=None:
        decimal = decimal[0:decimalDigits]
    else: decimal = None

    # reassamble number
    ret = ["","-"][sign]
    ret += integers
    if decimal != None:
        ret+="."+decimal
    return ret


