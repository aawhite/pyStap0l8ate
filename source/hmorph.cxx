#ifndef HMORPH_C
#define HMORPH_C

#include "RooFit.h"

#include "Riostream.h"
#include "Riostream.h"
#include <math.h>

#include "hmorph.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooRandom.h"
#include "RooMath.h"

using namespace std;

ClassImp(HMorph);
ClassImp(HMorphNorm);


////////////////////////////////////////////////////////////////////////////////

HMorph::HMorph(const char *name, const char *title,
          RooAbsReal& _mll, RooAbsReal& _lambda) :
  RooAbsPdf(name,title),
  mll("x","Observable",this,_mll),
  lambda("lambda","lambda",this,_lambda),
  _initialized(false)
{
    // cout<<"=========== INITIALIZE\n";
    // cout<<lambda<<endl;
    // cout<<"=========== /INITIALIZE\n";
    // this->Print();
}



////////////////////////////////////////////////////////////////////////////////

HMorph::HMorph(const HMorph& other, const char* name) :
  RooAbsPdf(other,name),
  mll("x",this,other.mll),
  lambda("lambda",this,other.lambda),
  _initialized(other._initialized)
{
    _nBins = other._nBins;
    _minBin = other._minBin;
    _maxBin = other._maxBin;
    _binWidth = other._binWidth;
    int nHists = other._lambdas.size();
    TH1* newHist;
    for (int i=0; i<nHists; i++){
        newHist = (TH1*)other._hists[i]->Clone();
        _hists.push_back(newHist);
        _lambdas.push_back(other._lambdas[i]);
        _norms.push_back(other._norms[i]);
    }
}

////////////////////////////////////////////////////////////////////////////////



// void HMorph::debug(){
//     cout<<"\n======= DEBUG =======\n";
//     cout<<"really in debug\n";
//     cout<<this<<endl;
//     cout<<"_initialized:    "<<_initialized<<endl;
//     cout<<"Number of hist   "<<_hists.size()<<endl;
//     cout<<"_nBins:          "<<_nBins<<endl;
//     cout<<"_minBin:         "<<_minBin<<endl;
//     cout<<"_maxBin:         "<<_maxBin<<endl;
//     cout<<"value mll:       "<<mll<<endl;
//     cout<<"value lambda:    "<<lambda<<endl;
//     cout<<"variable mll:    "; mll.Print();
//     cout<<"variable lambda: "; lambda.Print();
//     cout<<"current value:   "<<this->getVal()<<endl;
//     cout<<"====== \\DEBUG =======\n";
//     cout<<endl;
// }


void HMorph::add(TH1* hist, double paramValue){
    // Add a histogram associated to paramValue.
    // If not _initialized, define binning
    // If _initialized, check against binning
    bool status;
    if (!_initialized){
        status = this->_addFirst(hist,paramValue);
    }
    else{
        status = this->_addHist(hist,paramValue);
    }
    if (!status){
        cout<<"\n\n==== ERROR ADDING HIST\n";
        cout<<"==== Hist:\n";
        hist->Print();
        cout<<"\n";
    }
}

double HMorph::_interpolate(double x, double x1, double y1, double x2, double y2) const {
    // perform interpolation
    if (x2-x1==0) throw std::runtime_error("Division by zero in interpolation, x2-x1");
    double m  = (y2-y1)/(x2-x1);
    double y  = y1 + m*(x-x1);
    return y;
}

Double_t HMorph::evaluate() const{
    // if (!_initialized) return 1;
    // evaluate interpolated value at bin corresponding to mass variable mll
    // 1) find histogram index corresponding to morphing variable
    if (!_initialized) return 0;
    double morphIndex = this->_getMorphIndex();
    // 2) find bin index
    double binIndex = this->_getBinIndex();
    // 3) Interpolate
    double x1 = _lambdas[morphIndex];
    double x2 = _lambdas[morphIndex+1];
    if (_binWidth==0) throw std::runtime_error("Division by zero in evaluate, _binWidth");
    double y1 = _hists[morphIndex]->GetBinContent(binIndex)/_binWidth;
    double y2 = _hists[morphIndex+1]->GetBinContent(binIndex)/_binWidth;
    double x  = lambda;
    double y = this->_interpolate(x,x1,y1,x2,y2);

    // cout<<"\n==========================\n";
    // cout<<"binWidth      "<<_binWidth<<endl ; 
    // cout<<"y=            "<<y<<endl ; 
    // cout<<"y2=           "<<y2<<endl; 
    // cout<<"y1=           "<<y1<<endl; 
    // cout<<"x2=           "<<x2<<endl; 
    // cout<<"x1=           "<<x1<<endl; 
    // cout<<"binIndex=     "<<binIndex<<endl; 
    // cout<<"morphIndex=   "<<morphIndex<<endl;
    // cout<<"hist1:"; _hists[morphIndex]->Print();
    // cout<<"hist2:"; _hists[morphIndex+1]->Print();

    return y;
}

int HMorph::_getBinIndex() const{
    // return index of bin containing mll value
    // use 0th histo
    // double x = mll;
    return _hists[0]->FindBin(mll);
}

int HMorph::_getMorphIndex() const{
    // return index of the first _lambdas value which is below lambda->getVal()
    // if lowest _lambdas is higher, returns 0
    // if highest _lambdas is lower, returns nHists-1 (2nd last lambda index)
    double target = lambda;
    double nHists = _hists.size();
    // edge cases
    if (_lambdas[0]>=target) return 0;
    if (_lambdas[nHists-1]<=target) return nHists-2;
    // search
    for (int i=1; i<nHists; i++)
        if (_lambdas[i]>target) return i-1;
    // should not happen
    throw std::runtime_error("Could not find HMorph index");
}



bool HMorph::_addFirst(TH1* hist, double paramValue){
    // add the first histogram
    _initialized = true;
    // set bin params
    _nBins    = hist->GetNbinsX();
    _minBin   = hist->GetBinLowEdge(1);
    _maxBin   = hist->GetBinLowEdge(_nBins+1);
    _binWidth = hist->GetBinWidth(1);
    // add hist
    return this->_addHist(hist,paramValue);
}

bool HMorph::_addHist(TH1* hist, double paramValue){
    // add a histogram and param
    // check bins for compatibility
    if ( hist->GetNbinsX() != _nBins            ||
         hist->GetBinLowEdge(1) != _minBin      ||
         hist->GetBinLowEdge(_nBins+1) != _maxBin ||
         hist->GetBinWidth(1) != _binWidth
       ) { throw std::runtime_error("Issue with added histogram"); }
    // add histogram
    _hists.push_back(hist);
    _lambdas.push_back(paramValue);
    _norms.push_back(this->_th1Sum(hist));
    return true;
}

double HMorph::_th1Sum(TH1* hist){
    // return integral of th1
    double result = 0;
    for (int i=1; i<hist->GetNbinsX(); i++)
        result+=hist->GetBinContent(i);
    return result;
}

double HMorph::integral(char* rangeName) {
    // return interpolated integral
    if (!_initialized){
        cout<<"## Warning: HMorph not init ##\n";
        return 0;
    }
    // return cached value if lambda hasn't changed
    // cout<<"integral lambda:"<<lambda<<endl;
    // cout<<"integral mll   :"<<mll<<endl;
    // cout<<"_lastIntegralLambda:"<<_lastIntegralLambda<<endl;
    if (lambda == _lastIntegralLambda) {
        // cout<<"reuse integral\n";
        return _lastIntegralCache;
    }
    cout<<"calc new integral\n";
    double ret = this->analyticalIntegral(0,rangeName);
    // update state of last calc
    _lastIntegralLambda = lambda;
    _lastIntegralCache = ret;
    return ret;
}

Double_t HMorph::analyticalIntegral(Int_t code, const char* rangeName) const {

    // get range of integral: if rangeName is provided, use this
    double rMin;
    double rMax;
    if (rangeName){
        rMin = mll.min(rangeName);
        rMax = mll.max(rangeName);
    }
    else{
        rMin = mll.min();
        rMax = mll.max();
    }
    // determine indicies
    int morphIndex = this->_getMorphIndex();
    int minBin = _hists[0]->FindBin(rMin);
    int maxBin = _hists[0]->FindBin(rMax);
    double x1 = _lambdas[morphIndex];
    double x2 = _lambdas[morphIndex+1];
    double x  = lambda;
    // integrate hists
    double y1 = _hists[morphIndex]->Integral(minBin,maxBin);
    double y2 = _hists[morphIndex+1]->Integral(minBin,maxBin);
    // interpolate
    double integral = this->_interpolate(x,x1,y1,x2,y2);


    return integral;
};


#endif

