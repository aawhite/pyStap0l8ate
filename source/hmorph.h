#ifndef HMORPH
#define HMORPH

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooTrace.h"

class RooRealVar;


class HMorph : public RooAbsPdf {
public:
    HMorph() {    } ;
    HMorph(const char *name, const char *title,
           RooAbsReal& _mll, RooAbsReal& _lambda);
    HMorph(const HMorph& other, const char* name=0) ;
    virtual TObject* clone(const char* newname) const { return new HMorph(*this,newname); }
    inline virtual ~HMorph() {  }

    // specific functions
    bool _addFirst(TH1* hist, double paramValue);
    bool _addHist(TH1* hist, double paramValue);
    double _th1Sum(TH1* hist);
    int _getMorphIndex() const;
    int _getBinIndex() const;

    void debug(){
        cout<<"DEBUG HMorph:lambda = "<<lambda<<endl;
        cout<<"DEBUG HMorph:mll    = "<<mll<<endl;
    }

    double _interpolate(double x, double x1, double y1, double x2, double y2) const;


    // variables
    double _lastIntegralLambda = -1;
    double _lastIntegralCache = -1;
    int _nBins=-1;
    double _minBin=-1;
    double _maxBin=-1;
    double _binWidth=-1;
    bool _initialized;
    vector<TH1*> _hists;    // histograms
    vector<double> _lambdas; // morphing parameter value
    vector<double> _norms;  // histogram normalizaitons


    void add(TH1* hist, double paramValue);

    double getVal(){ 
        double ret = this->evaluate();
        // normalize
        // lambda=lambda*1;
        ret/=this->integral(0);
        return ret;
    };

    // void Print(const char* o = 0){ this->debug(); }
    // void Print(const char* o = 0){ cout<<"RooAbsPdf::HMorph = "<<this->getVal()<<endl; }

    // void Print(const char* o = 0){cout<<"MORPH CLASS";}

    // from roogaus
    double integral(char* rangeName=0);
    Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;
    Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const{ return 1; };
    // Int_t getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t staticInitOK=kTRUE) const{return 1;};
    // void generateEvent(Int_t code) {};
    // Double_t getLogVal(const RooArgSet* set) const {return 1;};

    // Bool_t canBeExtended() const {return kTRUE;};
    // virtual ExtendMode extendMode() const {return CanBeExtended;};
    virtual CacheMode canNodeBeCached() const { return Never ; }
    inline Bool_t isValueDirty() const { return kTRUE;}



    RooRealProxy mll ;
    RooRealProxy lambda ;

protected:
    Double_t evaluate() const ;

private:

    ClassDef(HMorph,1) // Gaussian PDF
};



// class HMorphNorm : public RooRealVar {
class HMorphNorm : public RooAbsPdf {
public:

    HMorphNorm(const char *name, const char *title, HMorph& morphInput,
               RooAbsReal& _mll, RooAbsReal& _lambda) :
        RooAbsPdf(name,title),
        // _morphInternal(&morphInput),
        _morphInternal("morph","morph",this,morphInput),
        mll("x","Observable",this,_mll),
        lambda("lambda","lambda",this,_lambda)
    { 
        _value=this->getVal();
        // _morphInternal.add(morphInput)
    };

    HMorphNorm(const HMorphNorm& other, const char* name=0) :
        RooAbsPdf(other,name),
        // _morphInternal(other._morphInternal),
        _morphInternal("morph",this,other._morphInternal),
        mll("x",this,other.mll),
        lambda("lambda",this,other.lambda)
    { _value=this->getVal();};

    virtual TObject* clone(const char* newname) const { return new HMorphNorm(*this,newname); }
    inline virtual ~HMorphNorm() {  }

    // double getVal(){return this->evaluate();};
    void debug(){
        cout<<"DEBUG HMorphNorm:lambda = "<<lambda<<endl;
        cout<<"DEBUG HMorphNorm:mll    = "<<mll<<endl;
    }

    // Double_t getValV(const RooArgSet* nset) const{ return 3; };

    // inline Bool_t isValueDirtyAndClear() const { return true; } 

    // HMorph* _morphInternal;
    RooRealProxy _morphInternal ;
    // RooListProxy _morphInternal ;

protected:
    Double_t evaluate() const {
        // _morphInternal.lambda=lambda;
        // _morphInternal.mll=mll;
        HMorph* tmp = (HMorph*)_morphInternal.absArg();
        _value      = tmp->integral(0);
        return _value;
    };

    RooRealProxy mll ;
    RooRealProxy lambda ;

private:
    ClassDef(HMorphNorm,1) // Gaussian PDF
};

#endif
