from __future__ import division
import sys,os, math, copy
sys.path = [os.path.realpath(__file__)] + sys.path
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
from ROOT import *
from ROOT.RooStats import *
import ROOT
import numpy as np
import numpy.ma as ma
# import poissonLimitFunction
import poissonLimitFunctionDoubleGauss as poissonLimitFunction
import roowrap as rw
from extra import *
from random import gauss
from math import sqrt
from ROOT.RooFit import RecycleConflictNodes

ROOT.RooMsgService.instance().setGlobalKillBelow(RooFit.WARNING)

###################################################
## load intepolation module:
#from ROOT import gROOT
## gROOT.LoadMacro("/home/prime/dilepton/pyStap0l8ate/source/morph.C")
#gROOT.LoadMacro("../source/morph.C")
#from ROOT import morph
###################################################


import random
random.seed(1)
np.random.seed(1)

from extra import green
print green("Loading pystrap")

####################################################################################################
# Main interface for pystrap
# Extrapolation is responsible for a *single* extrapolation
####################################################################################################
# The first set of functions (__init__, setExtrap, fit, singleBinTest, limits, plots) are the
# public interface for the class. Following these are a bunch of utilities for loading and fitting
# In the next update, these will be moved to separate files
# A straightforward example of how to run an extrapolation can be found in ../tests
####################################################################################################

def n():
    import time
    return str(time.time())

class extrapolation:

    def __init__(self,inputData={},**kwargs):
        # print "DEBUG: __init__"
        """ The following inputs can be used: 
                * inputData, a dictionary of inputs
                * key word arguments, which override the values in the dictionary
            The potential inputs are:
                * backgroundTemplate  #     Required # Histogram of background events
                * backgroundModel     #     Required # Pdf for background (ie diphoton5)
                * fitMin              #     Required # Minimum value for fit
                * fitMax              #     Required # Maximum value for fit
                * signalTemplate      # Not-required # Histogram of signal events
                * signalModel         # Not-required # Pdf for signal (ie gaussian)
                * extrapMin           # Not-required # Minimum value for extrapolation
                * extrapMax           # Not-required # Maximum value for extrapolation
                * interpLeadingCoef   #   Recomended # Bool to do p0/leading interpolation
            The NAME is important: Templates are loaded as TH1, Models are rooPDF's
                                   The string defining a TH1 template is as follows:
                                      "rootFileName.root:nameOfTh1:Units"
                                   Units may be GeV or TeV
                                   There are optional commands to add at the end of the string
                                   For example, you can add ":rebin10" to rebin by /10
                                                you can add ":rescale2" to rescale by x2

            The main ranges are:
                * fullRange           # from fitMin to max(fitMax,extrapMax)
                # * tempRange           # taken from template variable range
                * fitRange            # defined by inputs
                * extrapRange         # defined by inputs

            The Toys work like this. Call fitToy() to:
                1) Throw a poisson toy from the backgroundTemplate
                2) Fit the backgroundModel to the toy
                3) Save result as a rooWrapper in the list **self._toyFitResults**
                4) Save spurious signal into **self._toySpurious**
                   Save model parameters into dictionary **self._toyParameters**

        """
        self._rootFiles = []
        self._data = {}               # Parsed data dictionary
        self._rangeNumber = 0         # Variable to create unique ranges

        ### Load input ###
        self._dataRaw = inputData     # Copy input dictionary
        self._dataRaw.update(kwargs)  # Copy kwargs, overwrite existing values
        self._checkInputs()           # Check sanity of inputs

        ### Other internal data ###
        self.w = None # Workspace
        self._initWksp()              # Make workspace
        self._setBinningToTemplate()
        self._toyIndex = 1            # Index for indentifing toys
        self._toyFitResults = []         # Save results of toys as RooWrappers
        self._toyHistResults = []         # Save results of toys as RooWrappers
        # self._toySpurious= []         # Save spurious signals calculated from toys
        self._toyParameters = {}      # dict of lists of toy parameters
        self._toyYields = []          # list of limit dictionaries (_limits) for toys
        self._toyNames = []
        self._fitResults = []

        # These are created by fits
        self._nominalFitYield = None
        self._nominalFit = None
        self._yields = None
        self._debugString=""

        ### Parse input ###
        self._parseRawData(numbersOnly=True) # Load numbers first (Due to RooFit bug)
        self._makeRanges()
        self._parseRawData(numbersOnly=False) # Load input that needs to be loaded

        ### Inject signal to form template if needed ###
        # also forms tempRange
        self._formTemplate()
        # build model (S+B, or B only depends on what's provided)
        self._formModel()

        ### to p0 interpolation if needed (only if interpLeadingCoef is in args and true)
        #turning this off for now
        self._interpolateLeadingCoefValue()


    def update(self,**kwargs):
        """ Set extrapolation range, update extrap range """
        print "MACRO: update"
        # Update variables
        for name in kwargs.keys():
            self._parseRawDatum(name,kwargs[name])
        # Update ranges
        self._makeRanges()
        # update interpolated leading coef
        self._interpolateLeadingCoefValue()


    def fitToy(self,depth=1,saveToy=True):
        # print "DEBUG: fit"
        """ Throw toy from backgroundModel, perform fit on backgroundTemplate to toy
            Requires fit to have been called first
            Results are shown in _toyFitResults, _toyHistResults
            Depth is depth of Poisson blurring
            Requires a fit to be done first
        """
        if self._nominalFitYield == None:
            raise BaseException(red("Tried to fitToy() before doing a fit. Do a fit first"))
        pdfName = "model"
        # save snapshot of variables before doing fits to toys
        # also save _yields
        modelParameters = self.get(pdfName).getParameters(RooArgSet(self.get("x")))
        self.w.saveSnapshot("preToy",modelParameters)
        saveYields = copy.deepcopy(self._yields)
        # throw toy from backgroundModel
        self._throwToy(depth,pdfName=pdfName)
        # fit toy and save result in _toyFitResults, _toyHistResults
        self._fitToyBackend(pdfName=pdfName,saveToy=saveToy)
        # calculate/save yields, toy parameters
        self._calculateYields()
        self._saveToyParams(modelParameters)
        self._yields["fitDiff"] = self._yields["nFit"] - self._nominalFitYield
        self._toyYields.append(copy.deepcopy(self._yields))
        # increment toy ID
        self._toyIndex += 1
        # restore variables to state before toys
        self.w.loadSnapshot("preToy")
        self._yields = saveYields

    def toyNReco(self):
        """ Return nReco from toys 
        """
        # trimFrac = 0.10/2
        # trimFrac=0
        fitDiffs = [y["nReco"] for y in self._toyYields]
        # trimIndex = int(len(fitDiffs)*trimFrac)
        # fitDiffs = fitDiffs[trimIndex:len(fitDiffs)-trimIndex]
        return np.array(fitDiffs)

    def toyNSig(self):
        """ Return nSig from toys 
        """
        nSigs = [y["nSig"] for y in self._toyYields]
        return np.array(nSigs)

    def toyFitDiff(self):
        """ Return fit uncertainty from toys 
            Trim of extreme trimFrac
        """
        trimFrac = 0.10/2
        # trimFrac=0
        fitDiffs = sorted([y["fitDiff"] for y in self._toyYields])
        trimIndex = int(len(fitDiffs)*trimFrac)
        fitDiffs = fitDiffs[trimIndex:len(fitDiffs)-trimIndex]
        return np.array(fitDiffs)

    def _pdfFitDiff(self):
        """ Return fit uncertainty based on getPropagatedError method
        """
        x = self.get("x")
        fit = self.get("backgroundModel")
        fitResult = self._nominalFit
        # 
        integral = fit.createIntegral(RooArgSet(x),RooFit.Range(self._rName("extrapRange")))
        fitYield = integral.getPropagatedError(fitResult,RooArgSet(x))
        fitError = integral.getVal()
        return fitYield,fitError

    def _loopParams(self,pdfName):
        """ Loop over parameters, return dict of variables & info """
        rooParams = self.get(pdfName).getParameters(RooArgSet(self.get("x")))
        iterator = rooParams.fwdIterator()
        ret = {}
        while True:
            var=iterator.next()
            if not var: break
            ret[var.GetName()] = {}
            ret[var.GetName()]["val"] = var.getVal()
            ret[var.GetName()]["min"] = var.getMin()
            ret[var.GetName()]["max"] = var.getMax()
        return ret


    def fit(self):
        """ Perform fit for extrapolation
        """
        # orignal values
        self._prefitDiagnostic()
        self.origVals = self._loopParams("model")
        self._fitBackend(templateName="template",modelName="model")
        self.postFitVals = self._loopParams("model")
        self._calculateYields()
        # save nominal fit yield for toys later
        self._nominalFitYield = self._yields["nFit"]
        self._nominalFit = self._fitResults[-1]
        # print parameter values
        self._printFitResults()

    def _printFitResults(self):
        """ Print fit results, requires fit to have been called """
        print yellow("="*20,"Fit Results","="*20)
        self.get("template").Print()
        # self.get("model").Print()
        for varName in self.origVals.keys():
            if self.origVals[varName]["val"]==self.postFitVals[varName]["val"]:
                print yellow("{0}:\t{1}->{2}".format(varName,self.origVals[varName]["val"],self.postFitVals[varName]["val"]))
            else:
                print red("{0}:\t{1}->{2}".format(varName,self.origVals[varName]["val"],self.postFitVals[varName]["val"]))
        print yellow("="*53)



    def limits(self):
        # print "DEBUG: limits"
        """ Calculate limits
            Merge into yields of current fit, return
        """
        if self._yields == None:
            raise BaseException(red("Tried to call limits() before doing a fit. Do a fit first"))
        self._poissonLimits()
        return copy.deepcopy(self._yields)

    def yields(self):
        """ Calculate yields in the singal region
            Return yields in a dictionary
        """
        self._calculateYields()
        # if self._yields == None:
            # raise BaseException(red("Tried to call yields() before doing a fit. Do a fit first"))
        return copy.deepcopy(self._yields)

    def printFitResults(self):
        for i,fr in enumerate(self._fitResults):
            print green("="*10,i,"="*10)
            fr.Print()

    def debug(self):
        # print "DEBUG: debug function"
        """ Temporary debugging function. Feel free to overwrite this """
        print "#"*40
        self.get("backgroundTemplate").sumEntries("x","fitRange")
        temp="template"
        temp="backgroundTemplate"
        data = self.get(temp)
        print "fullRange\t",   data.sumEntries("x",self._rName("fullRange"))
        print "fitRange\t",    data.sumEntries("x",self._rName("fitRange"))
        print "extrapRange\t", data.sumEntries("x",self._rName("extrapRange"))
        print "ALL\t\t",       data.sumEntries()
        print "DONE"*10


####################################################################################################
# Private member functions
####################################################################################################

####################################################################################################
# SETUP member functions
####################################################################################################
    def _checkInputs(self):
        # print "DEBUG: _checkInputs"
        """ Check that the required internal variables are set """
        # ALL of these keys must be present
        requiredKeys = ["backgroundTemplate","backgroundModel","fitMin","fitMax"]
        # Loop over keys and check that they are all there
        for key in requiredKeys:
            if key not in self._dataRaw.keys():
                raise BaseException("Missing input during checkInputs: {0}".format(key))

    def _initWksp(self):
        precision = 1e-8
        RooAbsPdf.defaultIntegratorConfig().setEpsRel(precision);
        RooAbsPdf.defaultIntegratorConfig().setEpsAbs(precision);
        # print "DEBUG: _initWksp"
        """ Initialize workspace, default variables """
        self.w = RooWorkspace("w","w")
        x = RooRealVar("x","x",2,0,10)
        # x = RooRealVar("x","x",3,1,10)
        # x = RooRealVar("x","x",0)
        self.add("x",x)

    def _checkRootObjectExists(self, obj, msg=None):
        # print "DEBUG: _checkRootObjectExists"
        """ Check if object exists """
        if not msg:
            msg="Generic check for object printed above"
        if obj: return
        raise BaseException(msg)

    def _rName(self,name):
        """ Returns the proper name of a range
            There is a bug when associating ranges with RooDataSets. This is an
            Unfortunate work around: make a new range everytime a variable is 
            changed, and give it a unique name. This is the function that handels
            what the name should be
        """
        return name
        return name+"_"+str(self._rangeNumber)

    def _toyName(self,prefix):
        """ Returns proper toy name, based on self._toyIndex
        """
        l = "abcdefghijklmnopqrstuvwxyz"
        i = self._toyIndex+1
        s = "toy"
        while i>1:
            s+=l[i%26]
            i=int(i/10)
        return prefix+s

    def _makeRange(self, name, lowName, highName):
        """ Make range name+_rangeNumber, with lowName and highName defined in data
            These ranges are defined with RooRealVar, so to change, update those
            Should only be called from self._makeRanges()
        """
        # Check that ranges exist
        if not self.exists(lowName):
            raise BaseException("Range limit {0} not defined".format(lowName))
        if not self.exists(highName):
            raise BaseException("Range limit {0} not defined".format(highName))
        # get unique range name
        name = self._rName(name)
        # Make range based on RooRealVar
        # rangeMin   = self.get(lowName)
        # rangeMax   = self.get(highName)
        rangeMin   = float(self.get(lowName).getVal())
        rangeMax   = float(self.get(highName).getVal())
        x          = self.get("x")
        print "*"*200
        # print "debug1 range", name
        # x.Print()
        # x.removeRange(name) # avoid conflict
        x.setRange(name,rangeMin,rangeMax)
        # x.setMin(name,rangeMin)
        # x.setMax(name,rangeMax)
        print "DEBUG: created range {0} from {1} ===> {2}".format(name,rangeMin,rangeMax)
        # print "DEBUG: created range {0} from {1} ===> {2}".format(name,rangeMin.getVal(),rangeMax.getVal())
        print "*"*200

    def _makeRanges(self):
        """ Make the fitRange and extrapRange, save to _data and workspace 
            fitMin, fitMax must be defined
        """
        # increment range number for unique range names
        self._rangeNumber+=1
        # Make fit range
        self._makeRange("fitRange","fitMin","fitMax")
        # If extrapMin, extrapMax are defined, make extrapRange
        if self.exists("extrapMin") and self.exists("extrapMax"):
            self._makeRange("extrapRange","extrapMin","extrapMax")

        # make fullRange: from fitMin to max(fitMax,extrapMax)
        fullRangeMin = self.get("fitMin").getVal()
        fullRangeMax = self.get("fitMax").getVal()
        if self.exists("extrapMax"):
            fullRangeMax = max(fullRangeMax,self.get("extrapMax").getVal())
        self._parseFloat(fullRangeMin,"fullRangeMin")
        self._parseFloat(fullRangeMax,"fullRangeMax")
        self._makeRange("fullRange","fullRangeMin","fullRangeMax")

    def _parseRawDatum(self,name,datum):
        """ Parse individual object 
            Skip data == None
        """
        if datum==None: return
        if isinstance(datum,float) or isinstance(datum,int):
            # Copy floats directly to _data
            self._parseFloat(datum,name)
        elif "Template" in name:
            # Parse templates as TH1 histograms
            self._parseTh1ViaString(datum,name)
        elif "Model" in name or "Norm" in name:
            # Parse models as PDF's
            if isinstance(datum,str):
                # parse string input
                self._parsePdfViaString(datum,name)
            else:
                # otherwise, add a pdf object
                self._parsePdfObject(datum,name)

    def _parseRawData(self,numbersOnly=False):
        # print "DEBUG: _parseRawData"
        """ Parse objects in _dataRaw that need to be parsed
            Otherwise, copy into _data
            numbersOnly==True: only load numbers
        """
        for name in self._dataRaw.keys():
            rawVal = self._dataRaw[name]
            # this skip is needed, because of a rooFit bug where ranges 
            # must be defined before making RooDataHist
            if numbersOnly and not (isinstance(rawVal,float) or isinstance(rawVal,int)):
                continue
            self._parseRawDatum(name,rawVal)
        # set x range min, max
        if numbersOnly:
            x = self.get("x")
            x.setMin(self.get("fitMin").getVal())
            x.setMax(self.get("fitMax").getVal())
            if self.exists("extrapMax"):
                x.setMax(self.get("extrapMax").getVal())

    def _parsePdfObject(self, pdf, name):
        """ Load pdf=RooAbsPdf, save into workspace
        """
        print "=================================================="
        print "==         Loading external PDF                 =="
        print name
        print pdf
        print "Type is:",type(pdf).__name__
        pdf.Print()
        self.add(name,pdf)
        print "========== Done loading ext PDF =================="


    def _parsePdfViaString(self, string, name):
        # print "DEBUG: _parsePdfViaString"
        """ Load PDF based on string, save into workspace with factory
            Also saved to _data
            Example: EXPR::backgroundModel('b+x',x,b[0,0,1])
        """
        print "=================================================="
        print "==== Attempting to load object to factory     ===="
        print "==== If there is a problem, check this string ===="
        print "===", string, "==="
        print "=================================================="
        print "Running factory in _parsePdfViaString"
        pdf = self.w.factory(string)
        print "Check if object exists:"
        self._checkRootObjectExists(pdf,msg="Could not make PDF with string: {0}".format(string))
        # Save result
        self.add(name,pdf)

    def _parseFloat(self, value, name):
        # print "DEBUG: _parseFloat", name, value
        """ Parse float into RooRealVar, save to _data and workspace 
            If already exist, just update value
        """
        if not self.exists(name):
            v = RooRealVar(name,name,float(value))
            # Save result
            self.add(name,v)
        else:
            self.get(name).setVal(value)

    def _parseTh1ViaString(self, string, name):
        # print "DEBUG: _parseTh1ViaString", name, string
        """ Load TH1 name from root file 
            string=path.root:name:units (Etienne's style)
            name=name for histo
            TH1 object saved to _data
            Example: path/to/file.root:nameOfTh1f
        """
        # There must be a ":" in the string to separate the path and name
        if ":" not in string:
            raise BaseException("Missing delim in TH1 input string: {0}".format(string))
        # Split string into path, name
        splitString = string.split(":")
        if len(splitString)<3:
            raise BaseException("Poorly formed TH1 input string: {0}".format(string))
        path = splitString[0]
        pname = splitString[1]
        units = splitString[2].lower()
        # loop over remaining commands, check if exist
        rebin=None
        rescale=None
        for i in range(3,len(splitString)):
            command=splitString[i]
            if "rebin" in command:
                rebin=int(command.replace("rebin",""))
            if "rescale" in command:
                rescale=float(command.replace("rescale",""))
        # Open root file, append to global root file list
        # This keeps the file open
        self._rootFiles.append(TFile(path))
        if not self._rootFiles[-1]: raise BaseException("Could not open file {0}".format(pname))
        hist = self._rootFiles[-1].Get(pname)
        if not hist: raise BaseException("Could not load histogram {0} from {1}".format(pname,path))
        hist.SetName(pname)
        # rescale if in gev to tev
        if units=="gev":
            print "DEBUG: Rescalling based on gev units"
            hist = self._rescaleHistAxis(hist,1/1000)
        if rebin!= None:
            hist = hist.Rebin(rebin)
        if rescale!=None:
            # hist.Scale(rescale*hist.Integral())
            hist.Scale(rescale)
        # Check that the histogram was loaded
        self._checkRootObjectExists(hist,msg="Could not find TH1 {0} in file {1}".format(pname,path))
        # Convert histogram to RooDataHist
        rdh = self._histToRdh(hist)
        rdh.SetName(name)
        # Save result
        self.add(name,rdh)


    #it didn't seem to work properly for me.. the binning ended up getting strange..
    def _rescaleHistAxis(self,hist, factor):
        """ Utility to rescale axis of hist by factor. Returns new hist """
        name=hist.GetName()
        title=hist.GetTitle()
        minimum = hist.GetBinLowEdge(1)*factor
        maximum = hist.GetBinLowEdge(hist.GetNbinsX()+1)*factor
        rescaled = TH1D(name,title,hist.GetNbinsX(),minimum,maximum)
        for i in range(0,hist.GetNbinsX()+1):
            c = hist.GetBinCenter(i)
            v = hist.GetBinContent(i)
            rescaled.Fill(c*factor, v)
        return rescaled


    def _rdhToHist(self,rdh):
        # print "DEBUG: _rdhToHist"
        """ Convert RooDataHist to histogram, return TH1 """
        name= rdh.GetName()
        x   = self.get("x")
        hist = rdh.createHistogram(name,x)
        return hist

    def _histToRdh(self,hist):
        print ""
        print "*"*50, 3
        # print "DEBUG: _histToRdh"
        """ Convert histogram to RooDataHist, return RDH """
        name= hist.GetName()
        x   = self.get("x")
        print "TH1:"
        # hist.Print()
        print "nbinsx", hist.GetNbinsX()
        rdh = RooDataHist(name,name,RooArgList(x),hist)
        print "RDH:"
        # rdh.Print()
        print "*"*50, 4
        return rdh

    def _getRdhRange(self, rdh):
        """ Horrible function to get RDH range. Unclear why this isn't avialable in RDH 
            Returns min, max of bin range of rdh
        """
        self._checkRootObjectExists(rdh,msg="Tried to _getRdhRange, but rdh not exist")
        hist = self._rdhToHist(rdh)
        rangeMin = hist.GetBinLowEdge(1)
        rangeMax = hist.GetBinLowEdge(hist.GetNbinsX()+1)
        return rangeMin, rangeMax

    def _sumRdh(self,a,b,scale):
        """ Sum rdh called a, b*scale """
        aHist = self._rdhToHist(a)
        bHist = self._rdhToHist(b)
        # Add
        aHist.Add(bHist,float(scale))
        # Convert back to rdh
        added = self._histToRdh(aHist)
        return added

    def _formModel(self):
        """ Form model for fit
            If signalModel exists, form a template of sig*sigScale+bkg
        """
        print yellow("Forming model")
        if not self.exists("backgroundModel"):
            raise BaseException("Tried to form model but no backgroundModel defined")
        # If signal template exists, inject it
        x = self.get("x")
        if self.exists("signalModel") and 1:
            # make extended backgroundModel

            # self.get('x').setRange("test",2,6)
            # self.get('x').setRange("fitRange",4,6)
            # self.get('x').setRange("fitRange",1.5,2)

            bkg = self.get("backgroundTemplate")
            # nbkg = obs.sumEntries("x",self._rName("fitRange"))
            # nbkg = bkg.sumEntries("x","test")
            nbkg = bkg.sumEntries("x","fitRange")
            # nbkg = 0.00001
            # print red(nbkg); quit()
            # nbkg = 6
            self._parseFloat(nbkg, "backgroundNorm")

            bkgExt = RooExtendPdf("bkgExt","bkgExt",self.get("backgroundModel"),self.get("backgroundNorm"),"fitRange")
            sigExt = RooExtendPdf("sigExt","sigExt",self.get("signalModel"),self.get("signalNorm"),"fitRange")
            self.add("signalModelExt",sigExt)
            self.add("backgroundModelExt",bkgExt)

            print yellow("Forming RooAddPdf")
            model = RooAddPdf("model","model",
                              RooArgList(self.get("backgroundModelExt"), self.get("signalModelExt")),
                              RooArgList(self.get("backgroundNorm"),  self.get("signalNorm"))
                             )
            print yellow("Done RooAddPdf")
            self.add("model",model)

            l = self.get("lambda")
            print "Fixing addcoef range"
            self.get("model").fixAddCoefRange("fitRange",True)
            print "Fixing normalization range"
            self.get("model").fixAddCoefNormalization(RooArgSet(x),True)

        # Otherwise, copy background model
        else:
            bkg = self.get("backgroundModel")
            model = bkg.Clone()
            self.add("model",model)
        # Define tempRange based on template range
        # tempRangeMin,tempRangeMax=self._getRdhRange(template)
        print yellow("Done forming model")


    def _formTemplate(self):
        """ Form template to fit to 
            If signalTemplate exists, form a template of sig*sigScale+bkg
            Also, remakes templates - it appears templates must be made after defining ranges
        """
        print yellow("Forming template")
        if not self.exists("backgroundTemplate"):
            raise BaseException("Tried to form template but no backgroundTemplate defined")
        # If signal template exists, inject it
        if self.exists("signalTemplate"):
            if not self.exists("signalInjectScale"):
                raise BaseException("Tried to form template but no signalInjectScale defined")
            scale    = self.get("signalInjectScale").getVal()
            sig      = self.get("signalTemplate")
            bkg      = self.get("backgroundTemplate")
            template = self._sumRdh(bkg,sig,scale)
            print sig
            print bkg
            print template
        # Otherwise, copy backgroundTemplate to template
        else:
            bkg = self.get("backgroundTemplate")
            template = bkg.Clone()
        # Define tempRange based on template range
        # tempRangeMin,tempRangeMax=self._getRdhRange(template)
        print yellow("Done forming template")
        self.add("template",template)

    def _setBinningToTemplate(self):
        """ Set binning of "x" to that of template """
        # get template
        histPath = self._dataRaw["backgroundTemplate"]
        # make histogram called binningHist used for binning
        self._parseTh1ViaString(histPath,"binningHist")
        binningHist = self.get("binningHist")
        # get binning
        nBins  = binningHist.numEntries()
        low    = binningHist.get(0)["x"].getVal()
        second = binningHist.get(1)["x"].getVal()
        high   = binningHist.get(nBins-1)["x"].getVal()
        width  = second - low
        # correct for bin center value
        low  -= width/2
        high += width/2
        # make binning
        binning = RooBinning(low,high,"defaultBins")
        binning.addUniform(nBins,low,high)
        self.add("binning",binning,printOn=False)
        # set variable "x" to have binning from binningHist
        self.get("x").setBinning(binning)

    def add(self,name,obj,printOn=True):
        """ Add objects to workspace, add names to _data """
        print "="*50, "add", 1
        # print "DEBUG: add", name, obj
        if printOn:
            print "Adding object of type:", type(obj).__name__
            print "Adding object of name:", name
            obj.Print()
        else:
            print "Added",name,"print disabled"
            print obj
            print type(obj).__name__
            print "Name=",name
        print "="*50, "add", 2
        sys.stdout.flush()
        if "SetName" in dir(obj): obj.SetName(name)
        if "SetTitle" in dir(obj): obj.SetTitle(name)
        print "="*50, "add", 3
        # overwrite if already exists
        # if self.w.obj(name):
            # self.w.obj(name).Delete()
        # getattr(self.w,"import")(obj)
        if type(obj).__name__ == "RooBinning":
            getattr(self.w,"import")(obj)
        else:
            getattr(self.w,"import")(obj,RecycleConflictNodes())
        print "="*50, "add", 4

    def exists(self,name):
        """ Check if object exists """
        try:
            a=self.w.obj(name).GetName()
            # a=self.w.obj(name).Print()
            return 1
        except: return 0

    def get(self,name):
        if not self.exists(name):
            raise BaseException("Tried to get {0} which doesn't exist".format(name))
        return self.w.obj(name)

    def __del__(self):
        # print "DEBUG: Destructor"
        """ Clean up root classes """

        # Root is an idiot
        # del self.w
        # self.w.Delete()

    def _interpolate(self, xVect,yVect,xVal):
        # print "DEBUG: _interpolate", xVal
        """ interpolate a value for y based on xVal between the points in the vectors
            x-vector required to be in accending ordered
            y-vector should be sorted corresponding to x-vector
            vectors must have length >=2, xVect must increase absolutely
            returns linear interpolation between two nearest points to xVal
        """
        print "MACRO::interpolate called for xVal=",xVal
        # Find lower, upper x-point
        lowerI =-1
        upperI=-1
        # If value is too low, extend line (edge case)
        if (xVal<=xVect[0]):
            lowerI=0
            upperI=1
        # If value is too high, extend line (edge case)
        elif (xVal>=xVect[-1]):
            lowerI=len(xVect)-2
            upperI=len(xVect)-1
        # Otherwise, find upper and lower bounds inside
        else:
            for i in range(1,len(xVect)):
                if (xVect[i]>xVal):
                    lowerI=i-1
                    upperI=i
                    break
        # Perform interpolation
        # Get fraction that xVal is between upper and lower (can be negative)
        upperX = xVect[upperI]
        lowerX = xVect[lowerI]
        upperY = yVect[upperI]
        lowerY = yVect[lowerI]
        frac = (xVal-lowerX)/(upperX-lowerX)
        # Project onto y vector
        slope = (upperY-lowerY)/(upperX-lowerX)
        yVal = lowerY + slope*(xVal-lowerX)
        print "########################################", upperX, lowerX, upperY, lowerY, frac, slope
        return yVal


    def _interpolateLeadingCoefValue(self):
        # print "DEBUG: conditional _interpolateLeadingCoefValue"
        """ Interpolate leadingCoef initial values based on fitMax and vectors found by Deshan
            If "interpLeadingCoef" does not exist: this function does nothing
            Otherwise: set diphotonLeadingCoef in workspace based on fitMax (both must exist)
        """
        # do nothing if interpLeadingCoef not set to true
        if not self.exists("interpLeadingCoef"): return
        if not self.get("interpLeadingCoef").getVal(): return
        # get inputs for interpolation
        fitMax                  = self.get("fitMax").getVal()
        fitMaxVect              = [0.4, 0.5, 1.0, 1.4, 2.0, 2.4, 3.0]
        leadingCoefValueVect    = [5.0, 5.0, 4.0, 4.0, 3.0, 3.0, 3.0]
        leadingCoefValueVectMin = [5.0, 5.0, 3.0, 3.0, 2.0, 2.0, 2.0]
        leadingCoefValueVectMax = [6.0, 6.0, 5.0, 5.0, 4.0, 4.0, 4.0]
        # Do interpolations
        leadingCoefVal = self._interpolate(fitMaxVect, leadingCoefValueVect,    fitMax)
        leadingCoefMin = self._interpolate(fitMaxVect, leadingCoefValueVectMin, fitMax)
        leadingCoefMax = self._interpolate(fitMaxVect, leadingCoefValueVectMax, fitMax)
        # Set values in workspace
        if not self.exists("diphotonLeadingCoef"):
            raise BaseException("Tried interpolate leadingCoefvalue, but diphotonLeadingCoef not exist\n"+\
                                "check that the background function has diphotonLeadingCoef!")
        self.get("diphotonLeadingCoef").setVal(leadingCoefVal)
        self.get("diphotonLeadingCoef").setMin(leadingCoefMin)
        self.get("diphotonLeadingCoef").setMax(leadingCoefMax)


####################################################################################################
# FIT member functions
####################################################################################################

    def _fitToyBackend(self,pdfName="model",saveToy=True):
        """ Fit to toy (ID is in _toyIndex) 
            pdfName is name of PDF to fit to
            Results:
                # Calculate spurious signal of toy, adds to self._toySpurious
                Save fit RW into self._toyFitResults
        """
        toyName = self._toyName("backgroundModelToy")
        print "Fitting toy dataset: {0}".format(toyName),self._rName("fitRange")
        # grab ranges
        fitMin    = self.get("fitMin").getVal()
        fitMax    = self.get("fitMax").getVal()
        extrapMin = self.get("extrapMin").getVal()
        extrapMax = self.get("extrapMax").getVal()
        # make fit to toy
        self._fitBackend(templateName=toyName,modelName="model")
        # self._fitBackend(templateName="template",modelName="model")
        if saveToy:
            # get dataset of points for plotting later
            toyObj = self.get(toyName)
            fitObj = self.get(pdfName)
            toy    = rw.rooWrap(toyObj,self.get("x"))
            fit    = rw.rooWrap(fitObj,self.get("x"))
            fit.normalizeTo(toy,minRange=fitMin,maxRange=fitMax)
            self._toyFitResults.append(fit)
            self._toyHistResults.append(toy)
            # record toy spurious signal
            # fitSum = fit.sum(minRange=extrapMin,maxRange=extrapMax)
            # toySum = toy.sum(minRange=extrapMin,maxRange=extrapMax)

    def _throwToy(self,depth,templateName="template",pdfName=""):
        """ "Throw" a toy with from template
            Depth is number of Poisson steps to take
        """
        # print "DEBUG: _throwToy"
        toyName = self._toyName("backgroundModelToy")
        # copy template as roodatahist
        template = self.get(templateName)
        # make new toy if needed
        if not self.exists(toyName):
            toy  = RooDataHist(template,toyName)
            self.add(toyName,toy)
        toy  = self.get(toyName)
        # throw toy via generate
        print "Throwing toy as {0}, in range {1}".format(toyName,self._rName("fullRange"))
        for i in range(template.numEntries()):
            template.get(i)
            toy.get(i)
            # read values from template
            val = template.weight()
            mod = val
            for i in range(depth):
                try: 
                    mod = np.random.poisson(val,1)[0]
                except:
                    raise BaseException(red("Tried to generate toy perm for value {0}".format(val)))
            # modify toy
            toy.set(mod)
        self._toyNames.append(toyName)
        self._toyNames = list(set(self._toyNames))

    def _prefitDiagnostic(self):
        # print "DEBUG: _prefitDiagnostic"
        """ Print out the objects to be used for the fit """
        print "*"*50, 2
        print "The fit should use the following objects:"
        template = self.get("template")
        bkgModel = self.get("model")
        print template
        print bkgModel
        print "*"*50, 1

    def _fitBackend(self,templateName,modelName):
        """ Perform fit of modelName to templateName
            * templateName="template" for normal fit to data
            * templateName="backgroundModelToy8" for fit to toy 8
            * modelName="model" for fit to background
        """
        # print green("Fitting {0}".format(templateName))
        #adding this hear for now. But will clean up later.. 
        RooAbsPdf.defaultIntegratorConfig().setEpsRel(10e-8);
        RooAbsPdf.defaultIntegratorConfig().setEpsAbs(10e-8);
        template = self.get(templateName)
        bkgModel = self.get(modelName)
        # bkgModel.setNormRange(self._rName("fitRange")) # uncomment for take longer slow
        print template
        print red("Fitting to range",self._rName("fitRange"))
        print red(self._debugString)

        print RooFit.Save()
        print RooFit.Extended(True)
        print RooFit.SumW2Error(kFALSE),RooFit.Verbose(False)
        print RooFit.Range(self._rName("fitRange"))

        if 1:
            fitResult = bkgModel.fitTo( template,
                                        RooFit.Save(),
                                        # RooFit.Extended(self.get(modelName).canBeExtended()),
                                        RooFit.Extended(True),
                                        RooFit.SumW2Error(kFALSE),RooFit.Verbose(False),
                                        # RooFit.PrintLevel(-1),
                                        RooFit.Range(self._rName("fitRange"))
                                      )
        else:
            fitResult = RooFitResult("fr","fr")

        self._fitResults.append(fitResult)
        print red(self._debugString)
        print red("Done fitting to range",self._rName("fitRange"))


####################################################################################################
# Single Bin member functions
####################################################################################################

    def _saveToyParams(self,modelParameters):
        """ Save values of modelParameters into self._toyParameters 
        """
        # This messy loop is needed because of improper container impl.
        iterator = modelParameters.fwdIterator()
        while True:
            param=iterator.next()
            if not param: break
            name = param.GetName()
            val  = param.getVal()
            # save to dictionary
            if name not in self._toyParameters.keys():
                self._toyParameters[name]=[]
            self._toyParameters[name].append(val)

    def _calculateYields(self):
        """ Integrate inputs in ranges to get yields for single bin experiment 
            Also calculate "spurious signal"
            Results are placed into self._yields
        """
        print "MACRO: calculateYields"
        self._yields = {}
        hasSig = self.exists("signalTemplate")
        x   = self.get("x")
        bkgFit = self.get("backgroundModel")
        bkg = self.get("backgroundTemplate")
        obs = self.get("template")
        if hasSig: sig = self.get("signalTemplate")

        ##################################################
        # Yield calculations
        ##################################################
        # signal yields
        if hasSig:
            scale = self.get("signalInjectScale").getVal()
            sigInFullRange   = scale*sig.sumEntries("x",self._rName("fullRange"))
            sigInFitRange    = scale*sig.sumEntries("x",self._rName("fitRange"))
            sigInExtrapRange = scale*sig.sumEntries("x",self._rName("extrapRange"))

        # background yields
        obsInFullRange   = obs.sumEntries("x",self._rName("fullRange"))
        obsInFitRange    = obs.sumEntries("x",self._rName("fitRange"))
        obsInExtrapRange = obs.sumEntries("x",self._rName("extrapRange"))

        # background yields
        bkgInFullRange   = bkg.sumEntries("x",self._rName("fullRange"))
        bkgInFitRange    = bkg.sumEntries("x",self._rName("fitRange"))
        bkgInExtrapRange = bkg.sumEntries("x",self._rName("extrapRange"))

        # fitIntegral is UN-NORMED integral expected from fit
        bkgFitIntegralInExtrapRange = bkgFit.createIntegral(RooArgSet(x),RooFit.Range(self._rName("extrapRange"))).getVal() 
        bkgFitIntegralInFitRange    = bkgFit.createIntegral(RooArgSet(x),RooFit.Range(self._rName("fitRange"))).getVal() 
        bkgFitIntegralInFullRange   = bkgFit.createIntegral(RooArgSet(x),RooFit.Range(self._rName("fullRange"))).getVal()

        # set parameters to be used as yields
        nBkg = bkgInExtrapRange
        nObs = obsInExtrapRange
        if hasSig: nSig = sigInExtrapRange
        else: nSig=0
        # nfit is normalized based on the fit range
        if bkgFitIntegralInFitRange>0:
            nbkgFit = (obsInFitRange/bkgFitIntegralInFitRange) * bkgFitIntegralInExtrapRange 
        else:
            # if RooFit is saying fitIntegralInFitRange ==0, then set nFit=0
            nbkgFit=0
        self._yields["nBkg"] = nBkg
        self._yields["nSig"] = nSig
        self._yields["nFit"] = nbkgFit
        self._yields["nObs"] = nObs

        ##################################################
        # Spurious signal calculations
        ##################################################
        # Spurious signal calculated from bkg template
        nSpur  = nbkgFit - nBkg
        nReco  = nObs - nbkgFit
        self._yields["nSpur"] = nSpur
        self._yields["nReco"] = nReco
        # self._parseFloat(nSpur,"nSpur")





    def _poissonLimits(self):
        """ Function to calculate poisson limits
            This function is based on the tutorial here:
            https://twiki.cern.ch/twiki/bin/view/RooStats/RooStatsExercisesMarch2015   
            In addition to setting the various limit variables listed below, also
            Sets and returns self.limitResult, which stores these for easy plotting
        """
        print "MACRO: poissonLimits"

        # WRAPPER FOR POISSONLIMITFUNCTION WHICH HAS BEEN TESTED SEPERATELY
        # FUNCTION:
        #   poissonLimitFunction.poissonLimit(nExp, nSig, nObs, nSpur=1)
        # RETURNS:
        #   ret["upperLimit"] = htir.UpperLimit();
        #   ret["lowerLimit"] = htir.LowerLimit();
        #   ret["expLimit"]   = htir.GetExpectedUpperLimit(0);
        #   ret["uOneSig"]    = htir.GetExpectedUpperLimit(1);
        #   ret["uTwoSig"]    = htir.GetExpectedUpperLimit(2);
        #   ret["lOneSig"]    = htir.GetExpectedUpperLimit(-1);
        #   ret["lTwoSig"]    = htir.GetExpectedUpperLimit(-2);

        nBkg = self._yields["nBkg"]
        nFit = self._yields["nFit"]
        nSig = self._yields["nSig"]
        nObs = self._yields["nObs"]
        nSpur  = self._yields["nSpur"]
        limitResult=poissonLimitFunction.poissonLimit(nFit, nSig, nObs, abs(nSpur),abs(nSpur))
        # merge limit result into "_yields"
        self._yields.update(limitResult)

        return limitResult



