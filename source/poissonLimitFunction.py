from __future__ import division
import sys
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
sys.argv.append("-b")
from ROOT import TH1F, RooWorkspace, RooDataHist, RooArgList, TCanvas, RooFit, TText, TPad
from ROOT import TLine, gPad, RooArgSet, RooAddPdf,RooCategory,RooDataSet,RooCmdArg,RooSimultaneous
from ROOT import RooLinkedList, RooAbsReal, TFile
from ROOT.RooFit import *
from ROOT import kGreen, kRed, kFALSE, kBlue
from ROOT.RooStats import ModelConfig, AsymptoticCalculator,HypoTestInverter, HypoTestCalculatorGeneric


# poisson limit unit test for RooStats
# following https://twiki.cern.ch/twiki/bin/view/RooStats/RooStatsExercisesMarch2015

def poissonLimit(nExp, nSig, nObs, nSpur=1):
    # calculate poisson limit

    expectedBkg = nExp
    expectedSig = nSig
    measured    = nObs
    ss = nSpur

    # make workspace
    w = RooWorkspace("w","w")
    w.factory("x[0]")

    # make poisson model
    w.factory("sum:nExp(nSig[0],nBkg[0])")
    w.var("nBkg").setVal(expectedBkg)
    w.var("nSig").setVal(expectedSig)
    w.factory("Poisson:pdf(nObs[0],nExp)");
    w.factory("Gaussian:constraint(nBkgExpect[0],nBkg,ss[1])");
    w.factory("PROD:model(pdf,constraint)");
    w.var("ss").setVal(ss);

    ###
    w.var("nBkgExpect").setVal(expectedBkg);
    w.var("nBkgExpect").setConstant(True); # needed for being treated as global observables
    # w.var("nBkg").setConstant(True); # changed by aaron
    mc = ModelConfig("sbModel",w);
    mc.SetPdf(w.pdf("model"));
    mc.SetParametersOfInterest(RooArgSet(w.var("nSig")));
    mc.SetObservables(RooArgSet(w.var("nObs")));
    mc.SetNuisanceParameters(RooArgSet(w.var("nBkg")));
    # these are needed for the hypothesis tests
    mc.SetSnapshot(RooArgSet(w.var("nSig")));
    # mc.SetGlobalObservables(w.var("nBkgExpect")); # removed etienne
    mc.Print();
    # import model in the workspace
    getattr(w,"import")(mc);
    # make data set with the namber of observed events
    data = RooDataSet("data","", RooArgSet(w.var("nObs")));
    w.var("nObs").setVal(measured);
    data.add(RooArgSet(w.var("nObs")));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);
    # ##################################################
    sbModel = mc;

    # create null hypothesis H0 (based on H)
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    bModel.SetSnapshot((RooArgSet(w.var("nSig"))));
    # fix nSig value to 0
    nSigNoSig = bModel.GetParametersOfInterest().first();
    nSigNoSig.setVal(0);
    bModel.SetSnapshot(RooArgSet(nSigNoSig));

    # do aysmtotic test
    ac = AsymptoticCalculator(data, bModel, sbModel);
    ac.SetOneSided(True);
    # AsymptoticCalculator::SetPrintLevel(-1);

    # create hypotest inverter
    hypoCalc = HypoTestInverter(ac);
    hypoCalc.SetFixedScan(1000,0.01,100, True);#added by etienne 2
    hypoCalc.SetConfidenceLevel(0.95);
    hypoCalc.UseCLs(True);
    hypoCalc.SetVerbose(True);
    # hypoCalc.SetOneSidedDiscovery(True);

    # htr = HypoTestCalculatorGeneric(hypoCalc).GetHypoTest();
    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest();
    htir = hypoCalc.GetInterval();

    # get resutls
    ret = {}
    ret["upperLimit"] = htir.UpperLimit();
    ret["lowerLimit"] = htir.LowerLimit();
    ret["expLimit"]   = htir.GetExpectedUpperLimit(0);
    ret["uOneSig"]    = htir.GetExpectedUpperLimit(1);
    ret["uTwoSig"]    = htir.GetExpectedUpperLimit(2);
    ret["lOneSig"]    = htir.GetExpectedUpperLimit(-1);
    ret["lTwoSig"]    = htir.GetExpectedUpperLimit(-2);
    ret["nExp"]       = nExp
    ret["nSig"]       = nSig
    ret["nObs"]       = nObs
    ret["nSpur"]      = nSpur

    return ret


if __name__=="__main__":
    ### test
    nBkg=1001
    nSig=10
    nObs=1000
    nSpur=1
    limit=poissonLimit(nBkg, nSig, nObs, nSpur)
    print "="*50
    # print limit
    print "="*50

