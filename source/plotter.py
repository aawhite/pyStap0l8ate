from __future__ import division
import sys
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
from ROOT import *
from ROOT.RooStats import *
import numpy as np
import pystrap as ps
import roowrap as rw
import numpy.ma as ma
from extra import *

try:
    from matplotlib.ticker import ScalarFormatter
    import matplotlib.ticker as ticker
except: pass

import matplotlib.pyplot as plt
import matplotlib as mpl

colors = {"sig":"#c92702",
          "bkg":"#0232cf",
          "mod":"#cc8502",
          "dat":"#222222",
         }

####################################################################################################
# This holds the plotter "rooPlotter" class for making matplotlib plots with pystrap objects
# it is designed to be used with rooWrap, a wrapper for root objects
# Contents:
#  * Style functions
#  * Plotting functions
####################################################################################################

####################################################################################################
# Style functions
####################################################################################################

# mpl.use("pgf")
if mpl.__version__.split(".")[0]!="0":
    pgf_with_rc_fonts = {
        "font.family": "sans-serif",
        "font.size" : "15",
        "font.sans-serif": ["Helvetica"],
        "pgf.texsystem":"pdflatex",
        "pgf.preamble":[
                        r"\usepackage{amsmath}",\
                        r"\usepackage[english]{babel}",\
                        r"\usepackage{arev}",\
                       ]
    }
    mpl.rcParams.update(pgf_with_rc_fonts)
    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']
    # plt.rcParams["patch.force_edgecolor"] = True
    plt.clf(); plt.cla()
    fig = plt.figure(1)

def ticksInside(removeXLabel=False):
    """ Make atlas style ticks """
    ax=plt.gca()
    ax.tick_params(labeltop=False, labelright=False)
    plt.xlabel(ax.get_xlabel(), horizontalalignment='right', x=1.0)
    plt.ylabel(ax.get_ylabel(), horizontalalignment='right', y=1.0)
    ax.tick_params(axis='y',direction="in",left=1,right=1,which='both')
    ax.tick_params(axis='x',direction="in",labelbottom=not removeXLabel,bottom=1, top=1,which='both')

def atlasInternal(position="nw",status="Internal",size=12,lumi=36.1,subnote=""):
    ax=plt.gca()
    # decide the positioning
    if position=="se":
        textx=0.95; texty=0.05; verticalalignment="bottom"; horizontalalignment="right"
    if position=="nw":
        textx=0.05; texty=0.95; verticalalignment="top"; horizontalalignment="left"
    if position=="ne":
        textx=0.95; texty=0.95; verticalalignment="top"; horizontalalignment="right"
    if position=="n":
        textx=0.5; texty=0.95; verticalalignment="top"; horizontalalignment="center"
    # add label to plot
    if lumi==None:
        lines = [r"\noindent \textbf{{\emph{{ATLAS}}}} {0}".format(status),
                 r"$\textstyle\sqrt{s}=13 \text{ TeV }$",
                ]
    else:
        lines = [r"\noindent \textbf{{\emph{{ATLAS}}}} {0}".format(status),
                 r"$\textstyle\sqrt{s}=13 \text{ TeV } \int{\text{Ldt}}="+str(lumi)+r"\text{ fb}^{\text{-1}}$",
                ]
    if subnote: lines.append(subnote)
    labelString = "\n".join(lines)
    # labelString="l=36.1 fb-1"
    plt.text(textx,texty, labelString,transform=ax.transAxes,va=verticalalignment,ha=horizontalalignment, family="sans-serif",size=size)
    # # plt.tight_layout(.5)
    # # set axis labels
    ticksInside()



def chi2(a,b,onlyIf=lambda x: True):
    """ return chi2(a,b) of two numpy arrays
        use onlyIf function to calc in a range
    """
    if len(a)!=len(b):
        raise BaseException("Tried calc chi2 with two different sized arrays")
    ret=0
    for i in range(len(a)):
        if b[i]==0: continue
        if not onlyIf(i): continue
        ret+=(a[i]-b[i])**2 / b[i]
    return ret/len(a)


####################################################################################################
# Plotting functions
####################################################################################################

def plotOnlyToys(extrap, path=None, **kwargs):
    """ Make matplotlib style plot using the rooWrap class 
        Also adds toyFits
    """
    # get optional inputs
    xAxisName=False
    if "xAxisName" in kwargs.keys(): xAxisName=kwargs["xAxisName"]
    labels=False
    if "labels" in kwargs.keys(): labels=kwargs["labels"]
    logy=True
    if "logy" in kwargs.keys(): logy=kwargs["logy"]
    logx=False
    if "logx" in kwargs.keys(): logx=kwargs["logx"]
    ratioMax=None
    if "ratioMax" in kwargs.keys(): ratioMax=kwargs["ratioMax"]
    ratioMin=None
    if "ratioMin" in kwargs.keys(): ratioMin=kwargs["ratioMin"]
    xMin=None
    if "xMin" in kwargs.keys(): xMin=kwargs["xMin"]
    #
    print "*"*20,"plotting","*"*20
    # get things to plot from workspace
    rdhObj = extrap.get("template")
    fitObj = extrap.get("backgroundModel")
    x      = extrap.get("x")
    fitMax = extrap.get("fitMax").getVal()
    fitMin = extrap.get("fitMin").getVal()
    tempMin, tempMax = extrap._getRdhRange(extrap.get("template"))
    # get bounds for the plot
    if xMin!=None: plotMinX=xMin
    else: plotMinX = extrap.get("fitMin").getVal()
    try: plotMaxX = extrap.get("extrapMax").getVal()
    except: plotMaxX = extrap.get("fitMax").getVal()
    plotMinX = 0.15
    fitMaxX = extrap.get("fitMax").getVal()
    extrapMin = extrap.get("extrapMin").getVal()
    plotMaxX = 6
    # plotMaxX = extrap.get("fitMax").getVal()
    # make wrappers for objects
    rdh = rw.rooWrap(rdhObj,x)
    fit = rw.rooWrap(fitObj,x)
    rdh.rebin(100); rdh.scale(1/100)
    fit.rebin(100); fit.scale(1/100)
    # trim arrays to the bounds of the plot
    fit.cut(minRange=plotMinX,maxRange=plotMaxX)
    rdh.cut(minRange=plotMinX,maxRange=plotMaxX)
    # normalize
    fit.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
    # deal with toys
    toyRdhs = []
    for toyName in extrap._toyNames:
        obj = extrap.get(toyName)
        toy = rw.rooWrap(obj,x)
        toy.cut(minRange=fitMin,maxRange=fitMaxX)
        toyRdhs.append(toy)
        break
    for toy in extrap._toyFitResults:
        toy.cut(minRange=plotMinX,maxRange=plotMaxX)
        # toy.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
        # toy.rebin(100); toy.scale(1/100)
        # break
    ######################
    # make matplotlib plot
    ######################
    # make spacing tight
    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(8,6))

    ######################
    # top plot
    ######################
    ax = plt.gca()
    print "rdh.x",rdh.x
    print "fit.x",fit.x
    # add to plot
    objs = []
    # fit region
    plt.axvspan(fitMin,fitMaxX,alpha=0.2,linewidth=0,color='blue',label="Fit Region")
    plt.axvspan(extrapMin,plotMaxX,alpha=0.2,linewidth=0,color='red',label="Signal Region")
    objs.append(plt.plot([],[],linewidth=17,alpha=0.5,color='blue',label="Fit Region"))
    objs.append(plt.plot([],[],linewidth=17,alpha=0.5,color='red',label="Signal Region"))
    ############################################
    # plot all toys on plot
    for toy in extrap._toyFitResults:
        objs.append(ax.plot(toy.x, toy.y,"r-",linewidth=2,label="Toy Fit")) 
        break
    ############################################
    for toyRdh in toyRdhs:
        ax.plot(toyRdh.x, toyRdh.y,"r.",alpha=0.25)
    objs.append(ax.plot([], [],"r.",label="Toy"))
    objs.append(ax.plot(rdh.x, rdh.y,"k-",label="Nominal Template"))
    objs.append(ax.plot(fit.x, fit.y,"C0-",linewidth=4,label="Nominal Fit"))
    if logy: plt.yscale('log')
    if logx: plt.xscale('log')
    # plt.legend(fontsize=12,loc=3, frameon=False)
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc=3, frameon=False)
    plt.ylabel("Events / (0.001)",fontsize=12)
    plt.xlim((plotMinX,plotMaxX))
    plt.ylim((min(rdh.y)*0.8,max(rdh.y)*100))
    #correct the ticks

    plt.xlabel(r"$m_{ll}$ TeV",fontsize=12)
    atlasInternal(position="ne")
    ticksInside(removeXLabel=False)
    import matplotlib.ticker
    ax.yaxis.set_label_coords(-0.1, 1)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    locmaj = matplotlib.ticker.LogLocator(base=10,numticks=12)
    ax.yaxis.set_major_locator(locmaj)
    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9),numticks=12)
    ax.yaxis.set_minor_locator(locmin)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
    # save plot
    # fig.set_size_inches(w=6,h=6)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")
    plt.close()
    plt.clf()


def plotToys(extrap, path=None, **kwargs):
    """ Make matplotlib style plot using the rooWrap class 
        Also adds toyFits
    """
    # get optional inputs
    xAxisName=False
    if "xAxisName" in kwargs.keys(): xAxisName=kwargs["xAxisName"]
    labels=False
    if "labels" in kwargs.keys(): labels=kwargs["labels"]
    logy=True
    if "logy" in kwargs.keys(): logy=kwargs["logy"]
    logx=False
    if "logx" in kwargs.keys(): logx=kwargs["logx"]
    ratioMax=None
    if "ratioMax" in kwargs.keys(): ratioMax=kwargs["ratioMax"]
    ratioMin=None
    if "ratioMin" in kwargs.keys(): ratioMin=kwargs["ratioMin"]
    xMin=None
    if "xMin" in kwargs.keys(): xMin=kwargs["xMin"]
    #
    print "*"*20,"plotting","*"*20
    # get things to plot from workspace
    rdhObj = extrap.get("template")
    fitObj = extrap.get("backgroundModel")
    x      = extrap.get("x")
    fitMax = extrap.get("fitMax").getVal()
    fitMin = extrap.get("fitMin").getVal()
    tempMin, tempMax = extrap._getRdhRange(extrap.get("template"))
    # get bounds for the plot
    if xMin!=None: plotMinX=xMin
    else: plotMinX = extrap.get("fitMin").getVal()
    try: plotMaxX = extrap.get("extrapMax").getVal()
    except: plotMaxX = extrap.get("fitMax").getVal()
    plotMinX = 0.15
    # plotMaxX = extrap.get("fitMax").getVal()
    # make wrappers for objects
    rdh = rw.rooWrap(rdhObj,x)
    fit = rw.rooWrap(fitObj,x)
    # trim arrays to the bounds of the plot
    fit.cut(minRange=plotMinX,maxRange=plotMaxX)
    rdh.cut(minRange=plotMinX,maxRange=plotMaxX)
    # normalize
    fit.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
    # deal with toys
    toyRdhs = []
    for toyName in extrap._toyNames:
        obj = extrap.get(toyName)
        toy = rw.rooWrap(obj,x)
        toy.cut(minRange=plotMinX,maxRange=plotMaxX)
        toyRdhs.append(toy)
    for toy in extrap._toyFitResults:
        toy.cut(minRange=plotMinX,maxRange=plotMaxX)
        # toy.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
    ######################
    # make matplotlib plot
    ######################
    # make spacing tight
    import matplotlib.gridspec as gridspec
    grid = gridspec.GridSpec(3, 1, height_ratios=[4, 1,1])
    grid.update(wspace=0.025, hspace=0.05)
    plt.figure(figsize=(6,6))

    ######################
    # top plot
    ######################
    ax = plt.subplot(grid[0])
    # print "rdh.x",rdh.x
    # print "fit.x",fit.x
    # add to plot
    objs = []
    ############################################
    # plot +- 1 sigma band, with the +/- evaluated for above/below fit.y
    if len(extrap._toyFitResults):
        toys = []
        for toy in extrap._toyFitResults:
            toys.append(toy.y)
        toys = np.array(toys)
        upper=[]; lower=[]
        upperEnvelope=[]; lowerEnvelope=[]
        for i in range(toys.shape[1]):
            upperVals = ma.masked_where(toys[:,i]<fit.y[i],toys[:,i])
            lowerVals = ma.masked_where(toys[:,i]>fit.y[i],toys[:,i])
            upperEnvelope.append(toys[:,i].max())
            lowerEnvelope.append(toys[:,i].min())
            upper.append(np.sqrt(np.mean(np.abs(upperVals-fit.y[i])**2)))
            lower.append(np.sqrt(np.mean(np.abs(lowerVals-fit.y[i])**2)))
        upperEnvelope=np.array(upperEnvelope)
        lowerEnvelope=np.array(lowerEnvelope)
        upper=fit.y+np.array(upper)
        lower=fit.y-np.array(lower)
        # +/- sigma
        ax.fill_between(fit.x, lower,upper,linewidth=0,facecolor="g",label=r"$1\sigma_{toys}$",alpha=0.5)
        objs.append(ax.plot([], [],"g",linewidth=15,alpha=0.5,label=r"Toy $1\sigma_{stats}$"))
        # envelope
        ax.plot(fit.x, upperEnvelope,"g")
        ax.plot(fit.x, lowerEnvelope,"g")
        objs.append(ax.plot([], [],"g",label="Toy Envelope"))
    ############################################
    ############################################
    # plot all toys on plot
    # for i,toy in enumerate(extrap._toyFitResults):
        # objs.append(ax.plot(toy.x, toy.y,"r,",alpha=0.5,label=str(i))) 
    ############################################
    # for toyRdh in toyRdhs:
        # ax.plot(toyRdh.x, toyRdh.y,"r,",alpha=0.25)
    objs.append(ax.plot(rdh.x, rdh.y,"k.",label="Template"))
    objs.append(ax.plot(fit.x, fit.y,label="Fit"))
    objs.append(ax.plot([fitMax]*2,[min(rdh.y),max(rdh.y)],"r",alpha=0.25,label="Fit Max:{0:.2f} TeV".format(fitMax)))
    objs.append(ax.plot([fitMin]*2,[min(rdh.y),max(rdh.y)],"g",alpha=0.25,label="Fit Min:{0:.2f} TeV".format(fitMin)))
    if labels:
        for l in labels:
            objs.append(ax.plot([],[]," ",label=l))
    objs.append(ax.plot([],[]," ",label=r"$\chi^2={0:.4f}$".format(chi2(rdh.y,fit.y,onlyIf=lambda i: fit.x[i]>fitMax))))
    atlasInternal(position="ne",status="Internal")
    ticksInside(removeXLabel=True)
    if logy:
        plt.yscale('log')
    if logx: 
        plt.xscale('log')
    # plt.legend(fontsize=12,loc=3, frameon=False)
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc=3, frameon=False)
    plt.ylabel("Events / (0.001)",fontsize=12)
    plt.xlim((plotMinX,plotMaxX))
    plt.ylim((min(rdh.y)*0.8,max(rdh.y)*1.2))
    #correct the ticks
    import matplotlib.ticker
    ax.yaxis.set_label_coords(-0.1, 1)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    locmaj = matplotlib.ticker.LogLocator(base=10,numticks=12)
    ax.yaxis.set_major_locator(locmaj)
    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9),numticks=12)
    ax.yaxis.set_minor_locator(locmin)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())

    ######################
    # bottom plot
    ######################
    ax = plt.subplot(grid[1])
    # add to plot
    ratio=fit.y/rdh.y
    upperRatio = upper/rdh.y
    lowerRatio = lower/rdh.y
    ax.fill_between(fit.x, lowerRatio,upperRatio,linewidth=0,facecolor="g",alpha=0.5)
    ax.plot([min(fit.x),max(fit.x)], [1,1],"k",alpha=0.25)
    ax.plot(fit.x, ratio,label="Ratio")
    ticksInside()
    plt.ylabel("Fit/Data",fontsize=12)
    if xAxisName:
        plt.xlabel(xAxisName,fontsize=12)
    else:
        plt.xlabel(r"$m_{ll}$ TeV",fontsize=12)
    if logx: 
        plt.xscale('log')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.1f"))
        # ax.xaxis.set_minor_formatter(ticker.ScalarFormatter())
        # ax.xaxis.set_minor_formatter(ticker.FormatStrFormatter("%.8f"))
        ax.set_xticks([plotMinX,1,plotMaxX])
    plt.xlim((plotMinX,plotMaxX))
    #Get correct ticks
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    # optional, fix scale of ratio plot
    if ratioMin!=None and ratioMax!=None:
        plt.ylim((ratioMin,ratioMax))
        # make marks for above, below
        above = ma.masked_where(ratio<ratioMax, fit.x)
        below = ma.masked_where(ratio>ratioMin,  fit.x)
        shift = (ratioMax-ratioMin)*0.1
        ax.plot(above, [ratioMax-shift]*len(above),"r^")
        ax.plot(below, [ratioMin+shift]*len(below),"bv")
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.2))
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.05), minor = True)
    yrange = plt.gca().get_ylim()
    ax.plot([fitMax]*2,[yrange[0],yrange[1]],"r",alpha=0.25)
    ax.plot([fitMin]*2,[yrange[0],yrange[1]],"g",alpha=0.25)

    ######################
    # bottom plot
    ######################
    ax = plt.subplot(grid[2])
    # add to plot
    ratio=(fit.y-rdh.y)/fit.y
    upperRatio = (upper-rdh.y)/fit.y
    lowerRatio = (lower-rdh.y)/fit.y
    ax.fill_between(fit.x, lowerRatio,upperRatio,linewidth=0,facecolor="g",alpha=0.5)
    ax.plot([min(fit.x),max(fit.x)], [0,0],"k",alpha=0.25)
    ax.plot(fit.x, ratio,label="Pull")
    ticksInside()
    plt.ylabel("(Fit-Data)/Fit",fontsize=12)
    if xAxisName:
        plt.xlabel(xAxisName,fontsize=12)
    else:
        plt.xlabel(r"$m_{ll}$ TeV",fontsize=12)
    if logx: 
        plt.xscale('log')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.1f"))
        ax.set_xticks([plotMinX,1,plotMaxX])
    plt.xlim((plotMinX,plotMaxX))
    #Get correct ticks
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    scale = 3
    ratioLow = ratio.mean()-scale*ratio.std()
    ratioHigh = ratio.mean()+scale*ratio.std()
    plt.ylim((ratioLow,ratioHigh))
    above = ma.masked_where(ratio<ratioHigh, fit.x)
    below = ma.masked_where(ratio>ratioLow,  fit.x)
    shift = (ratioHigh-ratioLow)*0.1
    ax.plot(above, [ratioHigh-shift]*len(above),"r^")
    ax.plot(below, [ratioLow+shift]*len(below),"bv")
    yrange = plt.gca().get_ylim()
    ax.plot([fitMax]*2,[yrange[0],yrange[1]],"r",alpha=0.25)
    ax.plot([fitMin]*2,[yrange[0],yrange[1]],"g",alpha=0.25)

    # save plot
    # fig.set_size_inches(w=6,h=6)
    plt.savefig(path,bbox_inches="tight")
    plt.close()
    plt.clf()

    # save yield table
    yieldFile = open(path.replace(".png",".csv"),"w")
    # def sum(self,minRange=None,maxRange=None,invert=False):
    fitSum = fit.sum(minRange=fitMax,maxRange=plotMaxX)
    rdhSum = rdh.sum(minRange=fitMax,maxRange=plotMaxX)
    yieldFile.write("name,yield\n".format(fitSum))
    yieldFile.write("fit,{0}\n".format(fitSum))
    yieldFile.write("rhd,{0}\n".format(rdhSum))
    if len(extrap._toyFitResults): # if there are toy envelopes
        upperSum = rw.rooWrap(upper,fit).sum(minRange=fitMax,maxRange=plotMaxX)
        lowerSum = rw.rooWrap(lower,fit).sum(minRange=fitMax,maxRange=plotMaxX)
        upperEnvelopeSum = rw.rooWrap(upperEnvelope,fit).sum(minRange=fitMax,maxRange=plotMaxX)
        lowerEnvelopeSum = rw.rooWrap(lowerEnvelope,fit).sum(minRange=fitMax,maxRange=plotMaxX)
        yieldFile.write("upperSigmaSum,{0}\n".format(upperSum))
        yieldFile.write("lowerSigmaSum,{0}\n".format(lowerSum))
        yieldFile.write("upperEnvelopeSum,{0}\n".format(upperEnvelopeSum))
        yieldFile.write("lowerEnvelopeSum,{0}\n".format(lowerEnvelopeSum))


def plotPdfs(extrap, pdfExtraps, path=None, **kwargs):
    """ Plot with PDF's also drawn """
    # get optional inputs
    xAxisName=False
    if "xAxisName" in kwargs.keys(): xAxisName=kwargs["xAxisName"]
    labels=False
    if "labels" in kwargs.keys(): labels=kwargs["labels"]
    logy=True
    if "logy" in kwargs.keys(): logy=kwargs["logy"]
    logx=False
    if "logx" in kwargs.keys(): logx=kwargs["logx"]
    ratioMax=None
    if "ratioMax" in kwargs.keys(): ratioMax=kwargs["ratioMax"]
    ratioMin=None
    if "ratioMin" in kwargs.keys(): ratioMin=kwargs["ratioMin"]
    xMin=None
    if "xMin" in kwargs.keys(): xMin=kwargs["xMin"]
    #
    print "*"*20,"plotting","*"*20
    # get things to plot from workspace
    rdhObj = extrap.get("template")
    fitObj = extrap.get("backgroundModel")
    x      = extrap.get("x")
    fitMax = extrap.get("fitMax").getVal()
    fitMin = extrap.get("fitMin").getVal()
    tempMin, tempMax = extrap._getRdhRange(extrap.get("template"))
    # get bounds for the plot
    if xMin!=None: plotMinX=xMin
    else: plotMinX = extrap.get("fitMin").getVal()
    try: plotMaxX = extrap.get("extrapMax").getVal()
    except: plotMaxX = extrap.get("fitMax").getVal()
    plotMinX = 0.15
    # plotMaxX = extrap.get("fitMax").getVal()
    # make wrappers for objects
    rdh = rw.rooWrap(rdhObj,x)
    fit = rw.rooWrap(fitObj,x)
    # trim arrays to the bounds of the plot
    fit.cut(minRange=plotMinX,maxRange=plotMaxX)
    rdh.cut(minRange=plotMinX,maxRange=plotMaxX)
    # normalize
    fit.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
    # handel pdfs
    pdfs = []
    dats = []
    indexMaxSpur = 0
    indexMinSpur = 0
    maxSpur = pdfExtraps[0].yields()["nSpur"]
    minSpur = pdfExtraps[0].yields()["nSpur"]
    for i,pdfExtrap in enumerate(pdfExtraps):
        if pdfExtrap.yields()["nSpur"] > maxSpur:
            maxSpur = pdfExtrap.yields()["nSpur"]
            indexMaxSpur = i
        if pdfExtrap.yields()["nSpur"] < minSpur:
            minSpur = pdfExtrap.yields()["nSpur"]
            indexMinSpur = i
        dat  = pdfExtrap.get("template")
        pdf  = pdfExtrap.get("backgroundModel")
        x    = pdfExtrap.get("x")
        pdf  = rw.rooWrap(pdf,x)
        dat  = rw.rooWrap(dat,x)
        pdf.cut(minRange=plotMinX,maxRange=plotMaxX)
        dat.cut(minRange=plotMinX,maxRange=plotMaxX)
        pdf.normalizeTo(dat,minRange=fitMin,maxRange=fitMax)
        pdfs.append(pdf)
        dats.append(dat)
    # get envelope of pdfs 
    envelopeMax = np.array([pdf.y for pdf in pdfs]).max(axis=0)
    envelopeMin = np.array([pdf.y for pdf in pdfs]).min(axis=0)
    allPdfsMax = fit.y + (pdfs[indexMaxSpur].y-dats[indexMaxSpur].y)
    allPdfsMin = fit.y + (pdfs[indexMinSpur].y-dats[indexMinSpur].y)
    ######################
    # make matplotlib plot
    ######################
    # make spacing tight
    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(6,6))
    grid = gridspec.GridSpec(3, 1, height_ratios=[4, 1,1])
    grid.update(wspace=0.025, hspace=0.05)

    ######################
    # top plot
    ######################
    ax = plt.subplot(grid[0])
    print "rdh.x",rdh.x
    print "fit.x",fit.x
    # add to plot
    objs = []
    ############################################
    # plot +- 1 sigma band, with the +/- evaluated for above/below fit.y
    objs.append(ax.plot(rdh.x, rdh.y,"k.",label="Template"))
    objs.append(ax.plot(fit.x, fit.y,label="Fit"))
    for i, pdf in enumerate(pdfs):
        ax.plot(pdf.x, pdf.y,plotColorsDotted[i%len(plotColorsDotted)],alpha=0.5)
    objs.append(ax.plot(fit.x, envelopeMax,"g",label="PDF Envelope"))
    ax.plot(fit.x, envelopeMin,"g",label="PDF Envelope")
    objs.append(ax.plot([fitMax]*2,[min(rdh.y),max(rdh.y)],"r",alpha=0.25,label="Fit Max:{0:.2f} TeV".format(fitMax)))
    objs.append(ax.plot([fitMin]*2,[min(rdh.y),max(rdh.y)],"g",alpha=0.25,label="Fit Min:{0:.2f} TeV".format(fitMin)))
    if labels:
        for l in labels:
            objs.append(ax.plot([],[]," ",label=l))
    objs.append(ax.plot([],[]," ",label=r"$\chi^2={0:.4f}$".format(chi2(rdh.y,fit.y,onlyIf=lambda i: fit.x[i]>fitMax))))
    atlasInternal(position="ne",status="Internal")
    ticksInside(removeXLabel=True)
    if logy:
        plt.yscale('log')
    if logx: 
        plt.xscale('log')
    # plt.legend(fontsize=12,loc=3, frameon=False)
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc=3, frameon=False)
    plt.ylabel("Events / (0.001)",fontsize=12)
    plt.xlim((plotMinX,plotMaxX))
    plt.ylim((min(rdh.y)*0.8,max(rdh.y)*1.2))
    #correct the ticks
    import matplotlib.ticker
    ax.yaxis.set_label_coords(-0.1, 1)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    locmaj = matplotlib.ticker.LogLocator(base=10,numticks=12)
    ax.yaxis.set_major_locator(locmaj)
    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9),numticks=12)
    ax.yaxis.set_minor_locator(locmin)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())

    ######################
    # bottom plot
    ######################
    ax = plt.subplot(grid[1])
    # add to plot
    ratio=fit.y/rdh.y
    upperRatio = allPdfsMax/rdh.y
    lowerRatio = 1/upperRatio
    # lowerRatio = allPdfsMin/rdh.y
    ax.fill_between(fit.x, lowerRatio,upperRatio,linewidth=0,facecolor="g",alpha=0.5)
    ax.plot([min(fit.x),max(fit.x)], [1,1],"k",alpha=0.25)
    ax.plot(fit.x, ratio,label="Ratio")
    ticksInside()
    plt.ylabel("Fit/Data",fontsize=12)
    if xAxisName:
        plt.xlabel(xAxisName,fontsize=12)
    else:
        plt.xlabel(r"$m_{ll}$ TeV",fontsize=12)
    if logx: 
        plt.xscale('log')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.1f"))
        # ax.xaxis.set_minor_formatter(ticker.ScalarFormatter())
        # ax.xaxis.set_minor_formatter(ticker.FormatStrFormatter("%.8f"))
        ax.set_xticks([plotMinX,1,plotMaxX])
    plt.xlim((plotMinX,plotMaxX))
    #Get correct ticks
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    # optional, fix scale of ratio plot
    if ratioMin!=None and ratioMax!=None:
        plt.ylim((ratioMin,ratioMax))
        # make marks for above, below
        above = ma.masked_where(ratio<ratioMax, fit.x)
        below = ma.masked_where(ratio>ratioMin,  fit.x)
        shift = (ratioMax-ratioMin)*0.1
        ax.plot(above, [ratioMax-shift]*len(above),"r^")
        ax.plot(below, [ratioMin+shift]*len(below),"bv")
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.2))
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.05), minor = True)
    yrange = plt.gca().get_ylim()
    ax.plot([fitMax]*2,[yrange[0],yrange[1]],"r",alpha=0.25)
    ax.plot([fitMin]*2,[yrange[0],yrange[1]],"g",alpha=0.25)

    ######################
    # bottom plot
    ######################
    ax = plt.subplot(grid[2])
    # add to plot
    ratio=(fit.y-rdh.y)/fit.y
    upperRatio = (allPdfsMax-rdh.y)/rdh.y
    lowerRatio = -upperRatio
    # lowerRatio = (allPdfsMin-rdh.y)/rdh.y
    ax.fill_between(fit.x, lowerRatio,upperRatio,linewidth=0,facecolor="g",alpha=0.5)
    ax.plot([min(fit.x),max(fit.x)], [0,0],"k",alpha=0.25)
    ax.plot(fit.x, ratio,label="Pull")
    ticksInside()
    plt.ylabel("(Fit-Data)/Fit",fontsize=12)
    if xAxisName:
        plt.xlabel(xAxisName,fontsize=12)
    else:
        plt.xlabel(r"$m_{ll}$ TeV",fontsize=12)
    if logx: 
        plt.xscale('log')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.1f"))
        ax.set_xticks([plotMinX,1,plotMaxX])
    plt.xlim((plotMinX,plotMaxX))
    #Get correct ticks
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    scale = 3
    ratioLow = ratio.mean()-scale*ratio.std()
    ratioHigh = ratio.mean()+scale*ratio.std()
    plt.ylim((ratioLow,ratioHigh))
    above = ma.masked_where(ratio<ratioHigh, fit.x)
    below = ma.masked_where(ratio>ratioLow,  fit.x)
    shift = (ratioHigh-ratioLow)*0.1
    ax.plot(above, [ratioHigh-shift]*len(above),"r^")
    ax.plot(below, [ratioLow+shift]*len(below),"bv")
    yrange = plt.gca().get_ylim()
    ax.plot([fitMax]*2,[yrange[0],yrange[1]],"r",alpha=0.25)
    ax.plot([fitMin]*2,[yrange[0],yrange[1]],"g",alpha=0.25)

    # save plot
    # fig.set_size_inches(w=6,h=6)
    plt.savefig(path,bbox_inches="tight")
    plt.close()
    plt.clf()

    # save yield table
    yieldFile = open(path.replace(".png",".csv"),"w")
    # def sum(self,minRange=None,maxRange=None,invert=False):
    fitSum = fit.sum(minRange=fitMax,maxRange=plotMaxX)
    rdhSum = rdh.sum(minRange=fitMax,maxRange=plotMaxX)
    yieldFile.write("name,yield\n".format(fitSum))
    yieldFile.write("fit,{0}\n".format(fitSum))
    yieldFile.write("rhd,{0}\n".format(rdhSum))

def plotMplRatioNoSig(extrap, path=None, **kwargs):
    """ Make matplotlib style plot using the rooWrap class """
    xAxisName=False
    if "xAxisName" in kwargs.keys(): xAxisName=kwargs["xAxisName"]
    labels=False
    if "labels" in kwargs.keys(): labels=kwargs["labels"]
    logy=True
    if "logy" in kwargs.keys(): logy=kwargs["logy"]
    logx=False
    if "logx" in kwargs.keys(): logx=kwargs["logx"]
    ratioMax=None
    if "ratioMax" in kwargs.keys(): ratioMax=kwargs["ratioMax"]
    ratioMin=None
    if "ratioMin" in kwargs.keys(): ratioMin=kwargs["ratioMin"]
    xMin=None
    if "xMin" in kwargs.keys(): xMin=kwargs["xMin"]
    plt.clf(); plt.cla()
    print "*"*20,"plotting","*"*20

    ########################################################
    # Retrieve objects from workspace
    ########################################################
    # templates
    bkgTempObj    = extrap.get("backgroundTemplate")
    datTempObj    = extrap.get("template")
    # fits
    modFitObj     = extrap.get("model")
    bkgFitObj     = extrap.get("backgroundModel")
    # other
    x             = extrap.get("x")
    fitMax        = extrap.get("fitMax").getVal()
    fitMin        = extrap.get("fitMin").getVal()
    plotMinX      = extrap.get("fitMin").getVal()
    plotMaxX      = extrap.get("extrapMax").getVal()
    ########################################################
    # Process as rooWraps
    ########################################################
    # templates
    bkgTempRw     = rw.rooWrap(bkgTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    datTempRw     = rw.rooWrap(datTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    # fits
    modFitRw      = rw.rooWrap(modFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    bkgFitRw      = rw.rooWrap(bkgFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    # normalizations, scale singal
    print red(datTempRw.y)
    print red(modFitRw.y)
    modFitRw.normalizeTo(datTempRw,minRange=fitMin,maxRange=fitMax)
    bkgFitRw.normalizeTo(modFitRw,minRange=fitMin,maxRange=fitMax)

    datRatio=bkgFitRw.y/datTempRw.y
    bkgRatio=bkgFitRw.y/bkgTempRw.y

    # return

    ######################
    # make matplotlib plot
    ######################
    # make spacing tight
    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(6,6))
    # plt.figure(figsize=(6,10))
    # grid = gridspec.GridSpec(2, 1, height_ratios=[1, 1])
    grid = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    grid.update(wspace=0.025, hspace=0.05)

    ######################
    # top plot
    ######################
    ax = plt.subplot(grid[0])
    # add to plot
    objs = []
    objs.append(ax.plot(datTempRw.x, datTempRw.y,"k.",label="Template"))
    objs.append(ax.plot(modFitRw.x, modFitRw.y,"C9",label="Fit"))
    objs.append(ax.plot([fitMax]*2,[min(datTempRw.y),max(datTempRw.y)],"r",alpha=0.25,label="Fit Max:{0:.2f} TeV".format(fitMax)))
    objs.append(ax.plot([fitMin]*2,[min(datTempRw.y),max(datTempRw.y)],"g",alpha=0.25,label="Fit Min:{0:.2f} TeV".format(fitMin)))
    rebinFactor=100
    bkgFitRw.rebin(rebinFactor)
    width = [bkgFitRw.x[i+1]-bkgFitRw.x[i] for i in range(len(bkgFitRw.x)-1)]
    width.append(width[-1])
    objs.append(ax.bar(bkgFitRw.x,bkgFitRw.y/rebinFactor,width=width,bottom=None,color="b",linewidth=0,alpha=0.25,label="Background"))
    # if labels:
    #     for l in labels:
    #         objs.append(ax.plot([],[]," ",label=l))
    yields = extrap.yields()
    nFit = yields["nFit"]
    nObs = yields["nObs"]
    # plot setup
    atlasInternal(position="ne")
    ticksInside(removeXLabel=True)
    plt.yscale('log')
    plt.xscale('log')
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc=3, frameon=False)
    plt.ylabel("Events / (0.001)",fontsize=12)
    plt.xlim((plotMinX,plotMaxX))
    plt.ylim((min(datTempRw.y)*0.8,max(datTempRw.y)*10))
    #correct the ticks
    import matplotlib.ticker
    ax.yaxis.set_label_coords(-0.1, 1)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    locmaj = matplotlib.ticker.LogLocator(base=10,numticks=12)
    ax.yaxis.set_major_locator(locmaj)
    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9),numticks=12)
    ax.yaxis.set_minor_locator(locmin)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())

    ######################
    # bottom plot
    ######################
    ax = plt.subplot(grid[1])
    # add to plot
    ax.plot([min(modFitRw.x),max(modFitRw.x)], [1,1],"k",alpha=0.25)
    ax.plot(modFitRw.x, datRatio,label="Template Ratio")
    ax.plot(modFitRw.x, bkgRatio,label="Background Ratio")
    ticksInside()
    plt.ylabel("Fit/Template",fontsize=12)
    if xAxisName:
        plt.xlabel(xAxisName,fontsize=12)
    else:
        plt.xlabel(r"$m_{ll}$ TeV",fontsize=12)
    if logx: 
        plt.xscale('log')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.1f"))
        # ax.xaxis.set_minor_formatter(ticker.ScalarFormatter())
        # ax.xaxis.set_minor_formatter(ticker.FormatStrFormatter("%.8f"))
        ax.set_xticks([plotMinX,1,plotMaxX])
    plt.xlim((plotMinX,plotMaxX))
    #Get correct ticks
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    # optional, fix scale of ratio plot
    if ratioMin!=None and ratioMax!=None:
        plt.ylim((ratioMin,ratioMax))
        # make marks for above, below
        above = ma.masked_where(np.logical_or(bkgRatio<ratioMax,datRatio<ratioMax), modFitRw.x)
        below = ma.masked_where(np.logical_or(bkgRatio>ratioMin,datRatio>ratioMin),  modFitRw.x)
        shift = (ratioMax-ratioMin)*0.1
        ax.plot(above, [ratioMax-shift]*len(above),"r^")
        ax.plot(below, [ratioMin+shift]*len(below),"bv")
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.2))
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.05), minor = True)
    yrange = plt.gca().get_ylim()
    ax.plot([fitMax]*2,[yrange[0],yrange[1]],"r",alpha=0.25)
    ax.plot([fitMin]*2,[yrange[0],yrange[1]],"g",alpha=0.25)

    ax.set_yticks(np.arange(0.6,1.5,0.2))
    ax.set_yticks(np.arange(0.6,1.5,0.05), minor = True)
    plt.legend(fontsize=8)
    # save plot
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")
    plt.close()
    plt.clf()

    # some fit diagnostics
    postFitVals = extrap._loopParams("model")
    print postFitVals
    print yellow("="*20,"Fit Results","="*20)
    for varName in postFitVals.keys():
        print red("{0}:\t{1}<{2:.4f}<{3}".format(varName,postFitVals[varName]["min"],
                                                     postFitVals[varName]["val"],
                                                     postFitVals[varName]["max"],
                 ))
    print yellow("="*53)


def mplTestSigComp(extrap, path=None, **kwargs):
    """ Make matplotlib style plot using the rooWrap class """
    plt.clf(); plt.cla()
    print "*"*20,"plotting","*"*20
    ########################################################
    # Retrieve objects from workspace
    ########################################################
    # extrap.get("sigFrac").setVal(0.1)
    # templates
    sigTempObj    = extrap.get("signalTemplate")
    bkgTempObj    = extrap.get("backgroundTemplate")
    datTempObj    = extrap.get("template")
    # fits
    modFitObj     = extrap.get("model")
    bkgFitObj     = extrap.get("backgroundModel")
    sigFitObj     = extrap.get("signalModel")
    # other
    injectScale   = extrap.get("signalInjectScale").getVal()
    sigFrac       = extrap.get("sigFrac").getVal()
    x             = extrap.get("x")
    fitMax        = extrap.get("fitMax").getVal()
    fitMin        = extrap.get("fitMin").getVal()
    plotMinX      = extrap.get("fitMin").getVal()
    plotMaxX      = extrap.get("extrapMax").getVal()
    ########################################################
    # Process as rooWraps
    ########################################################
    # templates
    sigTempRw     = rw.rooWrap(sigTempObj,x)
    bkgTempRw     = rw.rooWrap(bkgTempObj,x)
    datTempRw     = rw.rooWrap(datTempObj,x)
    sigTempRw.scale(injectScale)
    # fits
    modFitRw      = rw.rooWrap(modFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    bkgFitRw      = rw.rooWrap(bkgFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    sigFitRw      = rw.rooWrap(sigFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)

    modFitObj.normalizeTo(datTempRw,minRange=fitMin,maxRange=fitMax)
    bkgFitRw.normalizeTo(modFitRw,minRange=fitMin,maxRange=fitMax)
    sigFitRw.normalizeTo(modFitRw,minRange=fitMin,maxRange=fitMax)
    sigFitRw.scale(sigFrac)
    bkgFitRw.scale(1-sigFrac)

    # sigFitRw.scale(0.6)
    # bkgFitRw.scale(0.4)
    # bkgFitRw.scale(scale)
    ######################
    # make matplotlib plot
    ######################
    # make spacing tight
    plt.figure(figsize=(8,9))
    ax = plt.gca()
    # add to plot
    objs = []
    objs.append(ax.plot(datTempRw.x, datTempRw.y,"k.",label="Template"))
    objs.append(ax.plot(modFitRw.x, modFitRw.y,label="Fit"))
    objs.append(ax.plot(bkgFitRw.x, bkgFitRw.y,"y--",label="Bkg Fit"))
    objs.append(ax.plot(sigFitRw.x, sigFitRw.y,"c:",label="Signal Fit"))
    objs.append(ax.plot(sigFitRw.x, sigFitRw.y+bkgFitRw.y,"r:",label="S+B"))
    # objs.append(ax.plot(bkgFit.x, sigFit.y+bkgFit.y,"r:",label="TEST S+B component"))
    objs.append(ax.plot([fitMax]*2,ax.get_ylim(),"r",alpha=0.25,label="Fit Max:{0:.2f} TeV".format(fitMax)))
    objs.append(ax.plot([fitMin]*2,ax.get_ylim(),"g",alpha=0.25,label="Fit Min:{0:.2f} TeV".format(fitMin)))
    rebinFactor=100
    bkgTempRw.rebin(rebinFactor)
    sigTempRw.rebin(rebinFactor)
    sigWidth = [bkgTempRw.x[i+1]-bkgTempRw.x[i] for i in range(len(bkgTempRw.x)-1)]
    sigWidth.append(sigWidth[-1])
    # objs.append(ax.bar(bkgTempRw.x,bkgTempRw.y/rebinFactor,width=sigWidth,bottom=None,color="b",linewidth=0,alpha=0.25,label="Background"))
    # objs.append(ax.bar(sigTempRw.x,sigTempRw.y/rebinFactor,bottom=bkgTempRw.y/rebinFactor,width=sigWidth,color="r",linewidth=0,alpha=0.25,label="Signal"))
    # objs.append(ax.bar(sigTempRw.x,sigTempRw.y/rebinFactor,bottom=None,width=sigWidth,color="r",linewidth=0,alpha=0.25,label="Signal"))
    # objs.append(ax.plot([],[]," ",label=r"$\chi^2={0:.4f}$".format(chi2(datTempRw.y,fit.y,onlyIf=lambda i: fit.x[i]>fitMax))))
    # objs.append(ax.plot([],[]," ",label="reco/inj={0:.2f}".format((nObs-nFit)/nSig)))
    atlasInternal(position="ne")
    ticksInside(removeXLabel=False)
    plt.yscale('log')
    plt.xscale('log')
    # plt.legend(fontsize=12,loc=3, frameon=False)
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc=3, frameon=False)
    plt.xlim((plotMinX,plotMaxX))
    # plt.ylim((min(datTempRw.y)*0.8,max(datTempRw.y)*1.2))
    plt.ylim(bottom=0)

    plt.legend(fontsize=8)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")
    plt.close()
    plt.clf()

def plotMplRatioNoSig2(extrap, path=None, **kwargs):
    """ Make matplotlib style plot using the rooWrap class """

    print green("Plotting",path)



    lumi=False
    if "lumi" in kwargs.keys(): lumi=kwargs["lumi"]
    xAxisName=False
    if "xAxisName" in kwargs.keys(): xAxisName=kwargs["xAxisName"]
    labels=False
    if "labels" in kwargs.keys(): labels=kwargs["labels"]
    logy=True
    if "logy" in kwargs.keys(): logy=kwargs["logy"]
    logx=False
    if "logx" in kwargs.keys(): logx=kwargs["logx"]
    ratioMax=None
    if "ratioMax" in kwargs.keys(): ratioMax=kwargs["ratioMax"]
    ratioMin=None
    if "ratioMin" in kwargs.keys(): ratioMin=kwargs["ratioMin"]
    xMin=None
    if "xMin" in kwargs.keys(): xMin=kwargs["xMin"]
    plt.clf(); plt.cla()
    print "*"*20,"plotting","*"*20
    ########################################################
    # Retrieve objects from workspace
    ########################################################
    # templates
    x             = extrap.get("x")

    bkgTempObj    = extrap.get("backgroundTemplate")
    datTempObj    = extrap.get("template")
    # fits
    modFitObj     = extrap.get("model")
    bkgFitObj     = extrap.get("backgroundModel")
    # other
    fitMax        = extrap.get("fitMax").getVal()
    fitMin        = extrap.get("fitMin").getVal()
    plotMinX      = extrap.get("fitMin").getVal()
    plotMaxX      = extrap.get("extrapMax").getVal()
    bkgNorm       = extrap.get("backgroundNorm").getVal()
    sigNorm       = extrap.get("signalNorm").getVal()
    lambdaVal     = extrap.get("lambda").getVal()
    ########################################################
    # Process as rooWraps
    ########################################################
    # templates
    bkgTempRw     = rw.rooWrap(bkgTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    datTempRw     = rw.rooWrap(datTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    # fits
    modFitRw      = rw.rooWrap(modFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    bkgFitRw      = rw.rooWrap(bkgFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    # normalizations, scale singal

    bkgFitRw.normalize(bkgNorm,minRange=fitMin,maxRange=fitMax)
    modFitRw.normalize(sigNorm+bkgNorm,minRange=fitMin,maxRange=fitMax)

    modRatio=modFitRw.y/datTempRw.y
    datRatio=bkgFitRw.y/datTempRw.y
    bkgRatio=bkgFitRw.y/bkgTempRw.y

    ######################
    # make matplotlib plot
    ######################
    # make spacing tight
    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(6,6))
    # plt.figure(figsize=(6,10))
    # grid = gridspec.GridSpec(2, 1, height_ratios=[1, 1])
    grid = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    grid.update(wspace=0.025, hspace=0.05)

    ######################
    # top plot
    ######################
    ax = plt.subplot(grid[0])
    # add to plot
    objs = []
    objs.append(ax.plot(datTempRw.x,datTempRw.y,color=colors["dat"],label="Template"))
    objs.append(ax.plot(bkgFitRw.x, bkgFitRw.y,color=colors["bkg"],label="Background Component"))
    objs.append(ax.plot(modFitRw.x, modFitRw.y,color=colors["mod"],label="Fit"))

    objs.append(ax.plot([fitMax]*2,[min(datTempRw.y),max(datTempRw.y)],"r",alpha=0.25,label="Fit Max:{0:.2f} TeV".format(fitMax)))
    objs.append(ax.plot([fitMin]*2,[min(datTempRw.y),max(datTempRw.y)],"g",alpha=0.25,label="Fit Min:{0:.2f} TeV".format(fitMin)))
    objs.append(ax.plot([],[]," ",label=r"$\Lambda=${0:.3g} TeV".format(lambdaVal)))
    rebinFactor=100
    bkgTempRw.rebin(rebinFactor)
    width = [bkgTempRw.x[i+1]-bkgTempRw.x[i] for i in range(len(bkgTempRw.x)-1)]
    width.append(width[-1])
    objs.append(ax.bar(bkgTempRw.x,bkgTempRw.y/rebinFactor,width=width,bottom=None,color="b",linewidth=0,alpha=0.25,label="Background"))
    if labels:
        for l in labels:
            objs.append(ax.plot([],[]," ",label=l))
    yields = extrap.yields()
    nFit = yields["nFit"]
    nObs = yields["nObs"]
    # plot setup
    atlasInternal(position="ne",lumi=lumi)
    ticksInside(removeXLabel=True)
    plt.yscale('log')
    plt.xscale('log')
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc=3, frameon=False)
    plt.ylabel("Events / (0.001)",fontsize=12)
    plt.xlim((plotMinX,plotMaxX))
    plt.ylim((min(datTempRw.y)*0.8,max(datTempRw.y)*10))
    #correct the ticks
    import matplotlib.ticker
    ax.yaxis.set_label_coords(-0.1, 1)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    locmaj = matplotlib.ticker.LogLocator(base=10,numticks=12)
    ax.yaxis.set_major_locator(locmaj)
    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9),numticks=12)
    ax.yaxis.set_minor_locator(locmin)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())

    ######################
    # bottom plot
    ######################
    ax = plt.subplot(grid[1])
    # add to plot
    ax.plot([min(modFitRw.x),max(modFitRw.x)], [1,1],"k",alpha=0.25)
    ax.plot(modFitRw.x,datRatio,color=colors["bkg"],label="Bkg/Data")
    ax.plot(modFitRw.x,modRatio,color=colors["mod"],label="Fit/Data")
    # ax.plot(modFitRw.x, bkgRatio,label="Bkg/Bkg Component")
    ticksInside()
    plt.ylabel("BkgFit/X",fontsize=12)
    if xAxisName:
        plt.xlabel(xAxisName,fontsize=12)
    else:
        plt.xlabel(r"$m_{ll}$ TeV",fontsize=12)
    if logx: 
        plt.xscale('log')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.1f"))
        # ax.xaxis.set_minor_formatter(ticker.ScalarFormatter())
        # ax.xaxis.set_minor_formatter(ticker.FormatStrFormatter("%.8f"))
        ax.set_xticks([plotMinX,1,plotMaxX])
    plt.xlim((plotMinX,plotMaxX))
    #Get correct ticks
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    # optional, fix scale of ratio plot
    if ratioMin!=None and ratioMax!=None:
        plt.ylim((ratioMin,ratioMax))
        # make marks for above, below
        above = ma.masked_where(np.logical_or(bkgRatio<ratioMax,datRatio<ratioMax), modFitRw.x)
        below = ma.masked_where(np.logical_or(bkgRatio>ratioMin,datRatio>ratioMin),  modFitRw.x)
        shift = (ratioMax-ratioMin)*0.1
        ax.plot(above, [ratioMax-shift]*len(above),"r^")
        ax.plot(below, [ratioMin+shift]*len(below),"bv")
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.2))
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.05), minor = True)
    yrange = plt.gca().get_ylim()
    ax.plot([fitMax]*2,[yrange[0],yrange[1]],"r",alpha=0.25)
    ax.plot([fitMin]*2,[yrange[0],yrange[1]],"g",alpha=0.25)

    ax.set_yticks(np.arange(0.6,1.5,0.2))
    ax.set_yticks(np.arange(0.6,1.5,0.05), minor = True)
    # save plot
    plt.legend(fontsize=8)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")
    plt.close()
    plt.clf()

    print "\n==== Objects"
    bkgFitObj.Print()
    modFitObj.Print()
    extrap.get("lambda").Print()



def plotMplRatio(extrap, path=None, **kwargs):
    """ Make matplotlib style plot using the rooWrap class """

    # try: 
    #     print "*"*50
    #     print red(extrap.get("signalModel"))
    #     print red(extrap.get("signalTemplate"))
    #     print "*"*50
    # except:
    #     plotMplRatioNoSig(extrap,path=path,**kwargs)
    #     return

    # if not extrap.exists("signalModel") or not extrap.exists("signalTemplate"):
    #     plotMplRatioNoSig(extrap,path=path,**kwargs)
    print green("Plotting",path)

    signalTemplate = extrap.exists("signalTemplate")
    signalModel    = extrap.exists("signalModel")


    lumi=False
    if "lumi" in kwargs.keys(): lumi=kwargs["lumi"]
    xAxisName=False
    if "xAxisName" in kwargs.keys(): xAxisName=kwargs["xAxisName"]
    labels=False
    if "labels" in kwargs.keys(): labels=kwargs["labels"]
    logy=True
    if "logy" in kwargs.keys(): logy=kwargs["logy"]
    logx=False
    if "logx" in kwargs.keys(): logx=kwargs["logx"]
    ratioMax=None
    if "ratioMax" in kwargs.keys(): ratioMax=kwargs["ratioMax"]
    ratioMin=None
    if "ratioMin" in kwargs.keys(): ratioMin=kwargs["ratioMin"]
    xMin=None
    if "xMin" in kwargs.keys(): xMin=kwargs["xMin"]
    plt.clf(); plt.cla()
    print "*"*20,"plotting","*"*20
    ########################################################
    # Retrieve objects from workspace
    ########################################################
    # templates
    x             = extrap.get("x")

    bkgTempObj    = extrap.get("backgroundTemplate")
    datTempObj    = extrap.get("template")
    sigTempObj    = extrap.get("signalTemplate")
    # fits
    modFitObj     = extrap.get("model")
    bkgFitObj     = extrap.get("backgroundModel")
    sigFitObj     = extrap.get("signalModel")
    # other
    fitMax        = extrap.get("fitMax").getVal()
    fitMin        = extrap.get("fitMin").getVal()
    plotMinX      = extrap.get("fitMin").getVal()
    plotMaxX      = extrap.get("extrapMax").getVal()
    bkgNorm       = extrap.get("backgroundNorm").getVal()
    sigNorm       = extrap.get("signalNorm").getVal()
    lambdaVal     = extrap.get("lambda").getVal()
    ########################################################
    # Process as rooWraps
    ########################################################
    # templates
    bkgTempRw     = rw.rooWrap(bkgTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    datTempRw     = rw.rooWrap(datTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    sigTempRw     = rw.rooWrap(sigTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    # fits
    modFitRw      = rw.rooWrap(modFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    bkgFitRw      = rw.rooWrap(bkgFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    sigFitRw      = rw.rooWrap(sigFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    # normalizations, scale singal

    sigFitRw.normalize(sigNorm,minRange=fitMin,maxRange=fitMax)
    bkgFitRw.normalize(bkgNorm,minRange=fitMin,maxRange=fitMax)
    modFitRw.normalize(sigNorm+bkgNorm,minRange=fitMin,maxRange=fitMax)

    modRatio=modFitRw.y/datTempRw.y
    datRatio=bkgFitRw.y/datTempRw.y
    bkgRatio=bkgFitRw.y/bkgTempRw.y

    ######################
    # make matplotlib plot
    ######################
    # make spacing tight
    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(6,6))
    # plt.figure(figsize=(6,10))
    # grid = gridspec.GridSpec(2, 1, height_ratios=[1, 1])
    grid = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    grid.update(wspace=0.025, hspace=0.05)

    ######################
    # top plot
    ######################
    ax = plt.subplot(grid[0])
    # add to plot
    objs = []
    objs.append(ax.plot(datTempRw.x,datTempRw.y,color=colors["dat"],label="Template"))
    objs.append(ax.plot(bkgFitRw.x, bkgFitRw.y,color=colors["bkg"],label="Background Component"))
    objs.append(ax.plot(sigFitRw.x, sigFitRw.y,color=colors["sig"],label="Siganl Component"))
    objs.append(ax.plot(modFitRw.x, modFitRw.y,color=colors["mod"],label="Fit"))

    objs.append(ax.plot([fitMax]*2,[min(datTempRw.y),max(datTempRw.y)],"r",alpha=0.25,label="Fit Max:{0:.2f} TeV".format(fitMax)))
    objs.append(ax.plot([fitMin]*2,[min(datTempRw.y),max(datTempRw.y)],"g",alpha=0.25,label="Fit Min:{0:.2f} TeV".format(fitMin)))
    objs.append(ax.plot([],[]," ",label=r"$\Lambda=${0:.3g} TeV".format(lambdaVal)))
    rebinFactor=100
    bkgTempRw.rebin(rebinFactor)
    sigWidth = [bkgTempRw.x[i+1]-bkgTempRw.x[i] for i in range(len(bkgTempRw.x)-1)]
    sigWidth.append(sigWidth[-1])
    objs.append(ax.bar(bkgTempRw.x,bkgTempRw.y/rebinFactor,width=sigWidth,bottom=None,color="b",linewidth=0,alpha=0.25,label="Background"))
    if signalTemplate:
        sigTempRw.rebin(rebinFactor)
        # objs.append(ax.bar(sigTempRw.x,sigTempRw.y/rebinFactor,bottom=bkgFitRw.y/rebinFactor,width=sigWidth,color="r",linewidth=0,alpha=0.25,label="Signal"))
        objs.append(ax.bar(sigTempRw.x,sigTempRw.y/rebinFactor,bottom=None,width=sigWidth,color="r",linewidth=0,alpha=0.25,label="Signal"))
    if labels:
        for l in labels:
            objs.append(ax.plot([],[]," ",label=l))
    yields = extrap.yields()
    nFit = yields["nFit"]
    nObs = yields["nObs"]
    nSig = yields["nSig"]
    if nSig!=0:objs.append(ax.plot([],[]," ",label="reco/inj={0:.2f}".format((nObs-nFit)/nSig)))
    # plot setup
    atlasInternal(position="ne",lumi=lumi)
    ticksInside(removeXLabel=True)
    plt.yscale('log')
    plt.xscale('log')
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc=3, frameon=False)
    plt.ylabel("Events / (0.001)",fontsize=12)
    plt.xlim((plotMinX,plotMaxX))
    plt.ylim((min(datTempRw.y)*0.8,max(datTempRw.y)*10))
    #correct the ticks
    import matplotlib.ticker
    ax.yaxis.set_label_coords(-0.1, 1)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    locmaj = matplotlib.ticker.LogLocator(base=10,numticks=12)
    ax.yaxis.set_major_locator(locmaj)
    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9),numticks=12)
    ax.yaxis.set_minor_locator(locmin)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())

    ######################
    # bottom plot
    ######################
    ax = plt.subplot(grid[1])
    # add to plot
    ax.plot([min(modFitRw.x),max(modFitRw.x)], [1,1],"k",alpha=0.25)
    ax.plot(modFitRw.x,datRatio,color=colors["bkg"],label="Bkg/Data")
    ax.plot(modFitRw.x,modRatio,color=colors["mod"],label="Fit/Data")
    # ax.plot(modFitRw.x, bkgRatio,label="Bkg/Bkg Component")
    ticksInside()
    plt.ylabel("BkgFit/X",fontsize=12)
    if xAxisName:
        plt.xlabel(xAxisName,fontsize=12)
    else:
        plt.xlabel(r"$m_{ll}$ TeV",fontsize=12)
    if logx: 
        plt.xscale('log')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.1f"))
        # ax.xaxis.set_minor_formatter(ticker.ScalarFormatter())
        # ax.xaxis.set_minor_formatter(ticker.FormatStrFormatter("%.8f"))
        ax.set_xticks([plotMinX,1,plotMaxX])
    plt.xlim((plotMinX,plotMaxX))
    #Get correct ticks
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    # optional, fix scale of ratio plot
    if ratioMin!=None and ratioMax!=None:
        plt.ylim((ratioMin,ratioMax))
        # make marks for above, below
        above = ma.masked_where(np.logical_or(bkgRatio<ratioMax,datRatio<ratioMax), modFitRw.x)
        below = ma.masked_where(np.logical_or(bkgRatio>ratioMin,datRatio>ratioMin),  modFitRw.x)
        shift = (ratioMax-ratioMin)*0.1
        ax.plot(above, [ratioMax-shift]*len(above),"r^")
        ax.plot(below, [ratioMin+shift]*len(below),"bv")
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.2))
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.05), minor = True)
    yrange = plt.gca().get_ylim()
    ax.plot([fitMax]*2,[yrange[0],yrange[1]],"r",alpha=0.25)
    ax.plot([fitMin]*2,[yrange[0],yrange[1]],"g",alpha=0.25)

    ax.set_yticks(np.arange(0.6,1.5,0.2))
    ax.set_yticks(np.arange(0.6,1.5,0.05), minor = True)
    # save plot
    plt.legend(fontsize=8)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")
    plt.close()
    plt.clf()

    print "\n==== Objects"
    sigFitObj.Print()
    bkgFitObj.Print()
    modFitObj.Print()
    extrap.get("lambda").Print()

    print yellow("="*53)
    print red("Plot diagnostics")
    i = int((sigFitRw.y.size)*0.8)
    print green("Signal Fit",sigFitRw.y[i])
    print green("Background Fit",bkgFitRw.y[i])
    print green("Model Fit",modFitRw.y[i])
    print yellow("S+B=(S+B)?", sigFitRw.y[i]+bkgFitRw.y[i],"=",modFitRw.y[i])
    print yellow("="*53)



def plotMcParamHist(nominal,mcExtraps,path=None,**kwargs):
    """ Make plot of the distribution of parameters from MC variations
        Uses nominal._toyParameters[name] = [list of param vals]
        Also showing nominal value
    """
    # hacky way to get nSpur into loop
    for paramName in nominal._toyParameters.keys():
        # clear plot
        plt.clf(); plt.cla()
        plt.figure(figsize=(6,6))
        # get values of params
        nominalVal = nominal.get(paramName).getVal()
        nominalMin = nominal.get(paramName).getMin()
        nominalMax = nominal.get(paramName).getMax()
        paramDist = [e.get(paramName).getVal() for e in mcExtraps]
        paramDist.append(nominalVal)
        print red(paramName,nominalVal,paramDist)
        # make histogram
        binArry = np.arange(nominalMin,nominalMax,(nominalMax-nominalMin)/20)
        n, bins, patches = plt.hist(paramDist, bins = binArry)
        paramPath = path.replace(".","-"+paramName+".")
        # draw true line
        yrange = plt.gca().get_ylim()
        plt.plot([nominalVal]*2,yrange,"r",linewidth=5,alpha=0.5,label="Nominal fit value: {0:.3f}".format(nominalVal))
        plt.ylim((yrange[0],yrange[1]*1.5))
        plt.plot([nominalMin]*2,yrange,"g",linewidth=5,alpha=0.5)
        plt.plot([nominalMax]*2,yrange,"g",linewidth=5,alpha=0.5)
        # save
        plt.legend(loc=1)
        plt.title("Distribution for {0}".format(paramName))
        plt.xlabel(paramName)
        plt.savefig(paramPath,bbox_inches="tight")

    # make plot of nSpur
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))
    # get values of params
    nominalVal = nominal.limits()["nSpur"]
    paramDist = [e.limits()["nSpur"] for e in mcExtraps]
    paramDist.append(nominalVal)
    print red("nSpur",nominalVal,paramDist)
    # make histogram
    n, bins, patches = plt.hist(paramDist, bins = 20)
    paramPath = path.replace(".","-"+"pdfUncert"+".")
    # draw true line
    yrange = plt.gca().get_ylim()
    plt.plot([nominalVal]*2,yrange,"r",linewidth=5,alpha=0.5,label="Nominal fit value: {0:.3f}".format(nominalVal))
    plt.ylim((yrange[0],yrange[1]*1.5))
    # save
    plt.legend(loc=1)
    plt.title("Distribution for PDF Uncertainty (nSpur)")
    plt.xlabel("Fit - MC")
    plt.savefig(paramPath,bbox_inches="tight")

def plotAllToys(extrap, path=None, **kwargs):
    """ Make plot of all toys, individually
    """
    if len(extrap._toyFitResults) != len(extrap._toyHistResults):
        raise BaseException(red("Miss-match results lengths"))
    if len(extrap._toyFitResults) != len(extrap._toyYields):
        raise BaseException(red("Miss-match yields lengths"))
    plotMax = extrap.get("extrapMax").getVal()
    plotMin = extrap.get("fitMin").getVal()
    fitMin = extrap.get("fitMin").getVal()
    fitMax = extrap.get("fitMax").getVal()
    # get nominal fit
    rdhObj = extrap.get("template")
    fitObj = extrap.get("backgroundModel")
    x      = extrap.get("x")
    rdh = rw.rooWrap(rdhObj,x)
    fit = rw.rooWrap(fitObj,x)
    # trim arrays to the bounds of the plot, normalize
    fit.cut(minRange=plotMin,maxRange=plotMax)
    rdh.cut(minRange=plotMin,maxRange=plotMax)
    fit.normalizeTo(rdh,minRange=fitMin,maxRange=fitMax)
    # loop over all toys
    for i in range(len(extrap._toyFitResults)):
        print "Making toy plot",i,"of",len(extrap._toyFitResults)
        plt.clf(); plt.cla()
        plt.figure(figsize=(6,6))
        toyFit = extrap._toyFitResults[i]
        toyHist = extrap._toyHistResults[i]
        diff = extrap._toyYields[i]["fitDiff"]
        # list of paramerers
        objs = []
        for paramName in extrap._toyParameters.keys():
            objs.append(plt.plot([],[]," ",label="{0}={1:.3f}".format(paramName,extrap._toyParameters[paramName][i])))
        objs.append(plt.plot(toyHist.x,toyHist.y,"ro",alpha=0.25,label="Toy template \\#{0}".format(i)))
        objs.append(plt.plot(rdh.x,rdh.y,"ko",label="Template"))
        objs.append(plt.plot(fit.x,fit.y,"c-",label="Fit"))
        objs.append(plt.plot(toyFit.x,toyFit.y,"r-",alpha=1,label="Toy Fit \\#{0}".format(i)))
        objs.append(plt.plot([],[]," ",label="Diff from nominal: {0:.2f}".format(diff)))
        plt.legend([o[0] for o in objs],[j[0].get_label() for j in objs],fontsize=12,loc=3, frameon=False)
        plt.title("Toy analysis for toy \\#{0}".format(i))
        plt.xlabel(r"$M_{ll}$ TeV")
        plt.yscale('log')
        plt.xscale('log')
        plt.xlim((plotMin,plotMax))
        plt.ylim((min(rdh.y)*0.8,max(rdh.y)*1.2))
        thisPath = path.replace(".png","-Toy{0}.png".format(i))
        atlasInternal(position="ne",status="Internal")
        ticksInside(removeXLabel=False)
        plt.savefig(thisPath,bbox_inches="tight")

def plotToyParamHist(extrap, path=None, **kwargs):
    """ Make plot of the distribution of parameters from toys
        Uses extrap._toyParameters[name] = [list of param vals]
        Also showing nominal value
    """
    for paramName in extrap._toyParameters.keys():
        # clear plot
        plt.clf(); plt.cla()
        plt.figure(figsize=(6,6))
        # get values of params
        paramDist = np.array(extrap._toyParameters[paramName])
        trueVal = extrap.get(paramName).getVal()
        trueMin = extrap.get(paramName).getMin()
        trueMax = extrap.get(paramName).getMax()
        # make histogram
        binArry = np.arange(trueMin,trueMax,(trueMax-trueMin)/60)
        n, bins, patches = plt.hist(paramDist, bins = binArry)
        plt.plot([],[]," ",label=r"$\sigma={0:.2f}$".format(paramDist.std()))
        plt.plot([],[]," ",label=r"$n={0:.2f}$".format(paramDist.mean()))
        paramPath = path.replace(".","-"+paramName+".")
        # draw true line
        yrange = plt.gca().get_ylim()
        plt.plot([trueVal]*2,yrange,"r",alpha=0.5,label="Nominal fit value: {0:.3f}".format(trueVal))
        plt.ylim((yrange[0],yrange[1]*1.5))
        plt.plot([trueMin]*2,yrange,"g",linewidth=5,alpha=0.5)
        plt.plot([trueMax]*2,yrange,"g",linewidth=5,alpha=0.5)
        # save
        plt.legend(loc=1)
        plt.title("Distribution for {0}".format(paramName))
        plt.xlabel(paramName)
        plt.savefig(paramPath,bbox_inches="tight")

    # make plot of fitDiff
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))
    # get values of params
    paramDist = extrap.toyFitDiff()
    # make histogram
    n, bins, patches = plt.hist(paramDist, bins = 60)
    plt.plot([],[]," ",label=r"$\sigma={0:.2f}$".format(paramDist.std()))
    plt.plot([],[]," ",label=r"$n={0:.2f}$".format(paramDist.mean()))
    paramPath = path.replace(".","-"+"fitDiff"+".")
    # draw true line
    yrange = plt.gca().get_ylim()
    plt.ylim((yrange[0],yrange[1]*1.5))
    # save
    plt.legend(loc=1)
    plt.title("Distribution for Fit Uncertainty")
    plt.xlabel("Toy Fit - Nominal Fit")
    plt.savefig(paramPath,bbox_inches="tight")

def multiChanLimitPlot(limitsToMake,lumi):
    plt.figure(figsize=(8,6))
    xlabels = []
    for i,l in enumerate(limitsToMake):
        if "limits" not in l.keys(): continue
        observed = l["limits"]["observed"]
        expected = l["limits"]["expected"]
        uOneSig  = l["limits"]["uOneSig"]
        uTwoSig  = l["limits"]["uTwoSig"]
        lOneSig  = l["limits"]["lOneSig"]
        lTwoSig  = l["limits"]["lTwoSig"]
        prevExp  = l["prevLimExp"]
        xlabels.append(l["model"]+" "+l["interference"])
        width=1
        plt.bar(width*i,uTwoSig-lTwoSig,edgecolor="none",width=width,bottom=lTwoSig,color="yellow",label="$2\\sigma$ Expected")
        plt.bar(width*i,uOneSig-lOneSig,edgecolor="none",width=width,bottom=lOneSig,color="green",label="$1\\sigma$ Expected")
        prev=plt.errorbar(width*i,prevExp,fmt="r",linewidth=4,xerr=width/2,label=r"Basian Expected Limit ($36.1fb^{-1}$)")
        obs=plt.errorbar(width*i,observed,fmt="ko",xerr=width/2,label="Single-Bin Observid Limit")
        exp=plt.errorbar(width*i,expected,fmt="k",xerr=width/2,label="Single-Bin Expected Limit")
        exp[-1][0].set_linestyle('--')
        if i==0: plt.legend(fontsize=12,loc=0, frameon=False)
    atlasInternal(position="nw",lumi=lumi,subnote="$ee$ channel\n\\emph{Simulation}")
    # plt.ylabel("Signal Strength $\\mu$")
    plt.ylabel("95\% CL on $\\Lambda$")
    # plt.title(title)
    plt.xticks(np.arange(0.0,len(xlabels)+0.0,1),xlabels)
    plt.ylim(0,plt.gca().get_ylim()[1]*1.5)
    # plt.yscale("log")
    # plt.ylim(0.10,1600)
    path = "plots/limits.pdf"
    plt.savefig(path,bbox_inches="tight")
    path = "plots/limits.png"
    plt.savefig(path,bbox_inches="tight")

def plotXY(xlist,ylist,names,xaxis=" ",yaxis=" ",path="plots/plot.png",oneToOne=False):
    ''' Make ATLAS style plot 
        Multiple x, y
    '''
    # check inputs
    if len(xlist)!=len(ylist) or len(ylist)!=len(names):
        raise BaseException("Mismatched input slize in plotter")

    n = len(xlist)
    plt.figure(figsize=(8,6))
    objs=[]
    maxX=0;maxY=0
    for i in range(n):
        if "const" in names[i]: marker="^"
        elif "dest" in names[i]: marker="v"
        else: marker="o"
        ylist[i]=ma.masked_where(np.abs(ylist[i])>1e16,ylist[i])
        # xlist[i]=ma.masked_where(np.abs(xlist[i])>20,xlist[i])
        objs.append(plt.plot(xlist[i],ylist[i],marker=marker,label=names[i]))
        maxX=max(maxX,np.nanmax(xlist[i]))
        maxY=max(maxY,np.nanmax(ylist[i]))
    plt.xlabel(xaxis)
    plt.ylabel(yaxis)
    if oneToOne:
        objs.append(plt.plot([0,max(maxY,maxX)],[0,max(maxY,maxX)],"b-",label="One-to-one"))
        # objs.append(plt.plot([0,20],[0,20],"b-",label="One-to-one"))
    atlasInternal(position="ne",status="Internal")
    ticksInside(removeXLabel=False)
    # plt.yscale("log")
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc="upper left", frameon=False)
    if plt.gca().get_ylim()[1]>0: plt.ylim(plt.gca().get_ylim()[0],max(abs(plt.gca().get_ylim()[0]),abs(plt.gca().get_ylim()[1]))*1.5)
    path =path.replace(" ","_")
    path =path.replace("(","_")
    path =path.replace(")","_")
    print red("saving",path)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")


def histCompareRatio(path,axisName,aHist,aName,bHist,bName,lower,upper):
    # compare two hists, TH1F
    # decide if logy
    logy=True

    import matplotlib.gridspec as gridspec
    plt.clf(); plt.cla()
    grid = gridspec.GridSpec(3, 1, height_ratios=[4, 1,1])
    grid.update(wspace=0.025, hspace=0.05)
    # plt.figure(figsize=(8,8))
    plt.figure(figsize=(6,8))
    # make roowrappers
    aRw = rw.rooWrap(aHist)
    bRw = rw.rooWrap(bHist)
    aRw.cut(minRange=lower,maxRange=upper)
    bRw.cut(minRange=lower,maxRange=upper)
    ax = aRw.x
    bx = bRw.x
    ay = aRw.y
    by = bRw.y

    ##################################
    # Top plot
    ##################################
    axis = plt.subplot(grid[0])
    ay=np.array(ay)
    by=np.array(by)
    # scale b to a
    scale=1
    if sum(by)!=0:
        scale = sum(ay)/sum(by)
    # scale = 1
    if logy:
        ay = np.array([x if x>0 else np.nan for x in ay])
        by = np.array([x if x>0 else np.nan for x in by])
    plt.errorbar(ax,ay,color="C0",linestyle=" ",marker="o",label=aName,yerr=np.sqrt(ay))
    plt.errorbar(bx,by*scale,color="C3",linestyle=" ",marker="o",label=bName,yerr=np.sqrt(by)*scale)
    # pick upper/lower limits
    upperLim = max(np.nanmax(ay),scale*np.nanmax(by))
    lowerLim = min(np.nanmin(ay),scale*np.nanmin(by))
    lowerLim = max(lowerLim,0.001)
    print red(upperLim,lowerLim)
    print green(ay)
    print green(by)
    plt.ylabel('Events')
    # plt.legend(loc="center right",fontsize=14)
    plt.legend(loc="upper left",fontsize=12)
    if logy:
        plt.ylim(top=upperLim*10)
        plt.ylim(bottom=lowerLim)
        plt.yscale("log")
    else: plt.ylim(top=upperLim*1.5)
    atlasInternal(position="ne",status="Internal",lumi="")
    ticksInside(removeXLabel=True)

    ######################
    # bottom plot
    ######################
    axis       = plt.subplot(grid[1])
    ratio      = ay/(by*scale)
    ratioError = np.sqrt(ay**2 / (by*scale)**3)
    ratio      = np.array([x if not np.isinf(x) else np.nan for x in ratio])
    ratioError = np.array([x if not np.isinf(x) else np.nan for x in ratioError])
    # for scaling axis
    stdev  = np.nanstd(ratio)
    mean   = np.nanmean(ratio)
    pltLow = mean-2*stdev
    pltHigh= mean+2*stdev
    print green(ratio)
    print red(stdev,mean,pltLow,pltHigh)
    plt.errorbar(ax,ratio,yerr=ratioError,linestyle=" ",marker="o",color="black",label=r"$e/\mu$")
    # plt.errorbar(ax,ratio,yerr=ratioError,linestyle=" ",marker="o",color="black",label="{0}/{1}".format(aName,bName))
    if not logy:
        plt.ylim((pltLow,pltHigh))
    axis.plot([min(ax),max(ax)], [1,1],"k",alpha=0.25)
    # plot arrows for the points above or below the plot
    above = ma.masked_where(ratio<pltHigh, ax)
    below = ma.masked_where(ratio>pltLow,  ax)
    axis.plot(above, [mean+1.7*stdev]*len(above),"r^")
    axis.plot(below, [mean-1.7*stdev]*len(below),"bv")
    # set up plot
    latexName = axisName
    latexName = latexName.replace("Eta",r"$\eta$")
    latexName = latexName.replace("Phi",r"$\phi$")
    latexName = latexName.replace("Pt",r"$p_T$ GeV")
    latexName = latexName.replace("maxDilepMass",r"$m_{ll}$ GeV")
    plt.xlabel(latexName)
    plt.legend(loc=0,fontsize=12)
    ticksInside()
    ##
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path,bbox_inches="tight")

def histCompare(axisName,ax,ay,aName,bx,by,bName):
    # compare two hists
    # decide if logy
    logy=(axisName in ["Pt","maxDilepMass"])

    plt.figure("fig",figsize=(6,6))
    plt.clf(); plt.cla()
    ay=np.array(ay)
    by=np.array(by)
    # scale b to a
    scale=1
    if sum(by)!=0:
        scale = sum(ay)/sum(by)
    scale = 1
    if logy:
        ay = np.array([x if x>0 else np.nan for x in ay])
        by = np.array([x if x>0 else np.nan for x in by])
    plt.errorbar(ax,ay,color="C0",linestyle=" ",marker="o",label=aName,yerr=np.sqrt(ay))
    plt.errorbar(bx,by*scale,color="C3",linestyle=" ",marker="o",label=bName,yerr=np.sqrt(ay)/scale)
    # pick upper/lower limits
    upperLim = max(max(ay),scale*max(by))
    lowerLim = min(min(ay),scale*min(by))
    lowerLim = max(lowerLim,0.001)
    # set up plot
    latexName = axisName
    latexName = latexName.replace("Eta",r"$\eta$")
    latexName = latexName.replace("Phi",r"$\phi$")
    latexName = latexName.replace("Pt",r"$p_T$ GeV")
    latexName = latexName.replace("maxDilepMass",r"$m_{ll}$")
    plt.xlabel(latexName)
    plt.ylabel('Events')
    plt.legend(loc="center right",fontsize=14)
    plt.legend(loc="upper left",fontsize=12)
    if logy:
        plt.ylim(top=upperLim*10)
        plt.ylim(bottom=lowerLim)
        plt.yscale("log")
    else: plt.ylim(top=upperLim*1.5)
    ticksInside()
    atlasInternal(position="ne",status="Internal",lumi="")
    ##
    plt.savefig("plots/output-{0}.png".format(axisName),bbox_inches="tight")
    # plt.savefig("plots/output-{0}.pdf".format(axisName),bbox_inches="tight")

def histCompareSepPlots(axisName,ax,ay,aName,bx,by,bName,path=""):
    # compare two hists
    # decide if logy
    logy=(axisName in ["leadingLepPt","subleadingLepPt","Pt","maxDilepMass"])

    import matplotlib.gridspec as gridspec
    plt.clf(); plt.cla()
    plt.figure(figsize=(5,4))

    ##################################
    # Top plot
    ##################################
    axis = plt.gca()
    ay=np.array(ay)
    by=np.array(by)
    # scale b to a
    scale=1
    # if sum(by)!=0:
    #     scale = sum(ay)/sum(by)
    # scale = 1
    if logy:
        ay = np.array([x if x>0 else np.nan for x in ay])
        by = np.array([x if x>0 else np.nan for x in by])
    plt.errorbar(ax,ay,color="C0",linestyle=" ",marker="o",label=aName,yerr=np.sqrt(ay))
    plt.errorbar(bx,by*scale,color="C3",linestyle=" ",marker="o",label=bName,yerr=np.sqrt(by)*scale)
    # pick upper/lower limits
    upperLim = max(max(ay),scale*max(by))
    lowerLim = min(min(ay),scale*min(by))
    lowerLim = max(lowerLim,0.001)
    plt.ylabel('Events')
    # plt.legend(loc="center right",fontsize=14)
    plt.legend(loc="upper left",fontsize=12)
    if logy:
        plt.ylim(top=upperLim*10)
        plt.ylim(bottom=lowerLim)
        plt.yscale("log")
    else: plt.ylim(top=upperLim*1.5)
    # atlasInternal(position="ne",status="Internal",lumi="")
    ticksInside(removeXLabel=False)
    savePath="plots/output-{0}".format(axisName)
    if path!="": savePath=savePath+"-"+path
    latexName = axisName
    latexName = latexName.replace("Eta",r"$\eta$")
    latexName = latexName.replace("Phi",r"$\phi$")
    latexName = latexName.replace("Pt",r"$p_T$ GeV")
    latexName = latexName.replace("maxDilepMass",r"$m_{ll}$ GeV")
    plt.xlabel(latexName)
    plt.savefig(savePath+".pdf",bbox_inches="tight")
    plt.savefig(savePath+".png",bbox_inches="tight")

    ######################
    # bottom plot
    ######################
    plt.clf(); plt.cla()
    plt.figure(figsize=(5,4))
    axis       = plt.gca()
    ratio      = ay/(by*scale)
    ratioError = np.sqrt(ay**2 / (by*scale)**3)
    ratio      = np.array([x if not np.isinf(x) else np.nan for x in ratio])
    ratioError = np.array([x if not np.isinf(x) else np.nan for x in ratioError])
    # for scaling axis
    stdev  = np.nanstd(ratio)
    mean   = np.nanmean(ratio)
    pltLow = mean-2*stdev
    pltHigh= mean+2*stdev
    print green(axisName)
    print green(ratio)
    print red(stdev,mean,pltLow,pltHigh)
    plt.errorbar(ax,ratio,yerr=ratioError,linestyle=" ",marker="o",color="black")
    # plt.errorbar(ax,ratio,yerr=ratioError,linestyle=" ",marker="o",color="black",label="{0}/{1}".format(aName,bName))
    try: plt.ylim((pltLow,pltHigh))
    except: pass
    axis.plot([min(ax),max(ax)], [1,1],"k",alpha=0.25)
    # plot arrows for the points above or below the plot
    above = ma.masked_where(ratio<pltHigh, ax)
    below = ma.masked_where(ratio>pltLow,  ax)
    axis.plot(above, [mean+1.7*stdev]*len(above),"r^")
    axis.plot(below, [mean-1.7*stdev]*len(below),"bv")
    # set up plot
    plt.xlabel(latexName)
    plt.ylabel("{0}/{1}".format(aName,bName),fontsize=10)
    # plt.legend(loc=0,fontsize=12)
    ticksInside(removeXLabel=False)
    ##
    savePath="plots/ratio-{0}".format(axisName)
    if path!="": savePath=savePath+"-"+path
    plt.savefig(savePath+".pdf",bbox_inches="tight")
    plt.savefig(savePath+".png",bbox_inches="tight")

def listOfHistsRatio(axisName,namesL,axL,ayL,aName,bxL,byL,bName,path="output",ratioLabel=""):
    # compare two hists
    # decide if logy
    logy=(axisName in ["leadingLepPt","subleadingLepPt","Pt","maxDilepMass"])
    import matplotlib.gridspec as gridspec

    for i in range(len(namesL)):
        names=namesL[i]
        ax=axL[i]
        ay=ayL[i]
        bx=bxL[i]
        by=byL[i]
        plt.clf(); plt.cla()
        grid = gridspec.GridSpec(3, 1, height_ratios=[2, 1,1])
        grid.update(wspace=0.025, hspace=0.05)
        plt.figure(figsize=(6,8))
        # plt.figure(figsize=(8,8))

        ##################################
        # Top plot
        ##################################
        axis = plt.subplot(grid[0])
        ay=np.array(ay)
        by=np.array(by)
        # scale b to a
        # scale=1
        # if sum(by)!=0:
        #     scale = sum(ay)/sum(by)
        scale = 1
        if logy:
            ay = np.array([x if x>0 else np.nan for x in ay])
            by = np.array([x if x>0 else np.nan for x in by])
        # print yellow(ay)
        # print yellow(by)
        # quit()
        plt.errorbar(ax,ay,color="C0",linestyle=" ",marker="o",label=aName,yerr=np.sqrt(ay))
        plt.errorbar(bx,by*scale,color="C3",linestyle=" ",marker="o",label=bName,yerr=np.sqrt(by)*scale)
        # pick upper/lower limits
        upperLim = max(max(ay),scale*max(by))
        lowerLim = min(min(ay),scale*min(by))
        lowerLim = max(lowerLim,0.001)
        plt.ylabel('Events')
        # plt.legend(loc="center right",fontsize=14)
        plt.legend(loc="upper left",fontsize=12)
        if logy:
            plt.ylim(top=upperLim*10)
            plt.ylim(bottom=lowerLim)
            plt.yscale("log")
        else: plt.ylim(top=upperLim*1.5)
        atlasInternal(position="ne",status="Internal",lumi="")
        ticksInside(removeXLabel=True)

        ######################
        # bottom plot
        ######################
        axis       = plt.subplot(grid[1])
        ratio      = ay/(by*scale)
        ratioError = np.sqrt(ay**2 / (by*scale)**3)
        ratio      = np.array([x if not np.isinf(x) else np.nan for x in ratio])
        ratioError = np.array([x if not np.isinf(x) else np.nan for x in ratioError])
        # for scaling axis
        stdev  = np.nanstd(ratio)
        mean   = np.nanmean(ratio)
        pltLow = mean-2*stdev
        pltHigh= mean+2*stdev
        if pltHigh<1.1: pltHigh=1.5
        if pltLow>0: pltLow=0
        print green(axisName)
        print green(ratio)
        print red(stdev,mean,pltLow,pltHigh)
        plt.errorbar(ax,ratio,yerr=ratioError,linestyle=" ",marker="o",color="black")
        # plt.errorbar(ax,ratio,yerr=ratioError,linestyle=" ",marker="o",color="black",label="{0}/{1}".format(aName,bName))
        try: plt.ylim((pltLow,pltHigh))
        except: pass
        axis.plot([min(ax),max(ax)], [1,1],"k",alpha=0.25,label="Ratio=1")
        # plot arrows for the points above or below the plot
        above = ma.masked_where(ratio<pltHigh, ax)
        below = ma.masked_where(ratio>pltLow,  ax)
        axis.plot(above, [mean+1.7*stdev]*len(above),"r^")
        axis.plot(below, [mean-1.7*stdev]*len(below),"bv")
        # set up plot
        latexName = axisName
        latexName = latexName.replace("Eta",r"$\eta$")
        latexName = latexName.replace("Phi",r"$\phi$")
        latexName = latexName.replace("Pt",r"$p_T$ GeV")
        latexName = latexName.replace("maxDilepMass",r"$m_{ll}$ GeV")
        plt.xlabel(latexName)
        plt.ylabel(ratioLabel)
        plt.legend(loc=0,fontsize=12)
        ticksInside()
        ##
        savePath="plots/{0}-{1}-{2}".format(axisName,names,path)
        savePath=savePath.replace(" ","_")
        print red("Saving",savePath)
        plt.savefig(savePath,bbox_inches="tight")
        plt.savefig(savePath.replace(".png",".pdf"),bbox_inches="tight")

        # break

        # if i==1: break

def plotInjVsReco(xlist,ylist,labels,nSigUncert,names,xaxis=" ",yaxis=" ",path="plots/plot.png",oneToOne=False,tags=False):
    ''' Make ATLAS style plot 
        Multiple x, y
    '''
    # check inputs
    if len(xlist)!=len(ylist) or len(ylist)!=len(names):
        print red("xlist",len(xlist))
        print red("ylist",len(ylist))
        print red("names",len(names))
        raise BaseException("Mismatched input slize in plotter")

    n = len(xlist)
    plt.figure(figsize=(6,6))
    objs=[]
    maxX=0;maxY=0
    for i in range(n):
        plt.clf(); plt.cla()
        if "const" in names[i]: marker="^"
        elif "dest" in names[i]: marker="v"
        else: marker="o"
        ylist[i]=ma.masked_where(np.abs(ylist[i])>1e16,ylist[i])
        # xlist[i]=ma.masked_where(np.abs(xlist[i])>20,xlist[i])
        # objs.append(plt.plot(xlist[i],ylist[i],marker=marker,label=names[i]))
        try:
            # plot with error bars
            plt.errorbar(xlist[i],ylist[i],yerr=nSigUncert[i],marker=marker,label=names[i])
        except:
            # plot without errorbars
            plt.plot(xlist[i],ylist[i],marker=marker,label=names[i])
        for j in range(len(xlist[i])):
            if labels[i][j]=="NoSig":
                plt.text(xlist[i][j],ylist[i][j],labels[i][j],fontsize=8)
            else:
                plt.text(xlist[i][j],ylist[i][j],r"$\lambda={0}$".format(labels[i][j]),fontsize=8)

        # maxX=max(maxX,np.nanmax(xlist[i]))
        # maxY=max(maxY,np.nanmax(ylist[i]))
        maxX = np.nanmax(xlist[i])
        maxY = np.nanmax(ylist[i])
        plt.xlabel(xaxis)
        plt.ylabel(yaxis)
        if tags: plt.title(tags[i][0])
        plt.plot([0,max(maxY,maxX)],[0,max(maxY,maxX)],"b:",label="One-to-one")
        plt.plot([0,max(maxY,maxX)],[0,0],"r",alpha=0.5)
            # objs.append(plt.plot([0,20],[0,20],"b-",label="One-to-one"))
        atlasInternal(position="ne",status="Internal")
        ticksInside(removeXLabel=False)
        # plt.yscale("log")
        # plt.legend([o[0] for o in objs],[j[0].get_label() for j in objs],fontsize=12,loc="upper left", frameon=False)
        plt.legend(loc=2,fontsize=10)
        if plt.gca().get_ylim()[1]>0: plt.ylim(plt.gca().get_ylim()[0],max(abs(plt.gca().get_ylim()[0]),abs(plt.gca().get_ylim()[1]))*1.5)
        thisPath =path.replace(".png","{0}.png".format(names[i]))
        thisPath =thisPath.replace(" ","_")
        thisPath =thisPath.replace("(","_")
        thisPath =thisPath.replace(")","_")
        if tags:
            thisPath=thisPath.replace(".png","{0}.png".format(tags[i][0]))
        print red("saving",thisPath)
        plt.savefig(thisPath,bbox_inches="tight")
        plt.savefig(thisPath.replace(".png",".pdf"),bbox_inches="tight")


def simplePlot(extrap):
    """ Diagnostic plot """
    w = extrap.w
    plt.figure(figsize=(8,9))

    # get objects
    x   = w.var("x")
    sig = rw.rooWrap(w.pdf("signalModel"),x)
    bkg = rw.rooWrap(w.pdf("backgroundModel"),x)
    mod = rw.rooWrap(w.pdf("model"),x)
    sigFrac = w.var("sigFrac").getVal()

    # normalizations
    sig.normalizeTo(mod,minRange=0,maxRange=1)
    bkg.normalizeTo(mod,minRange=0,maxRange=1)

    plt.plot(sig.x,sig.y,"r",label="Signal Model")
    plt.plot(bkg.x,bkg.y,"b",label="Background Model")
    plt.plot(mod.x,mod.y,"m",linewidth=4,alpha=0.5,label="rooAddPdf Result")
    plt.plot(mod.x,sig.y*sigFrac+(1-sigFrac)*bkg.y,"c:",linewidth=4,alpha=0.5,label="Expected Sum")
    plt.legend()
    plt.gca().set_yticklabels([])
    plt.gca().set_xticklabels([])
    plt.ylabel("y")
    plt.xlabel("x")

    plt.savefig("plots/output.png",bbox_inches="tight")


def plotMplRatioLxp(extrap, path=None, **kwargs):
    """ Make matplotlib style plot using the rooWrap class """
    xAxisName=False
    if "xAxisName" in kwargs.keys(): xAxisName=kwargs["xAxisName"]
    labels=False
    if "labels" in kwargs.keys(): labels=kwargs["labels"]
    logy=True
    if "logy" in kwargs.keys(): logy=kwargs["logy"]
    logx=False
    if "logx" in kwargs.keys(): logx=kwargs["logx"]
    ratioMax=None
    if "ratioMax" in kwargs.keys(): ratioMax=kwargs["ratioMax"]
    ratioMin=None
    if "ratioMin" in kwargs.keys(): ratioMin=kwargs["ratioMin"]
    xMin=None
    if "xMin" in kwargs.keys(): xMin=kwargs["xMin"]
    sigCompOn = extrap.exists("signalModel")
    print "*"*20,"plotting","*"*20
    ########################################################
    # Retrieve objects from workspace
    ########################################################
    # extrap.get("sigFrac").setVal(0.1)
    # templates
    sigTempObj    = extrap.get("signalTemplate")
    bkgTempObj    = extrap.get("backgroundTemplate")
    datTempObj    = extrap.get("template")
    # fits
    modFitObj     = extrap.get("model")
    bkgFitObj     = extrap.get("backgroundModel")
    sigFitObj     = extrap.get("signalModel")
    # other
    injectScale   = extrap.get("signalInjectScale").getVal()
    sigFrac       = extrap.get("sigFrac").getVal()
    x             = extrap.get("x")
    fitMax        = extrap.get("fitMax").getVal()
    fitMin        = extrap.get("fitMin").getVal()
    plotMinX      = extrap.get("fitMin").getVal()
    plotMaxX      = extrap.get("extrapMax").getVal()
    ########################################################
    # Process as rooWraps
    ########################################################
    # templates
    sigTempRw     = rw.rooWrap(sigTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    bkgTempRw     = rw.rooWrap(bkgTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    datTempRw     = rw.rooWrap(datTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    sigTempRw.scale(injectScale)
    # fits
    modFitRw      = rw.rooWrap(modFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    bkgFitRw      = rw.rooWrap(bkgFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    sigFitRw      = rw.rooWrap(sigFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    # normalizations, scale singal
    print red(datTempRw.y)
    print red(modFitRw.y)
    modFitRw.normalizeTo(datTempRw,minRange=fitMin,maxRange=fitMax)
    bkgFitRw.normalizeTo(modFitRw,minRange=fitMin,maxRange=fitMax)
    sigFitRw.normalizeTo(modFitRw,minRange=fitMin,maxRange=fitMax)
    sigFitRw.scale(sigFrac)
    bkgFitRw.scale(1-sigFrac)

    datRatio=bkgFitRw.y/datTempRw.y
    bkgRatio=bkgFitRw.y/bkgTempRw.y

    ######################
    # make matplotlib plot
    ######################

    ######################
    # top plot
    ######################
    plt.figure(figsize=(6,6))
    plt.clf(); plt.cla()
    ax = plt.gca()
    # add to plot
    objs = []
    objs.append(ax.plot(datTempRw.x, datTempRw.y,"k.",label="Template"))
    objs.append(ax.plot(modFitRw.x, modFitRw.y,"c",label="Fit"))
    objs.append(ax.plot(bkgFitRw.x, bkgFitRw.y,"b",label="Background Component"))
    objs.append(ax.plot(sigFitRw.x, sigFitRw.y,"r",label="Siganl Component"))
    objs.append(ax.plot([fitMax]*2,[min(datTempRw.y),max(datTempRw.y)],"r",alpha=0.25))
    objs.append(ax.plot([fitMin]*2,[min(datTempRw.y),max(datTempRw.y)],"g",alpha=0.25))
    rebinFactor=100
    # bkgFitRw.rebin(rebinFactor)
    bkgTempRw.rebin(rebinFactor)
    sigTempRw.rebin(rebinFactor)
    sigWidth = [bkgFitRw.x[i+1]-bkgFitRw.x[i] for i in range(len(bkgFitRw.x)-1)]
    sigWidth.append(sigWidth[-1])
    objs.append(ax.bar(bkgTempRw.x,bkgTempRw.y/rebinFactor,width=sigWidth,bottom=None,color="b",linewidth=0,alpha=0.25,label="Background"))
    # objs.append(ax.bar(sigTempRw.x,sigTempRw.y/rebinFactor,bottom=bkgFitRw.y/rebinFactor,width=sigWidth,color="r",linewidth=0,alpha=0.25,label="Signal"))
    objs.append(ax.bar(sigTempRw.x,sigTempRw.y/rebinFactor,bottom=None,width=sigWidth,color="r",linewidth=0,alpha=0.25,label="Signal"))
    yields = extrap.yields()
    nFit = yields["nFit"]
    nObs = yields["nObs"]
    nSig = yields["nSig"]
    # plot setup
    plt.yscale('log')
    plt.xscale('log')
    # plt.legend()
    plt.ylabel("Events / (0.001)")
    plt.xlim((plotMinX,plotMaxX))
    plt.ylim((min(datTempRw.y)*0.8,max(datTempRw.y)*10))
    #correct the ticks
    if xAxisName:
        plt.xlabel(xAxisName)
    else:
        plt.xlabel(r"$m_{ll}$ TeV")
    if logx: 
        plt.xscale('log')
        ax = plt.gca()
    # save
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")
    plt.close()
    plt.clf()

    ######################
    # bottom plot
    ######################
    plt.figure(figsize=(6,3))
    plt.clf(); plt.cla()
    ax = plt.gca()
    # add to plot
    ax.plot([min(modFitRw.x),max(modFitRw.x)], [1,1],"k",alpha=0.25)
    ax.plot(modFitRw.x, datRatio,label="Data Ratio")
    ax.plot(modFitRw.x, bkgRatio,label="Background Ratio")
    plt.ylabel("BkgFit/X")
    if xAxisName:
        plt.xlabel(xAxisName)
    else:
        plt.xlabel(r"$m_{ll}$ TeV")
    if logx: 
        plt.xscale('log')
        ax = plt.gca()
    plt.xlim((plotMinX,plotMaxX))
    # optional, fix scale of ratio plot
    yrange = plt.gca().get_ylim()
    ax.plot([fitMax]*2,[yrange[0],yrange[1]],"r",alpha=0.25)
    ax.plot([fitMin]*2,[yrange[0],yrange[1]],"g",alpha=0.25)

    # save plot
    # plt.legend()
    plt.savefig(path.replace(".png","-ratio.png"),bbox_inches="tight")
    plt.savefig(path.replace(".png","-ratio.png").replace(".png",".pdf"),bbox_inches="tight")
    plt.close()
    plt.clf()

def plotMplRatioSBOldModel(extrap, path=None, **kwargs):
    """ Make matplotlib style plot using the rooWrap class """

    # try: 
    #     print "*"*50
    #     print red(extrap.get("signalModel"))
    #     print red(extrap.get("signalTemplate"))
    #     print "*"*50
    # except:
    #     plotMplRatioNoSig(extrap,path=path,**kwargs)
    #     return

    # if not extrap.exists("signalModel") or not extrap.exists("signalTemplate"):
    #     plotMplRatioNoSig(extrap,path=path,**kwargs)

    signalTemplate = extrap.exists("signalTemplate")
    signalModel    = extrap.exists("signalModel")


    lumi=False
    if "lumi" in kwargs.keys(): lumi=kwargs["lumi"]
    xAxisName=False
    if "xAxisName" in kwargs.keys(): xAxisName=kwargs["xAxisName"]
    labels=False
    if "labels" in kwargs.keys(): labels=kwargs["labels"]
    logy=True
    if "logy" in kwargs.keys(): logy=kwargs["logy"]
    logx=False
    if "logx" in kwargs.keys(): logx=kwargs["logx"]
    ratioMax=None
    if "ratioMax" in kwargs.keys(): ratioMax=kwargs["ratioMax"]
    ratioMin=None
    if "ratioMin" in kwargs.keys(): ratioMin=kwargs["ratioMin"]
    xMin=None
    if "xMin" in kwargs.keys(): xMin=kwargs["xMin"]
    plt.clf(); plt.cla()
    print "*"*20,"plotting","*"*20
    ########################################################
    # Retrieve objects from workspace
    ########################################################
    # extrap.get("sigFrac").setVal(0.1)
    # templates
    x             = extrap.get("x")

    bkgTempObj    = extrap.get("backgroundTemplate")
    datTempObj    = extrap.get("template")
    # fits
    modFitObj     = extrap.get("model")
    bkgFitObj     = extrap.get("backgroundModel")
    # other
    if extrap.exists("sigFrac"):
        sigFrac   = extrap.get("sigFrac").getVal()
    else: sigFrac = 0
    fitMax        = extrap.get("fitMax").getVal()
    fitMin        = extrap.get("fitMin").getVal()
    plotMinX      = extrap.get("fitMin").getVal()
    plotMaxX      = extrap.get("extrapMax").getVal()
    ########################################################
    # Process as rooWraps
    ########################################################
    # templates
    bkgTempRw     = rw.rooWrap(bkgTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    datTempRw     = rw.rooWrap(datTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    # fits
    modFitRw      = rw.rooWrap(modFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    bkgFitRw      = rw.rooWrap(bkgFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
    # normalizations, scale singal
    print red(datTempRw.y)
    print red(modFitRw.y)
    modFitRw.normalizeTo(datTempRw,minRange=fitMin,maxRange=fitMax)
    bkgFitRw.normalizeTo(modFitRw,minRange=fitMin,maxRange=fitMax)
    bkgFitRw.scale(1-sigFrac)
    if signalModel:
        sigFitObj = extrap.get("signalModel")
        sigFitRw = rw.rooWrap(sigFitObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
        if sigFitRw.sum(minRange=fitMin,maxRange=fitMax)!=0:
            sigFitRw.normalizeTo(modFitRw,minRange=fitMin,maxRange=fitMax)
        sigFitRw.scale(sigFrac)
    if signalTemplate:
        sigTempObj    = extrap.get("signalTemplate")
        sigTempRw     = rw.rooWrap(sigTempObj,x).cut(minRange=plotMinX,maxRange=plotMaxX)
        injectScale   = extrap.get("signalInjectScale").getVal()
        sigTempRw.scale(injectScale)

    modRatio=modFitRw.y/datTempRw.y
    datRatio=bkgFitRw.y/datTempRw.y
    bkgRatio=bkgFitRw.y/bkgTempRw.y

    ######################
    # make matplotlib plot
    ######################
    # make spacing tight
    import matplotlib.gridspec as gridspec
    plt.figure(figsize=(6,6))
    # plt.figure(figsize=(6,10))
    # grid = gridspec.GridSpec(2, 1, height_ratios=[1, 1])
    grid = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    grid.update(wspace=0.025, hspace=0.05)

    ######################
    # top plot
    ######################
    ax = plt.subplot(grid[0])
    # add to plot
    objs = []
    objs.append(ax.plot(datTempRw.x, datTempRw.y,"k.",label="Template"))
    objs.append(ax.plot(modFitRw.x, modFitRw.y,"C9",label="Fit"))
    objs.append(ax.plot(bkgFitRw.x, bkgFitRw.y,"C0",label="Background Component"))
    if signalModel: objs.append(ax.plot(sigFitRw.x, sigFitRw.y,"C3",label="Siganl Component"))
    objs.append(ax.plot([fitMax]*2,[min(datTempRw.y),max(datTempRw.y)],"r",alpha=0.25,label="Fit Max:{0:.2f} TeV".format(fitMax)))
    objs.append(ax.plot([fitMin]*2,[min(datTempRw.y),max(datTempRw.y)],"g",alpha=0.25,label="Fit Min:{0:.2f} TeV".format(fitMin)))
    rebinFactor=100
    bkgTempRw.rebin(rebinFactor)
    sigWidth = [bkgTempRw.x[i+1]-bkgTempRw.x[i] for i in range(len(bkgTempRw.x)-1)]
    sigWidth.append(sigWidth[-1])
    objs.append(ax.bar(bkgTempRw.x,bkgTempRw.y/rebinFactor,width=sigWidth,bottom=None,color="b",linewidth=0,alpha=0.25,label="Background"))
    if signalTemplate:
        sigTempRw.rebin(rebinFactor)
        # objs.append(ax.bar(sigTempRw.x,sigTempRw.y/rebinFactor,bottom=bkgFitRw.y/rebinFactor,width=sigWidth,color="r",linewidth=0,alpha=0.25,label="Signal"))
        objs.append(ax.bar(sigTempRw.x,sigTempRw.y/rebinFactor,bottom=None,width=sigWidth,color="r",linewidth=0,alpha=0.25,label="Signal"))
    if labels:
        for l in labels:
            objs.append(ax.plot([],[]," ",label=l))
    yields = extrap.yields()
    nFit = yields["nFit"]
    nObs = yields["nObs"]
    nSig = yields["nSig"]
    if nSig!=0:objs.append(ax.plot([],[]," ",label="reco/inj={0:.2f}".format((nObs-nFit)/nSig)))
    # plot setup
    atlasInternal(position="ne",lumi=lumi)
    ticksInside(removeXLabel=True)
    plt.yscale('log')
    plt.xscale('log')
    plt.legend([o[0] for o in objs],[i[0].get_label() for i in objs],fontsize=12,loc=3, frameon=False)
    plt.ylabel("Events / (0.001)",fontsize=12)
    plt.xlim((plotMinX,plotMaxX))
    if signalModel:
        print "Setting mimimum to min of data, signal", 
        print green(sigFitRw.y)
        print green(sigFitRw.x)
        print green(sigFitRw.y)
        print yellow(np.nonzero(sigFitRw.y))
        print yellow(np.nonzero(datTempRw.y))
        print np.min(np.nonzero(datTempRw.y)),np.min(np.nonzero(sigFitRw.y))
        plt.ylim((min(min(np.nonzero(datTempRw.y)),min(np.nonzero(sigFitRw.y)))*0.8,max(datTempRw.y)*10))
    else:
        print "Setting mimimum"
        plt.ylim((min(datTempRw.y)*0.8,max(datTempRw.y)*10))
    #correct the ticks
    import matplotlib.ticker
    ax.yaxis.set_label_coords(-0.1, 1)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    locmaj = matplotlib.ticker.LogLocator(base=10,numticks=12)
    ax.yaxis.set_major_locator(locmaj)
    locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9),numticks=12)
    ax.yaxis.set_minor_locator(locmin)
    ax.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())

    ######################
    # bottom plot
    ######################
    ax = plt.subplot(grid[1])
    # add to plot
    ax.plot([min(modFitRw.x),max(modFitRw.x)], [1,1],"k",alpha=0.25)
    ax.plot(modFitRw.x, datRatio,label="Bkg/Data")
    ax.plot(modFitRw.x, modRatio,label="Fit/Data")
    ax.plot(modFitRw.x, bkgRatio,label="Bkg/Bkg Component")
    ticksInside()
    plt.ylabel("BkgFit/X",fontsize=12)
    if xAxisName:
        plt.xlabel(xAxisName,fontsize=12)
    else:
        plt.xlabel(r"$m_{ll}$ TeV",fontsize=12)
    if logx: 
        plt.xscale('log')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
        ax.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.1f"))
        # ax.xaxis.set_minor_formatter(ticker.ScalarFormatter())
        # ax.xaxis.set_minor_formatter(ticker.FormatStrFormatter("%.8f"))
        ax.set_xticks([plotMinX,1,plotMaxX])
    plt.xlim((plotMinX,plotMaxX))
    #Get correct ticks
    ax.yaxis.set_label_coords(-0.1, 0.95)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 10)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 0)
    # optional, fix scale of ratio plot
    if ratioMin!=None and ratioMax!=None:
        plt.ylim((ratioMin,ratioMax))
        # make marks for above, below
        above = ma.masked_where(np.logical_or(bkgRatio<ratioMax,datRatio<ratioMax), modFitRw.x)
        below = ma.masked_where(np.logical_or(bkgRatio>ratioMin,datRatio>ratioMin),  modFitRw.x)
        shift = (ratioMax-ratioMin)*0.1
        ax.plot(above, [ratioMax-shift]*len(above),"r^")
        ax.plot(below, [ratioMin+shift]*len(below),"bv")
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.2))
        ax.set_yticks(np.arange(ratioMin+0.1,ratioMax,0.05), minor = True)
    yrange = plt.gca().get_ylim()
    ax.plot([fitMax]*2,[yrange[0],yrange[1]],"r",alpha=0.25)
    ax.plot([fitMin]*2,[yrange[0],yrange[1]],"g",alpha=0.25)

    ax.set_yticks(np.arange(0.6,1.5,0.2))
    ax.set_yticks(np.arange(0.6,1.5,0.05), minor = True)
    # save plot
    plt.legend(fontsize=8)
    plt.savefig(path,bbox_inches="tight")
    plt.savefig(path.replace(".png",".pdf"),bbox_inches="tight")
    plt.close()
    plt.clf()

    # some fit diagnostics
    # postFitVals = extrap._loopParams("model")
    # print postFitVals
    # print yellow("="*20,"Fit Results","="*20)
    # for varName in postFitVals.keys():
    #     print red("{0}:\t{1}<{2:.4f}<{3}".format(varName,postFitVals[varName]["min"],
    #                                                  postFitVals[varName]["val"],
    #                                                  postFitVals[varName]["max"],
    #              ))
    print yellow("="*53)
