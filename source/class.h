#ifndef HCLASS
#define HCLASS

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooTrace.h"

class RooRealVar;

/*
################################################################################
#                                                                              #
#                             HMorph and HMorphNorm                            #
#                                                                              #
#  There are two classes in this file:                                         #
#  1) HMorph: a PDF to morph between an arbitrary number of input histogram    #
#             the class does interpolations both between the bins of the       #
#             histograms (mll dimension) and the histograms (labda dimension)  #
#  2) HMorphNorm: a RooRealVar to morph between the normalizations provided    #
#                 by the input histograms. It is associated to HMorph, and     #
#                 currently is hard coded to request an integral in "fitRange" #
#  These classes work together to provide a model in an extended pdf           #
#                                                                              #
################################################################################
*/

class HMorph : public RooAbsPdf {
public:

    HMorph(const char *name, const char *title,
           RooAbsReal& _mll, RooAbsReal& _lambda) :
        _initialized(0),
        RooAbsPdf(name,title),
        mll("mll","Observable",this,_mll),
        lambda("lambda","lambda",this,_lambda)
    {};

    HMorph(const HMorph& other, const char* name=0) :
        RooAbsPdf(other,name),
        mll("mll",this,other.mll),
        lambda("lambda",this,other.lambda)
    {
       _nBins = other._nBins;
       _minBin = other._minBin;
       _maxBin = other._maxBin;
       _binWidth = other._binWidth;
       _initialized = other._initialized;
       int nHists = other._lambdas.size();
       TH1* newHist;
       for (int i=0; i<nHists; i++){
           // newHist = (TH1*)other._hists[i]->Clone();
           newHist = other._hists[i];
           _hists.push_back(newHist);
           _lambdas.push_back(other._lambdas[i]);
           _norms.push_back(other._norms[i]);
       }
    };

    inline virtual ~HMorph() {
        cout<<">>>>>>>>>>>>>>>>>>>>>>>>\n";
        cout<<"Calling HMorph Destructo\n";
        cout<<"<<<<<<<<<<<<<<<<<<<<<<<<\n";
        return;
    }

    virtual TObject* clone(const char* newname) const { return new HMorph(*this,newname); }

    // #####################################
    // variables
    // #####################################
    int _nBins=-1;           // number of bins, should be the same for all histograms
    double _minBin=-1;       // min bin, should be the same for all histograms
    double _maxBin=-1;       // max bin, should be the same for all histograms
    double _binWidth=-1;     // bin width, should be the same for all histograms, all bins
    bool _initialized;       // keeps track of whether the class has been initialized
    vector<TH1*> _hists;     // vector of histograms
    vector<double> _lambdas; // morphing parameter value
    vector<double> _norms;   // histogram normalizaitons
    RooRealProxy mll ;       // x/binn dimension variable
    RooRealProxy lambda ;    // morphing dimension variable

    void add(TH1* hist, double paramValue){
        // Add a histogram associated to paramValue. This is a public interface func
        // If not _initialized, define binning
        // If _initialized, check against binning
        bool status;
        if (!_initialized){
            status = this->_addFirst(hist,paramValue);
        }
        else{
            status = this->_addHist(hist,paramValue);
        }
        if (!status){
            cout<<"\n\n==== ERROR ADDING HIST\n";
            cout<<"==== Hist:\n";
            hist->Print();
            cout<<"\n";
        }
    }

    bool _addFirst(TH1* hist, double paramValue){
        // add the first histogram
        _initialized = true;
        // set bin params
        _nBins    = hist->GetNbinsX();
        _minBin   = hist->GetBinLowEdge(1);
        _maxBin   = hist->GetBinLowEdge(_nBins+1);
        _binWidth = hist->GetBinWidth(1);
        cout<<"Added first histogram:"<<endl;
        cout<<"GetNbinsX:"<<hist->GetNbinsX()<<endl;
        cout<<"GetBinLowEdge:"<<hist->GetBinLowEdge(1)<<endl;
        cout<<"GetBinLowEdge:"<<hist->GetBinLowEdge(_nBins+1)<<endl;
        cout<<"GetBinWidth:"<<hist->GetBinWidth(1)<<endl;
        // add hist
        return this->_addHist(hist,paramValue);
    }

    Double_t analyticalIntegral(Int_t code, const char* rangeName) const{
        // Implemented to avoid a numeric integration of pdf
        return this->trueIntegral(rangeName);
    }

    Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName) const{
        // Implemented to avoid a numeric integration of pdf
        // copy variable which can integrate over to analVars
        analVars.add(*(allVars.find("x")));
        return 1;
    }

    Double_t trueIntegral(const char* rangeName) const {
        // get range of integral: if rangeName is provided, use this
        if (!_initialized){
            cout<<"## Warning: HMorph not init, returning 0 ##\n";
            return 0;
        }
        double rMin = mll.min(rangeName);
        double rMax = mll.max(rangeName);
        int morphIndex = this->_getMorphIndex();
        int minBin = _hists[0]->FindBin(rMin);
        int maxBin = _hists[0]->FindBin(rMax);
        double x1 = _lambdas[morphIndex];
        double x2 = _lambdas[morphIndex+1];
        double x  = lambda;
        // integrate hists
        double y1 = _hists[morphIndex]->Integral(minBin,maxBin);
        double y2 = _hists[morphIndex+1]->Integral(minBin,maxBin);
        // interpolate
        double integral = this->_interpolate(x,x1,y1,x2,y2);
        return integral;
    };

protected:

    bool _addHist(TH1* hist, double paramValue){
        // add a histogram and param
        // check bins for compatibility
        if ( hist->GetNbinsX() != _nBins              ||
             hist->GetBinLowEdge(1) != _minBin        ||
             hist->GetBinLowEdge(_nBins+1) != _maxBin ||
             hist->GetBinWidth(1) != _binWidth
           ) { 
               cout<<"==================== DEBUG CRASH ====================\n";
               cout<<"GetNbinsX:"<<hist->GetNbinsX()<<"=="<<_nBins<<endl;
               cout<<"GetBinLowEdge:"<<hist->GetBinLowEdge(1)<<"=="<<_minBin<<endl;
               cout<<"GetBinLowEdge:"<<hist->GetBinLowEdge(_nBins+1)<<"=="<<_maxBin<<endl;
               cout<<"GetBinWidth:"<<hist->GetBinWidth(1)<<"=="<<_binWidth<<endl;
               cout<<"=====================================================\n";
               throw std::runtime_error("Issue with added histogram"); 
           }
        // add histogram
        _hists.push_back(hist);
        _lambdas.push_back(paramValue);
        _norms.push_back(this->_th1Sum(hist));
        return true;
    }

    double _th1Sum(TH1* hist){
        // return integral of th1
        double result = 0;
        for (int i=1; i<hist->GetNbinsX(); i++)
            result+=hist->GetBinContent(i);
        return result;
    }


    double _interpolate(double x, double x1, double y1, double x2, double y2) const {
        // perform interpolation
        y1 = y1>0?y1:0;
        y2 = y2>0?y2:0;
        if (x2-x1==0) throw std::runtime_error("Division by zero in interpolation, x2-x1");
        double m  = (y2-y1)/(x2-x1);
        double y  = y1 + m*(x-x1);
        return y;
    }

    double _getInterpolatedHistValue(TH1* hist) const{
        // return a value interpolated between the bins of the histogram
        // the returned values should be continuous (a requirement of RooAddPdf)
        if (!hist)
           throw std::runtime_error("Expected histogram not found!"); 
        double x = mll;
        double binIndex = hist->FindBin(x);
        double binCenter = hist->GetBinCenter(binIndex);
        bool isUpper     = (x>binCenter);
        double ret;
        if (isUpper){
            double x1 = hist->GetBinCenter(binIndex);
            double x2 = hist->GetBinCenter(binIndex+1);
            double y1 = hist->GetBinContent(binIndex);
            double y2 = hist->GetBinContent(binIndex+1);
            ret = this->_interpolate(x,x1,y1,x2,y2)/_binWidth;
        }
        else{
            double x1 = hist->GetBinCenter(binIndex-1);
            double x2 = hist->GetBinCenter(binIndex);
            double y1 = hist->GetBinContent(binIndex-1);
            double y2 = hist->GetBinContent(binIndex);
            ret = this->_interpolate(x,x1,y1,x2,y2)/_binWidth;
        }
        // cout<<ret<<endl;
        return ret;
    }

    int _getMorphIndex() const{
        // return index of the first _lambdas value which is below lambda->getVal()
        // if lowest _lambdas is higher, returns 0
        // if highest _lambdas is lower, returns nHists-1 (2nd last lambda index)
        double target = lambda;
        double nHists = _hists.size();
        // edge cases
        if (_lambdas[0]>=target) return 0;
        if (_lambdas[nHists-1]<=target) return nHists-2;
        // search
        for (int i=1; i<nHists; i++)
            if (_lambdas[i]>target) return i-1;
        // should not happen
        throw std::runtime_error("Could not find HMorph index in getMorphIndex");
    }




    Double_t evaluate() const{
        if (!_initialized) return 1;
        // evaluate interpolated value at bin corresponding to mass variable mll
        // 1) find histogram index corresponding to morphing variable
        if (!_initialized) return 0;
        double morphIndex = this->_getMorphIndex();
        if (!_hists[morphIndex] || !_hists[morphIndex+1])
           throw std::runtime_error("Expected histogram not found!"); 
        double binIndex = _hists[morphIndex]->FindBin(mll);
        // ###################################
        // double morphIndex = 0;
        // double binIndex = 15;
        // 3) get interpolation between bins of histograms (mll direction)
        double y1 = this->_getInterpolatedHistValue(_hists[morphIndex]);
        double y2 = this->_getInterpolatedHistValue(_hists[morphIndex+1]);
        // 4) get values of lambda
        double x1 = _lambdas[morphIndex];
        double x2 = _lambdas[morphIndex+1];
        double x  = lambda;
        // 5) interpolate over lambda direction
        double y = this->_interpolate(x,x1,y1,x2,y2);
        // cout<<"                          "<<lambda<<" "<<mll<<" "<<y<<endl;
        return y;
    }

private:
    ClassDef(HMorph,1) // Gaussian PDF
};


class HMorphNorm : public RooAbsReal {
public:

    HMorphNorm(const char *name, const char *title, HMorph& morphInput,
               RooAbsReal& _mll, RooAbsReal& _lambda) :
        RooAbsReal(name,title),
        _morphInternal("morph","morph",this,morphInput),
        lambda("lambda","lambda",this,_lambda)
    {
    };

    HMorphNorm(const HMorphNorm& other, const char* name=0) :
        RooAbsReal(other,name),
        _morphInternal("morph",this,other._morphInternal),
        lambda("lambda",this,other.lambda)
    {
    };

    virtual TObject* clone(const char* newname) const { return new HMorphNorm(*this,newname); }
    inline virtual ~HMorphNorm() {  }

    RooRealProxy _morphInternal ;
    RooRealProxy lambda ;
protected:
    Double_t evaluate() const {
        // return the integral in the fit range for the HMorph
        return ((HMorph*)_morphInternal.absArg())->trueIntegral("fitRange");
    };
private:
    ClassDef(HMorphNorm,1) // Gaussian PDF
};

ClassImp(HMorph);
ClassImp(HMorphNorm);

#endif
