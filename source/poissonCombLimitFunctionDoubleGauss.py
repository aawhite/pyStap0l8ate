from __future__ import division
import sys
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
sys.argv.append("-b")
from ROOT import TH1F, RooWorkspace, RooDataHist, RooArgList, TCanvas, RooFit, TText, TPad
from ROOT import TLine, RooArgSet, RooAddPdf,RooCategory,RooDataSet,RooCmdArg,RooSimultaneous
from ROOT import RooLinkedList, RooAbsReal, TFile
from ROOT.RooFit import *
from ROOT import kGreen, kRed, kFALSE, kBlue
from ROOT.RooStats import ModelConfig, AsymptoticCalculator,HypoTestInverter, HypoTestCalculatorGeneric
from extra import *


# poisson limit with RooStats
# model is Poisson * Gauss * Gauss

# following https://twiki.cern.ch/twiki/bin/view/RooStats/RooStatsExercisesMarch2015

def poissonCombLimit(nBkg_ee, nSig_ee, nObs_ee, nBkg_mm, nSig_mm, nObs_mm, npWidth1_ee=1, npWidth2_ee=1, npWidth1_mm=1,npWidth2_mm=1):
# def poissonLimit(nBkg, nSig, nObs, np1=1,np2=2):
    """ calculate poisson limit
        np1 = nuissance parameter width  number:1
        np2 = nuissance parameter width  number:2
    """

    # make workspace
    w = RooWorkspace("w","w")
    w.factory("x[0]")

    # make poisson model ee
    w.factory("sum:nExp_ee(nSig[0,-300,300],prod:bkgProd_ee(sum:bkgSum_ee(1,np1_1[0,-1,1],np2_ee[0,-1,1]),nBkg_ee[{0}]))".format(nBkg_ee))
    #unsure what to do with nSig
    w.var("nSig").setVal(nSig_ee)
    w.factory("Poisson:pois_ee(nObs_ee[0],nExp_ee)");
    w.factory("Gaussian:g1_ee(np1_1,0,npWidth1_ee[{0}])".format(npWidth1_ee/nBkg_ee));
    w.factory("Gaussian:g2_ee(np2_ee,0,npWidth2_ee[{0}])".format(npWidth2_ee/nBkg_ee));
    w.factory("PROD:model_ee(pois_ee,g1_ee,g2_ee)");

    # make poisson model mm
    w.factory("sum:nExp_mm(nSig[0,-300,300],prod:bkgProd_mm(sum:bkgSum_mm(1,np1_2[0,-1,1],np2_mm[0,-1,1]),nBkg_mm[{0}]))".format(nBkg_mm))
    w.var("nSig").setVal(nSig_mm)
    w.factory("Poisson:pois_mm(nObs_mm[0],nExp_mm)");
    w.factory("Gaussian:g1_mm(np1_2,0,npWidth1_mm[{0}])".format(npWidth1_mm/nBkg_mm));
    w.factory("Gaussian:g2_mm(np2_mm,0,npWidth2[{0}])".format(npWidth2_mm/nBkg_mm));
    w.factory("PROD:model_mm(pois_mm,g1_mm,g2_mm)");

    #making joint model
    w.factory("channel[ee,mm]")
    w.factory("SIMUL:jointModel(channel,ee=model_ee,mm=model_mm)");
    pdf = w.pdf("jointModel")

    #define observable set
    w.defineSet("obs","nObs_ee,nObs_mm");

    # make data set with the number of observed events
    #making ee dataset
    data_ee = RooDataSet("data_ee","", RooArgSet(w.var("nObs_ee")));
    w.var("nObs_ee").setVal(nObs_ee);
    data_ee.add(RooArgSet(w.var("nObs_ee")))
    #making mm dataset
    data_mm = RooDataSet("data_mm","", RooArgSet(w.var("nObs_mm")));
    w.var("nObs_mm").setVal(nObs_mm);
    data_mm.add(RooArgSet(w.var("nObs_mm")))
    #combine dataset
    data = RooDataSet("data","",w.set("obs"),RooFit.Index(w.cat("channel")),RooFit.Import("ee",data_ee),RooFit.Import("mm",data_mm));
    data.add(w.set("obs"));
    # import data set in workspace and save it in a file
    getattr(w,"import")(data);

    #do I need to fit? Get the same answer - Will need to confirm.. 
    r = pdf.fitTo(data, RooFit.Save())
    
    ###
    mc = ModelConfig("sbModel",w);
    mc.SetPdf(pdf);
    mc.SetParametersOfInterest(RooArgSet(w.var("nSig")));
        
    #define observable parameters set?
    mc.SetObservables(w.set("obs"));
    
    #define nuisance parameters set
    w.defineSet("nuisParams","np1_1,np2_ee,np1_2,np2_mm");
    mc.SetNuisanceParameters(w.set("nuisParams"));
   
    # these are needed for the hypothesis tests
    mc.SetSnapshot(RooArgSet(w.var("nSig")));
    # mc.SetGlobalObservables(w.var("nBkgExpect")); # removed etienne
    
    mc.Print();
    # import model in the workspace
    getattr(w,"import")(mc);

    # ##################################################
    sbModel = mc;

    # create null hypothesis H0 (based on H)
    bModel = sbModel.Clone();
    bModel.SetName("bModel");
    bModel.SetSnapshot((RooArgSet(w.var("nSig"))));
    # fix nSig value to 0
    nSigNoSig = bModel.GetParametersOfInterest().first();
    nSigNoSig.setVal(0);
    # nSigNoSig.setConstant();
    bModel.SetSnapshot(RooArgSet(nSigNoSig));

    # do aysmtotic test
    ac = AsymptoticCalculator(data, bModel, sbModel);
    ac.SetOneSided(True);
    # AsymptoticCalculator::SetPrintLevel(-1);

    # create hypotest inverter
    hypoCalc = HypoTestInverter(ac);
    hypoCalc.SetFixedScan(1000,0.01,100, True);#added by etienne 2
    hypoCalc.SetConfidenceLevel(0.95);
    hypoCalc.UseCLs(True);
    hypoCalc.SetVerbose(False);
    # hypoCalc.SetOneSidedDiscovery(True);

    # htr = HypoTestCalculatorGeneric(hypoCalc).GetHypoTest();
    htr = hypoCalc.GetHypoTestCalculator().GetHypoTest();
    htir = hypoCalc.GetInterval();

    # get resutls
    ret = {}
    ret["upperLimit"] = htir.UpperLimit();
    ret["lowerLimit"] = htir.LowerLimit();
    ret["expLimit"]   = htir.GetExpectedUpperLimit(0);
    ret["uOneSig"]    = htir.GetExpectedUpperLimit(1);
    ret["uTwoSig"]    = htir.GetExpectedUpperLimit(2);
    ret["lOneSig"]    = htir.GetExpectedUpperLimit(-1);
    ret["lTwoSig"]    = htir.GetExpectedUpperLimit(-2);
    ret["nExpModel_ee"]  = w.obj("nExp_ee").getVal()
    ret["nExpModel_mm"]  = w.obj("nExp_mm").getVal()
    ret["nBkg_ee"]       = nBkg_ee
    ret["nObs_ee"]       = nObs_ee
    ret["npWidth1_ee"]   = npWidth1_ee
    ret["npWidth2_ee"]   = npWidth2_ee
    ret["nBkg_mm"]       = nBkg_mm
    ret["nObs_mm"]       = nObs_mm
    ret["npWidth1_mm"]   = npWidth1_mm
    ret["npWidth2_mm"]   = npWidth2_mm
    # ret["nSig"]       = w.var("nSig").getVal()
    # ret["np1"]        = w.var("np1").getVal()
    # ret["np2"]        = w.var("np2").getVal()
    
    return ret


if __name__=="__main__":
    ### test
    nBkg=10
    nSig=3
    nObs=15
    np1=1
    np2=1
    limit=poissonLimit(nBkg, nSig, nObs, np1,np2)
    print "="*50
    print limit
    # print limit["nObs"]
    print limit["nExpModel"]
    print limit["nBkg"]
    print limit["nSig"]
    print limit["np1"]
    print limit["np2"]
    print "="*50
